###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#author: Guido Andreassi <guido.andreassi@cern.ch>
#This is an optionsfile for producing the ntuples needed for the validation of the stripping (BhadronCompleteEvent stream) in RD

from DaVinci.Configuration import *
from Gaudi.Configuration import *
from DecayTreeTuple.Configuration import *


B2KstGamma = DecayTreeTuple('B2KstGamma')
B2KstGamma.Inputs = ["/Event/BhadronCompleteEvent/Phys/Beauty2XGammaExclusiveBd2KstGammaLine/Particles"]
B2KstGamma.Decay = "(B0 -> ^(K*(892)0-> ^K+ ^pi-) ^gamma) || (B0 -> ^(K*(892)0-> ^K- ^pi+) ^gamma)"


DaVinci().MoniSequence += [B2KstGamma]
DaVinci().TupleFile = "S29_validation_bhadroncomplete.root"
#DaVinci().EventPreFilters += [stripFilter]
DaVinci().DDDBtag   = 'dddb-20150724' 
DaVinci().CondDBtag = 'cond-20170510'
#DaVinci().DQFLAGStag    = dqflag
DaVinci().InputType = 'DST'
DaVinci().DataType = '2017' 

#for local test
""" 
import os
from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles(
        ['./00063876_00000019_1.leptonic.mdst',
         './00063876_00000029_1.leptonic.mdst']
        , clear=True)
"""
