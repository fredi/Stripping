###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Gaudi.Configuration import *
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles(["LFN:/lhcb/LHCb/Lead18/RDST/00083198/0000/00083198_00003427_1.rdst",
"LFN:/lhcb/LHCb/Lead18/RDST/00083198/0001/00083198_00010614_1.rdst",
"LFN:/lhcb/LHCb/Lead18/RDST/00083198/0000/00083198_00004715_1.rdst",
"LFN:/lhcb/LHCb/Lead18/RDST/00083198/0000/00083198_00004689_1.rdst"
#"LFN:/lhcb/LHCb/Lead18/RDST/00083198/0001/00083198_00010685_1.rdst",
#"LFN:/lhcb/LHCb/Lead18/RDST/00083198/0001/00083198_00010686_1.rdst",
#"LFN:/lhcb/LHCb/Lead18/RDST/00083198/0001/00083198_00010687_1.rdst"
#"LFN:/lhcb/LHCb/Lead18/RDST/00083198/0001/00083198_00010688_1.rdst",
#"LFN:/lhcb/LHCb/Lead18/RDST/00083198/0001/00083198_00010689_1.rdst",
#"LFN:/lhcb/LHCb/Lead18/RDST/00083198/0001/00083198_00010690_1.rdst",
#"LFN:/lhcb/LHCb/Lead18/RDST/00083198/0001/00083198_00010691_1.rdst",
#"LFN:/lhcb/LHCb/Lead18/RDST/00083198/0001/00083198_00010692_1.rdst",
#"LFN:/lhcb/LHCb/Lead18/RDST/00083198/0001/00083198_00010693_1.rdst",
#"LFN:/lhcb/LHCb/Lead18/RDST/00083198/0001/00083198_00010694_1.rdst",
#"LFN:/lhcb/LHCb/Lead18/RDST/00083198/0001/00083198_00010695_1.rdst"
                             ], clear=True)



from Gaudi.Configuration import FileCatalog

FileCatalog().Catalogs += [ 'xmlcatalog_file:$STRIPPINGSELECTIONSROOT/tests/data/Reco18Lead18_217952_PbNe.xml' ]

