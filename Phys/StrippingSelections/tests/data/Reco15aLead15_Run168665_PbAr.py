###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#-- GAUDI jobOptions generated on Wed Apr  5 16:43:00 2017
#-- Contains event types : 
#--   90000000 - 817 files - 14198787 events - 971.71 GBytes


#--  Extra information about the data processing phases:

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
    'LFN:/lhcb/LHCb/Lead15/RDST/00050695/0000/00050695_00000433_1.rdst',
    'LFN:/lhcb/LHCb/Lead15/RDST/00050695/0000/00050695_00000434_1.rdst',
    'LFN:/lhcb/LHCb/Lead15/RDST/00050695/0000/00050695_00000435_1.rdst',
    'LFN:/lhcb/LHCb/Lead15/RDST/00050695/0000/00050695_00000436_1.rdst',
    'LFN:/lhcb/LHCb/Lead15/RDST/00050695/0000/00050695_00000437_1.rdst',
    'LFN:/lhcb/LHCb/Lead15/RDST/00050695/0000/00050695_00000438_1.rdst',
], clear=True)




from Gaudi.Configuration import FileCatalog

FileCatalog().Catalogs += [ 'xmlcatalog_file:$STRIPPINGSELECTIONSROOT/tests/data/Reco15aLead15_Run168665_PbAr.xml' ]

