###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Gaudi.Configuration import *
from Configurables import DaVinci
from PRConfig import TestFileDB
TestFileDB.test_file_db["TestData_stripping_PbPbcollision18_reco18"].run(configurable=DaVinci())

#from GaudiConf import IOHelper
#IOHelper('ROOT').inputFiles([
#'LFN:/lhcb/LHCb/Lead18/RDST/00083197/0001/00083197_00017884_1.rdst',
#'LFN:/lhcb/LHCb/Lead18/RDST/00083197/0001/00083197_00017873_1.rdst',
#'LFN:/lhcb/LHCb/Lead18/RDST/00083197/0001/00083197_00017858_1.rdst',
#'LFN:/lhcb/LHCb/Lead18/RDST/00083197/0001/00083197_00015240_1.rdst',
#], clear=True)


from Gaudi.Configuration import FileCatalog

FileCatalog().Catalogs += [ 'xmlcatalog_file:$STRIPPINGSELECTIONSROOT/tests/data/Reco18Lead18_218177_PbPb.xml' ]

