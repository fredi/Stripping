###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os

strippingdatasets = {}

def make_stripping_job(name, rundirectory, datafile, nfiles = -1, evtmax = -1, 
                       reportfreq = 10000, dirac = True, diracsettings = {}, platform = 'x86_64-slc6-gcc62-opt', **kwargs) :
    app = GaudiExec(directory = rundirectory, platform = platform)
    j = Job(name = name, application = app)

    if datafile in strippingdatasets :
        j.inputdata = strippingdatasets[datafile]
    else :
        j.application.readInputData(datafile)
        j.inputdata.depth = 2
        xml = datafile.replace('.py', '.xml')
        if os.path.exists(xml) :
            j.inputdata.XMLCatalogueSlice = xml
        strippingdatasets[datafile] = j.inputdata

    if nfiles != -1 :
        j.inputdata = LHCbDataset(files = j.inputdata.files[:nfiles],
                                  XMLCatalogueSlice = j.inputdata.XMLCatalogueSlice,
                                  depth = 2)

    optsfile = datafile.replace('.py', '_DV.py')
    if os.path.exists(optsfile) :
        j.application.options = [optsfile]

    opts = '''from Configurables import Stripping, DaVinci

Stripping().ReportFrequency = {0}
Stripping().MonitorTiming = True
DaVinci().appendToMainSequence([Stripping().sequence])
DaVinci().HistogramFile = 'DV_stripping_histos.root'
'''.format(reportfreq)

    for attr, val in kwargs.iteritems() :
        opts += 'Stripping().' + attr + ' = ' + repr(val) + '\n'

    if evtmax != -1 :
        opts += 'DaVinci().EvtMax = ' + str(evtmax) + '\n'

    opts += '''# This is only necessary as for some reason using gaudirun.py calls
# the buggy applyConfigurableUsers_old rather than applyConfigurableUsers
# from GaudiKernel/Configurable.py. TracySys lists DecodeRawEvent in __used_configurables__, 
# but is only instantiated (by some reco. eff. lines) after the call of applyConfigurableUsers_old 
# is started (when Stripping.__apply_configuration__ is called), and so it isn't added to the list
# of configurables to be applied, leaving an un-applied user of DecodeRawEvent, so it can't be applied.
# Instantiating TrackSys up front solves this.

from TrackSys.Configuration import TrackSys
TrackSys()
'''

    splitter = SplitByFiles(filesPerJob = 1)
    if dirac :
        if diracsettings :
            backend = Dirac(settings = diracsettings)
        else :
            backend = Dirac()
    else :
        backend = Local()

    # Ganga is weird about xml catalogs, so make sure it's uploaded and used.
    if j.inputdata.XMLCatalogueSlice.namePattern :
        opts += '''from Gaudi.Configuration import FileCatalog
FileCatalog().Catalogs += [ 'xmlcatalog_file:{0}' ]
'''.format(j.inputdata.XMLCatalogueSlice.namePattern)
        j.inputfiles = [j.inputdata.XMLCatalogueSlice]

    j.application.extraOpts = opts
    j.application.extraArgs = ['-T']
    j.backend = backend
    j.splitter = splitter
    j.outputfiles = [LocalFile('*dst'),
                     LocalFile('strippingreport.py'),
                     LocalFile('*.root')]

    return j
