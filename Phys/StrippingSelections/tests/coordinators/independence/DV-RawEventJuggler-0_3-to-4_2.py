###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
# Raw event juggler to split DAQ/RawEvent into FULL.DST format
#
from Configurables import GaudiSequencer, RawEventJuggler
jseq=GaudiSequencer("RawEventSplitSeq")
juggler=RawEventJuggler("rdstJuggler")
juggler.Sequencer=jseq
juggler.Input=0.3  # 2015 Online (Moore) format 
juggler.Output=4.2 # Reco15 format

from Configurables import DaVinci
DaVinci().prependToMainSequence( [jseq] )
