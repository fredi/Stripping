###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#ganga submit.py       # All WGs 
ganga submit.py B2CC
ganga submit.py B2OC
ganga submit.py BandQ
ganga submit.py Charm
ganga submit.py BnoC
ganga submit.py QEE
ganga submit.py RD
ganga submit.py Semileptonic
ganga submit.py ALL   # "ALL" is a misleading WG name for calibration, min. bias etc. lines
