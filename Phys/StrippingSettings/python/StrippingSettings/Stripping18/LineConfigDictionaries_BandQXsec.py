###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
DiMuonForXsection = {
    'BUILDERTYPE' : 'DiMuonForXsectionConf',
    'CONFIG' : {
    'TotalCuts'  :  """
    (MINTREE('mu+'==ABSID,PT)>0.65*GeV)
    & (VFASPF(VCHI2PDOF)<20)
    & ((ADMASS('J/psi(1S)')<120*MeV) | (ADMASS('psi(2S)')<120*MeV) | (MM>8.5*GeV))
    """,
    'Prescale'   :  1.,
    'CheckPV'    :  False
    },
    'STREAMS' : [ 'ICHEP' ] ,
    'WGs'    : [ 'BandQ' ]
    }
