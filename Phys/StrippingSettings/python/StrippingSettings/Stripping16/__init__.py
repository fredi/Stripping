###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__all__ = [ 'LineConfigDictionaries_Charm',
            'LineConfigDictionaries_Calib',
            'LineConfigDictionaries_BetaS',
            'LineConfigDictionaries_MiniBias',
            'LineConfigDictionaries_Semileptonic',
            'LineConfigDictionaries_GTWG',
            'LineConfigDictionaries_GLWG',
            'LineConfigDictionaries_RDWG',
            'LineConfigDictionaries_CharmXsec',
            'LineConfigDictionaries_EWWG',
            'LineConfigDictionaries_PSWG'
            ]

