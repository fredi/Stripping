###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#########################################################################################################
##                          S T R I P P I N G  2 4 r 1                                                ##
##                                                                                                     ##
##  Configuration for B2OC WG                                                                          ##
##  Contact person: Wenbin Qian & Nicola Anne Skidmore (wenbin.qian@cern.ch & nicola.skidmore@cern.ch) ##
#########################################################################################################

from GaudiKernel.SystemOfUnits import *


Beauty2Charm = {
    'BUILDERTYPE' : 'Beauty2CharmConf',
    'CONFIG' : {
    "ALL" : { # Cuts made on all charged input particles in all lines (expt. upstream)
    'TRCHI2DOF_MAX' : 4.,
    'PT_MIN'        : '100*MeV',
    'P_MIN'         : '1000*MeV',
    'MIPCHI2DV_MIN' : 4., 
    'TRGHP_MAX'     : 0.4
    },
    "PIDPION" : {
    'TRCHI2DOF_MAX' : 4.,
    'PT_MIN'        : '100*MeV',
    'P_MIN'         : '1000*MeV',
    'MIPCHI2DV_MIN' : 4.,
    'PIDK_MAX'      : 20.,
    'TRGHP_MAX'     : 0.4
    },
    "PIDKAON" : {
    'TRCHI2DOF_MAX' : 4.,
    'PT_MIN'        : '100*MeV',
    'P_MIN'         : '1000*MeV',
    'MIPCHI2DV_MIN' : 4.,
    'PIDK_MIN'      : -10.,
    'TRGHP_MAX'     : 0.4
    },
    "PIDPROTON" : {
    'TRCHI2DOF_MAX' : 4.,
    'PT_MIN'        : '100*MeV',
    'P_MIN'         : '1000*MeV',
    'MIPCHI2DV_MIN' : 4.,
    'PIDp_MIN'      : -10.,
    'TRGHP_MAX'     : 0.4
    },
    "Xibc_Xc_PION" : {
    'TRCHI2DOF_MAX' : 4.,
    'PT_MIN'        : '100*MeV',
    'P_MIN'         : '1000*MeV',
    'MIPCHI2DV_MIN' : 1.0,
    'PIDK_MAX'      : 10.,
    'TRGHP_MAX'     : 0.4
    },
    "Xibc_Xc_KAON" : {
    'TRCHI2DOF_MAX' : 4.,
    'PT_MIN'        : '150*MeV',
    'P_MIN'         : '1000*MeV',
    'MIPCHI2DV_MIN' : 1.0,
    'PIDK_MIN'      : -5.0,
    'TRGHP_MAX'     : 0.4
    },
    "Xibc_Xc_PROTON" : {
    'TRCHI2DOF_MAX' : 4.,
    'PT_MIN'        : '400*MeV',
    'P_MIN'         : '1000*MeV',
    'MIPCHI2DV_MIN' : 1.0,
    'PIDp_MIN'      : -5.0,
    'TRGHP_MAX'     : 0.4
    },
    "Xibc_Xb_PION" : {
    'TRCHI2DOF_MAX' : 4.,
    'PT_MIN'        : '300*MeV',
    'P_MIN'         : '1000*MeV',
    'MIPCHI2DV_MIN' : 4.,
    'PIDK_MAX'      : 10.,
    'TRGHP_MAX'     : 0.4
    },
    "Xibc_Xb_KAON" : {
    'TRCHI2DOF_MAX' : 4.,
    'PT_MIN'        : '300*MeV',
    'P_MIN'         : '1000*MeV',
    'MIPCHI2DV_MIN' : 4.,
    'PIDK_MIN'      : -5.0,
    'TRGHP_MAX'     : 0.4
    },
    "Xibc_Xb_PROTON" : {
    'TRCHI2DOF_MAX' : 4.,
    'PT_MIN'        : '300*MeV',
    'P_MIN'         : '1000*MeV',
    'MIPCHI2DV_MIN' : 4.,
    'PIDp_MIN'      : -5.0,
    'TRGHP_MAX'     : 0.4
    },
    "UPSTREAM" : { # Cuts made on all upstream particles
    'TRCHI2DOF_MAX' : 4.,
    'PT_MIN'        : '100*MeV',
    'P_MIN'         : '1000*MeV',
    'MIPCHI2DV_MIN' : 4.
    },
    "KS0" : { # Cuts made on all K shorts
    'PT_MIN'        : '250*MeV',
    'BPVVDCHI2_MIN' : 36,
    'MM_MIN'        : '467.*MeV',
    'MM_MAX'        : '527.*MeV'
    },
    "Lambda0" : { # Cuts made on all Lambda0's
    'PT_MIN'        : '250*MeV',
    'BPVVDCHI2_MIN' : 36,
    'MM_MIN'        : '1086.*MeV',
    'MM_MAX'        : '1146.*MeV'
    },
    "Pi0" : { # Cuts made on all pi0's
    'PT_MIN'        : '500*MeV',
    'P_MIN'         : '1000*MeV',
    'CHILDCL1_MIN'  : 0.25,
    'CHILDCL2_MIN'  : 0.25,
    'FROM_B_P_MIN'  : '2000*MeV',
    'TIGHT_PT_MIN'  : '1500*MeV'
    },
    "gamma" : { # Cuts made on all photons
    'PT_MIN'     : '400*MeV',
    'CL_MIN'     : 0.25,
    'ISNOTE_MIN' : -999.0,
    'PT_VLAPH'   : '90*MeV'
    },
    "D2X" : { # Cuts made on all D's and Lc's used in all lines 
    'ASUMPT_MIN'    : '1800*MeV',
    'ADOCA12_MAX'   : '0.5*mm',
    'ADOCA13_MAX'   : '0.5*mm',
    'ADOCA23_MAX'   : '0.5*mm',
    'ADOCA14_MAX'   : '0.5*mm',
    'ADOCA24_MAX'   : '0.5*mm',
    'ADOCA34_MAX'   : '0.5*mm',
    'ADOCA15_MAX'   : '0.5*mm',
    'ADOCA25_MAX'   : '0.5*mm',
    'ADOCA35_MAX'   : '0.5*mm',
    'ADOCA45_MAX'   : '0.5*mm',
    'VCHI2DOF_MAX'  : 10,
    'BPVVDCHI2_MIN' : 36,
    'BPVDIRA_MIN'   : 0, 
    'MASS_WINDOW'   : '100*MeV'
    },
    "LC_FOR_XIBC" : { # Cuts made on Lc's/Xic(0) used in Xibc lines
    'ASUMPT_MIN'    : '1800*MeV',
    'ADOCA12_MAX'   : '0.5*mm',
    'ADOCA13_MAX'   : '0.5*mm',
    'ADOCA23_MAX'   : '0.5*mm',
    'ADOCA14_MAX'   : '0.5*mm',
    'ADOCA24_MAX'   : '0.5*mm',
    'ADOCA34_MAX'   : '0.5*mm',
    'ADOCA15_MAX'   : '0.5*mm',
    'ADOCA25_MAX'   : '0.5*mm',
    'ADOCA35_MAX'   : '0.5*mm',
    'ADOCA45_MAX'   : '0.5*mm',
    'VCHI2DOF_MAX'  : 8,
    'BPVVDCHI2_MIN' : 36,
    'BPVDIRA_MIN'   : 0, 
    'MASS_WINDOW'   : '50*MeV'
    },
    "D2X_FOR_DDX" : { # Cuts made on all D's and Lc's used in B->DDX lines 
    'ASUMPT_MIN'    : '1800*MeV',
    'ADOCA12_MAX'   : '0.5*mm',
    'ADOCA13_MAX'   : '0.5*mm',
    'ADOCA23_MAX'   : '0.5*mm',
    'ADOCA14_MAX'   : '0.5*mm',
    'ADOCA24_MAX'   : '0.5*mm',
    'ADOCA34_MAX'   : '0.5*mm',
    'ADOCA15_MAX'   : '0.5*mm',
    'ADOCA25_MAX'   : '0.5*mm',
    'ADOCA35_MAX'   : '0.5*mm',
    'ADOCA45_MAX'   : '0.5*mm',
    'VCHI2DOF_MAX'  : 8,
    'BPVVDCHI2_MIN' : 50,
    'BPVDIRA_MIN'   : 0, 
    'MASS_WINDOW'   : '100*MeV'
    },
    "B2X" : { # Cuts made on all B's and Lb's used in all lines
    'SUMPT_MIN'     : '5000*MeV',
    'VCHI2DOF_MAX'  : 10,
    'BPVIPCHI2_MAX' : 25,
    'BPVLTIME_MIN'  : '0.2*ps',
    'BPVDIRA_MIN'   : 0.999,
    'AM_MIN'        : '4750*MeV', # Lb->X sets this to 5200*MeV
    'AM_MAX'        : '7000*MeV', # B->Dh+-h0 sets this to 5800*MeV
    'B2CBBDT_MIN'   : 0.05
    },
    "Bc2DD" : { # Cuts made on Bc -> DD lines
    'SUMPT_MIN'     : '5000*MeV',
    'VCHI2DOF_MAX'  : 10,
    'BPVIPCHI2_MAX' : 20,
    'BPVLTIME_MIN'  : '0.05*ps',
    'BPVDIRA_MIN'   : 0.999,
    'AM_MIN'        : '4800*MeV', 
    'AM_MAX'        : '6800*MeV',
    'B2CBBDT_MIN'   : -999.9
    },
    "Xibc" : { # Cuts made on Xibc -> Xc hh, LcD0, Xicc h lines
    'SUMPT_MIN'     : '5000*MeV',
    'VCHI2DOF_MAX'  : 8,
    'BPVIPCHI2_MAX' : 20,
    'BPVLTIME_MIN'  : '0.05*ps',
    'BPVDIRA_MIN'   : 0.99,
    'AM_MIN'        : '5500*MeV', 
    'AM_MAX'        : '9000*MeV',
    'B2CBBDT_MIN'   : -999.9
    },
    "XiccMu" : { # Cuts made on Xibc -> Xc hh, LcD0, Xicc h lines
    'SUMPT_MIN'     : '4000*MeV',
    'VCHI2DOF_MAX'  : 10,
    'BPVIPCHI2_MAX' : 200,
    'BPVLTIME_MIN'  : '0.0*ps',
    'BPVDIRA_MIN'   : 0.99,
    'AM_MIN'        : '3000*MeV',
    'AM_MAX'        : '8000*MeV',
    'B2CBBDT_MIN'   : -999.9
    },
    "Bc2BX" : { # Cuts made on Bc -> BHH lines
    'SUMPT_MIN'     : '1000*MeV',
    'VCHI2DOF_MAX'  : 10,
    'BPVIPCHI2_MAX' : 25,
    'BPVLTIME_MIN'  : '0.05*ps',
    'BPVDIRA_MIN'   : 0.999,
    'AM_MIN'        : '6000*MeV',
    'AM_MAX'        : '7200*MeV',
    'DZ1_MIN'       : '-1.5*mm',
    'B2CBBDT_MIN'   : -999.9
    },
    "Bc2DX" : { # Cuts made on Bc -> DX lines
    'SUMPT_MIN'     : '5000*MeV',
    'VCHI2DOF_MAX'  : 10,
    'BPVIPCHI2_MAX' : 25,
    'BPVLTIME_MIN'  : '0.05*ps',
    'BPVDIRA_MIN'   : 0.999,
    'AM_MIN'        : '5800*MeV', 
    'AM_MAX'        : '6800*MeV', 
    'B2CBBDT_MIN'   : -999.9
    },
    "Dstar" : { # Cuts made on all D*'s used in all lines 
    'ADOCA12_MAX'  : '0.5*mm',
    'VCHI2DOF_MAX'  : 10,
    'BPVVDCHI2_MIN' : 36,
    'BPVDIRA_MIN'   : 0, 
    'MASS_WINDOW'   : '600*MeV', # was 50MeV
    'DELTAMASS_MAX' : '200*MeV',
    'DELTAMASS_MIN' : '90*MeV',
    'DM_DSPH_MAX'   : '250*MeV',
    'DM_DSPH_MIN'   : '80*MeV'
    },
    "HH": { # Cuts for rho, K*, phi, XHH Dalitz analyese, etc.
    'MASS_WINDOW'   : {'KST':'150*MeV','RHO':'150*MeV','PHI':'150*MeV'},
    'DAUGHTERS'     : {'PT_MIN':'100*MeV','P_MIN':'2000*MeV'},
    'ADOCA12_MAX'  : '0.5*mm',
    'VCHI2DOF_MAX'  : 16,
    'BPVVDCHI2_MIN' : 16, 
    'BPVDIRA_MIN'   : 0,
    'ASUMPT_MIN'    : '1000*MeV',
    'pP_MIN'        : '5000*MeV' # for pH only (obviously)
    },
    "HHH": { # Cuts for PiPiPi, KPiPi analyese, etc.
    'MASS_WINDOW'   : {'A1':'3500*MeV','K1':'4000*MeV','PPH':'3600*MeV', 'PHH':'4000*MeV'},
    'KDAUGHTERS'    : {'PT_MIN':'100*MeV','P_MIN':'2000*MeV','PIDK_MIN':'-2'},
    'PiDAUGHTERS'   : {'PT_MIN':'100*MeV','P_MIN':'2000*MeV','PIDK_MAX':'10'},
    'pDAUGHTERS'    : {'PT_MIN':'100*MeV','P_MIN':'2000*MeV','PIDp_MIN':'-2'},
    'ADOCA12_MAX'   : '0.40*mm', 
    'ADOCA13_MAX'   : '0.40*mm',  
    'ADOCA23_MAX'   : '0.40*mm',    
    'VCHI2DOF_MAX'  : 8,
    'BPVVDCHI2_MIN' : 16, 
    'BPVDIRA_MIN'   : 0.98,
    'ASUMPT_MIN'    : '1250*MeV',
    'MIPCHI2DV_MIN' : 0.0,
    'BPVVDRHO_MIN'  : '0.1*mm',
    'BPVVDZ_MIN'    : '2.0*mm',
    'PTMIN1'       : '300*MeV',
    'PID'           : {'TIGHTERPI' : { 'P' : {'PIDp_MIN' : -10},
                                       'PI': {'PIDK_MAX' : 8},
                                       'K' : {'PIDK_MIN' : -10}},
                       'REALTIGHTK' : { 'P' : {'PIDp_MIN' : -10},
                                        'PI': {'PIDK_MAX' : 10},
                                        'K' : {'PIDK_MIN' : 4}}}
    },
    'PID' : {
    'P'  : {'PIDp_MIN' : -10},
    'PI' : {'PIDK_MAX' : 20},
    'K'  : {'PIDK_MIN' : -10},
    'TIGHT' : {    'P'  : {'PIDp_MIN' : -5},
                   'PI' : {'PIDK_MAX' : 10},
                   'K'  : {'PIDK_MIN' : -5}},
    'TIGHTER' : {    'P'  : {'PIDp_MIN' : 0},
                     'PI' : {'PIDK_MAX' : 10},
                     'K'  : {'PIDK_MIN' : 0}},
    'TIGHTPI' : { 'P' : {'PIDp_MIN' : -10},
                  'PI': {'PIDK_MAX' : 10},
                  'K' : {'PIDK_MIN' : -10}},
    'TIGHTER1' : {    'P'  : {'PIDp_MIN' : 0},
                     'PI' : {'PIDK_MAX' : 10},
                     'K'  : {'PIDK_MIN' : -1}},                      
    'TIGHTER2' : {    'P'  : {'PIDp_MIN' : 5},
                      'PI' : {'PIDK_MAX' : 10},
                      'K'  : {'PIDK_MIN' : 0}},
    'SPECIAL' : {    'P'  : {'PIDp_MIN' : -5},
                     'PI' : {'PIDK_MAX' : 5},
                     'K'  : {'PIDK_MIN' : 5}},
    'SPECIALPI': {'P' : {'PIDp_MIN' : -10},
                  'PI': {'PIDK_MAX' : 12},
                  'K' : {'PIDK_MIN' : -10}}
    },
    'FlavourTagging':[

    #checked
   
    'B02DDWSBeauty2CharmLine',

    'B02DsstKsPiLLDsst2DGammaD2HHHBeauty2CharmLine',
    'B02DsstKsPiDDDsst2DGammaD2HHHBeauty2CharmLine',


    'B02D0MuMuD2HHBeauty2CharmLine',
    'B02D0MuMuWSD2HHBeauty2CharmLine',
    'B02D0MuMuD2HHHHBeauty2CharmLine',
    'B02D0MuMuWSD2HHHHBeauty2CharmLine',
    'B02D0MuMuD2KSHHLLBeauty2CharmLine',
    'B02D0MuMuWSD2KSHHLLBeauty2CharmLine',
    'B02D0MuMuD2KSHHDDBeauty2CharmLine',
    'B02D0MuMuWSD2KSHHDDBeauty2CharmLine',
    
    'B02DsKPiPiLTUBD2HHHBeauty2CharmLine',
        
    ],
    'RawEvents' : [
    #checked
    
    ],
    'MDSTChannels':[
    #checked
    
    ],
     'RelatedInfoTools' : [
      { "Type" : "RelInfoConeVariables", 
        "ConeAngle" : 1.5, 
        "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
        "Location"  : 'P2ConeVar1'
      }, 
      { "Type" : "RelInfoConeVariables", 
        "ConeAngle" : 1.7, 
        "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
        "Location"  : 'P2ConeVar2'
      }, 
      { "Type" : "RelInfoConeVariables", 
        "ConeAngle" : 1.0, 
        "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
        "Location"  : 'P2ConeVar3'
      }, 
    ],
    'D0INC' : {'PT_MIN' : 1000, 'IPCHI2_MIN': 100},
    "Prescales" : { # Prescales for individual lines
    'RUN_BY_DEFAULT' : True, # False = lines off by default
    'RUN_RE'         : ['.*'],  
    # Defaults are defined in, eg, Beauty2Charm_B2DXBuilder.py.  Put the full
    # line name here to override. E.g. 'B2D0HD2HHBeauty2CharmTOSLine':0.5.
    
    'B02DsKPiPiLTUBD2HHHBeauty2CharmLine' : 0.004,
    'B2Dstar0PiDst02D0Pi0D2KSHHLLWSBeauty2CharmLine' : 0.1,
    'B2Dstar0PiDst02D0Pi0D2KSHHDDWSBeauty2CharmLine' : 0.1,
    'B2Dstar0PiDst02D0GammaD2KSHHLLWSBeauty2CharmLine' : 0.1,
    'B2Dstar0PiDst02D0GammaD2KSHHDDWSBeauty2CharmLine' : 0.1,
    'B2Dstar0KDst02D0Pi0D2KSHHLLWSBeauty2CharmLine' : 0.1,
    'B2Dstar0KDst02D0Pi0D2KSHHDDWSBeauty2CharmLine' : 0.1,
    'B2Dstar0KDst02D0GammaD2KSHHLLWSBeauty2CharmLine' : 0.1,
    'B2Dstar0KDst02D0GammaD2KSHHDDWSBeauty2CharmLine' : 0.1
    

    },
    'GECNTrkMax'   : 500
  },
  'STREAMS' : { 
    'BhadronCompleteEvent' : [

    ##Alessandro's 2015 lines
    'StrippingB02DsstarPiDsstar2DGammaD2HHHBeauty2CharmLine',
    'StrippingB02DsstarKDsstar2DGammaD2HHHBeauty2CharmLine',
    'StrippingB02DsstarKMCwNoPIDDsstar2DGammaD2HHHBeauty2CharmLine',

    #Steve 8 GeV mass window checked
    #'StrippingLb2XicPiNoIPXic2PKPiBeauty2CharmLine',
    #'StrippingLb2XicPiNoIPWSXic2PKPiBeauty2CharmLine', 
    'StrippingOmegab2Omegac0PiNoIPOmegac02PKKPiBeauty2CharmLine',
    
    

    #Steve 9 GeV checked
    #'StrippingB2D0PiD2HHBeauty2CharmLine',

    #Move to DST checked
    'StrippingB02DDBeauty2CharmLine',

    #Move to DST checked
    'StrippingB02DstDBeauty2CharmLine',
    'StrippingB02DstDD02K3PiBeauty2CharmLine',
    'StrippingB02DstDWSBeauty2CharmLine',


    ],  
    'Bhadron' : [

    #Added after rate issues
    'StrippingLb2XicPiXic2PKPiBeauty2CharmLine',
    'StrippingLb2XicPiWSXic2PKPiBeauty2CharmLine',

    'StrippingB2D0PiD2HHmuDSTBeauty2CharmLine',

    ##Moved to Bhadron on last commit
    'StrippingOmegab2Omegac0PiNoIPWSOmegac02PKKPiBeauty2CharmLine', 


    #Steve 9 GeV checked
    'StrippingLb2D0PHD02HHBeauty2CharmLine',
    'StrippingLb2D0PHWSD02HHBeauty2CharmLine', 
    'StrippingX2LcKPiOSLc2PKPiBeauty2CharmLine', 
    'StrippingX2LcKPiSSLc2PKPiBeauty2CharmLine',
    'StrippingX2LcPiPiSSLc2PKPiBeauty2CharmLine',
    'StrippingX2LcKKSSLc2PKPiBeauty2CharmLine',
    'StrippingX2LcPiPiOSLc2PKPiBeauty2CharmLine',
    'StrippingX2LcPiKOSLc2PKPiBeauty2CharmLine',
    'StrippingX2LcKKOSLc2PKPiBeauty2CharmLine',

    'StrippingLb2LcKLc2PKPiBeauty2CharmLine',
    'StrippingLb2LcKWSLc2PKPiBeauty2CharmLine',
    'StrippingLb2LcPiLc2PKPiBeauty2CharmLine',
    'StrippingLb2LcPiWSLc2PKPiBeauty2CharmLine',
   

    #Steve 8 GeV checked
    'StrippingXibc2DpKD2HHHBeauty2CharmLine',

    #B->D0D*0barK checked
    'StrippingB2Dst0D0KDst02D0Pi0ResolvedD02HHBeauty2CharmLine',
    'StrippingB2Dst0D0KDst02D0GammaD02HHBeauty2CharmLine',
    'StrippingB02Dst0DKDst02D0Pi0ResolvedD2HHHBeauty2CharmLine', 
    'StrippingB02Dst0DKDst02D0GammaD2HHHBeauty2CharmLine',

    #Prescaled WS Susan checked
    'StrippingB2Dstar0PiDst02D0Pi0D2KSHHLLWSBeauty2CharmLine',
    'StrippingB2Dstar0PiDst02D0Pi0D2KSHHDDWSBeauty2CharmLine',
    'StrippingB2Dstar0PiDst02D0GammaD2KSHHLLWSBeauty2CharmLine',
    'StrippingB2Dstar0PiDst02D0GammaD2KSHHDDWSBeauty2CharmLine',
    'StrippingB2Dstar0KDst02D0Pi0D2KSHHLLWSBeauty2CharmLine',
    'StrippingB2Dstar0KDst02D0Pi0D2KSHHDDWSBeauty2CharmLine',
    'StrippingB2Dstar0KDst02D0GammaD2KSHHLLWSBeauty2CharmLine',
    'StrippingB2Dstar0KDst02D0GammaD2KSHHDDWSBeauty2CharmLine',

    #Now with prescale, left in Bhadron checked
    'StrippingB02DDWSBeauty2CharmLine',

    #Agnieszka checked
    'StrippingB02DsstKsPiLLDsst2DGammaD2HHHBeauty2CharmLine',
    'StrippingB02DsstKsPiDDDsst2DGammaD2HHHBeauty2CharmLine',

    #Lower gamma pT cut 
    'StrippingB2Dstar0KDst02D0GammaD2HHHHBeauty2CharmLine',
    'StrippingB2Dstar0PiDst02D0GammaD2HHHHBeauty2CharmLine',
    'StrippingB2Dstar0KDst02D0GammaD2KSHHLLBeauty2CharmLine',
    'StrippingB2Dstar0PiDst02D0GammaD2KSHHLLBeauty2CharmLine',
    'StrippingB2Dstar0KDst02D0GammaD2KSHHDDBeauty2CharmLine',
    'StrippingB2Dstar0PiDst02D0GammaD2KSHHDDBeauty2CharmLine',
    'StrippingBc2DstD0Dst2DGammaD2KSHD02HHBeauty2CharmLine',
    'StrippingBc2DstD0Dst2DGammaD2KSHD02KHHHBeauty2CharmLine',
    'StrippingBc2DstD0Dst2DGammaD2KSHD02HHPI0Beauty2CharmLine',
    'StrippingBc2DstD0Dst2DGammaD2KSHD02KSHHBeauty2CharmLine',
    'StrippingBc2DstD0Dst2DGammaD2KSHHHD02HHBeauty2CharmLine',
    'StrippingBc2DstD0Dst2DGammaD2KSHHHD02KHHHBeauty2CharmLine',
    'StrippingBc2DstD0Dst2DGammaD2KSHHHD02HHPI0Beauty2CharmLine',
    'StrippingBc2DstD0Dst2DGammaD2KSHHHD02KSHHBeauty2CharmLine',
    'StrippingBc2DstD0Dst2DGammaD2HHHD02HHBeauty2CharmLine',
    'StrippingBc2DstD0Dst2DGammaD2HHHD02KHHHBeauty2CharmLine',
    'StrippingBc2DstD0Dst2DGammaD2HHHD02HHPI0Beauty2CharmLine',
    'StrippingBc2DstD0Dst2DGammaD2HHHD02KSHHBeauty2CharmLine',
    'StrippingBc2DDst0D2KSHHHDst02D0GammaD02KSHHBeauty2CharmLine',
    'StrippingBc2DDst0D2KSHHHDst02D0GammaD02KHHHBeauty2CharmLine',
    'StrippingBc2DDst0D2KSHHHDst02D0GammaD02HHBeauty2CharmLine',
    'StrippingBc2DDst0D2KSHDst02D0GammaD02KSHHBeauty2CharmLine',
    'StrippingBc2DDst0D2KSHDst02D0GammaD02KHHHBeauty2CharmLine',
    'StrippingBc2DDst0D2KSHDst02D0GammaD02HHBeauty2CharmLine',
    'StrippingBc2DDst0D2HHHDst02D0GammaD02KSHHBeauty2CharmLine',
    'StrippingBc2DDst0D2HHHDst02D0GammaD02KHHHBeauty2CharmLine',
    'StrippingBc2DDst0D2HHHDst02D0GammaD02HHBeauty2CharmLine',
    'StrippingBc2DstDst0Dst2DPI0D2HHHDst02D0GammaD02KHHHBeauty2CharmLine',
    'StrippingBc2DstDst0Dst2DGammaD2KSHDst02D0GammaD02KHHHBeauty2CharmLine',
    'StrippingBc2DstDst0Dst2DGammaD2KSHDst02D0GammaD02HHBeauty2CharmLine',
    'StrippingBc2DstDst0Dst2D0PID02KHHHDst02D0GammaD02KSHHBeauty2CharmLine',
    'StrippingBc2DstDst0Dst2D0PID02KHHHDst02D0GammaD02KHHHBeauty2CharmLine',
    'StrippingBc2DstDst0Dst2D0PID02KHHHDst02D0GammaD02HHBeauty2CharmLine',
    'StrippingBc2DstDst0Dst2DGammaD2KSHHHDst02D0GammaD02KSHHBeauty2CharmLine',
    'StrippingBc2DstDst0Dst2DGammaD2KSHHHDst02D0GammaD02KHHHBeauty2CharmLine',
    'StrippingBc2DstDst0Dst2DGammaD2KSHHHDst02D0PI0D02HHBeauty2CharmLine',
    'StrippingBc2DstDst0Dst2DGammaD2KSHHHDst02D0PI0D02KHHHBeauty2CharmLine',
    'StrippingBc2DstDst0Dst2DGammaD2KSHHHDst02D0PI0D02KSHHBeauty2CharmLine',
    'StrippingBc2DstDst0Dst2DGammaD2KSHHHDst02D0GammaD02HHBeauty2CharmLine',
    'StrippingBc2DstDst0Dst2DGammaD2HHHDst02D0GammaD02KSHHBeauty2CharmLine',
    'StrippingBc2DstDst0Dst2DGammaD2HHHDst02D0GammaD02KHHHBeauty2CharmLine',
    'StrippingBc2DstDst0Dst2DGammaD2HHHDst02D0PI0D02HHBeauty2CharmLine',
    'StrippingBc2DstDst0Dst2DGammaD2HHHDst02D0PI0D02KHHHBeauty2CharmLine',
    'StrippingBc2DstDst0Dst2DGammaD2HHHDst02D0PI0D02KSHHBeauty2CharmLine',
    'StrippingBc2DstDst0Dst2DGammaD2HHHDst02D0GammaD02HHBeauty2CharmLine',
    'StrippingBc2DstDst0Dst2D0PID02HHDst02D0GammaD02KSHHBeauty2CharmLine',
    'StrippingBc2DstDst0Dst2D0PID02HHDst02D0GammaD02KHHHBeauty2CharmLine',
    'StrippingBc2DstDst0Dst2D0PID02HHDst02D0GammaD02HHBeauty2CharmLine',
    'StrippingBc2DstDst0Dst2D0PID02KSHHDst02D0GammaD02KHHHBeauty2CharmLine',
    'StrippingBc2DstDst0Dst2D0PID02KSHHDst02D0GammaD02HHBeauty2CharmLine',
    'StrippingBc2DstDst0Dst2DPI0D2KSHHHDst02D0GammaD02KSHHBeauty2CharmLine',
    'StrippingBc2DstDst0Dst2DPI0D2KSHHHDst02D0GammaD02KHHHBeauty2CharmLine',
    'StrippingBc2DstDst0Dst2DPI0D2KSHHHDst02D0GammaD02HHBeauty2CharmLine',

  
    
    #Alexandra checked
    'StrippingB2Dstar0PiDst02D0Pi0D2HHResolvedBeauty2CharmLine',
    'StrippingB2Dstar0KDst02D0Pi0D2HHResolvedBeauty2CharmLine',
    'StrippingB2Dstar0PiDst02D0GammaD2HHBeauty2CharmLine',
    'StrippingB2Dstar0KDst02D0GammaD2HHBeauty2CharmLine',

    #Donal checked
    'StrippingBc2D0PiD02HHPIDBeauty2CharmLine',
    'StrippingBc2D0PiD02HHHHPIDBeauty2CharmLine',    
    'StrippingBc2D0KD02HHPIDBeauty2CharmLine',
    'StrippingBc2D0KD02HHHHPIDBeauty2CharmLine',
    'StrippingBc2DPiPiD2HHHPIDBeauty2CharmLine',
    'StrippingBc2DHHWSD2HHHPIDBeauty2CharmLine',
    'StrippingBc2DHHNPD2HHHPIDBeauty2CharmLine',
    'StrippingBc2DKPiD2HHHPIDBeauty2CharmLine',
    'StrippingBc2DKKD2HHHPIDBeauty2CharmLine',
    'StrippingBc2DPiKD2HHHPIDBeauty2CharmLine',
    'StrippingBc2DstarPiPiDstar2D0PiD02HHPIDBeauty2CharmLine',
    'StrippingBc2DstarHHWSDstar2D0PiD02HHPIDBeauty2CharmLine',
    'StrippingBc2DstarHHNPDstar2D0PiD02HHPIDBeauty2CharmLine',
    'StrippingBc2DstarKPiDstar2D0PiD02HHPIDBeauty2CharmLine',
    'StrippingBc2DstarKKDstar2D0PiD02HHPIDBeauty2CharmLine',
    'StrippingBc2DstarPiKDstar2D0PiD02HHPIDBeauty2CharmLine',
    'StrippingBc2DstarPiPiDstar2D0PiD02HHHHPIDBeauty2CharmLine',
    'StrippingBc2DstarHHWSDstar2D0PiD02HHHHPIDBeauty2CharmLine',
    'StrippingBc2DstarHHNPDstar2D0PiD02HHHHPIDBeauty2CharmLine',
    'StrippingBc2DstarKPiDstar2D0PiD02HHHHPIDBeauty2CharmLine',
    'StrippingBc2DstarKKDstar2D0PiD02HHHHPIDBeauty2CharmLine',
    'StrippingBc2DstarPiKDstar2D0PiD02HHHHPIDBeauty2CharmLine',    
    'StrippingB2D0KD2KSMuMuLLBeauty2CharmLine',
    'StrippingB2D0PiD2KSMuMuLLBeauty2CharmLine',
    'StrippingB2D0KD2KSMuMuDDBeauty2CharmLine',
    'StrippingB2D0PiD2KSMuMuDDBeauty2CharmLine',
    'StrippingB2D0KD2KSMuMuLLWSBeauty2CharmLine',
    'StrippingB2D0PiD2KSMuMuLLWSBeauty2CharmLine',
    'StrippingB2D0KD2KSMuMuDDWSBeauty2CharmLine',
    'StrippingB2D0PiD2KSMuMuDDWSBeauty2CharmLine',   
    'StrippingB2D0KD02KSPi0LLResolvedBeauty2CharmLine',
    'StrippingB2D0PiD02KSPi0LLResolvedBeauty2CharmLine',
    'StrippingB2D0KD02KSPi0LLMergedBeauty2CharmLine',
    'StrippingB2D0PiD02KSPi0LLMergedBeauty2CharmLine',
    'StrippingB2D0KD02KSPi0DDResolvedBeauty2CharmLine',
    'StrippingB2D0PiD02KSPi0DDResolvedBeauty2CharmLine',
    'StrippingB2D0KD02KSPi0DDMergedBeauty2CharmLine',
    'StrippingB2D0PiD02KSPi0DDMergedBeauty2CharmLine',
    'StrippingB2DMuMuD2HHHBeauty2CharmLine',
    'StrippingB2DMuMuWSD2HHHBeauty2CharmLine',
    'StrippingB2DMuMuNPD2HHHBeauty2CharmLine',
    'StrippingB2DMuMuD2KSHLLBeauty2CharmLine',
    'StrippingB2DMuMuWSD2KSHLLBeauty2CharmLine',
    'StrippingB2DMuMuNPD2KSHLLBeauty2CharmLine',
    'StrippingB2DMuMuD2KSHDDBeauty2CharmLine',
    'StrippingB2DMuMuWSD2KSHDDBeauty2CharmLine',
    'StrippingB2DMuMuNPD2KSHDDBeauty2CharmLine',
    'StrippingB2DstarMuMuD0PiBeauty2CharmLine',
    'StrippingB2DstarMuMuWSD0PiBeauty2CharmLine',
    'StrippingB2DstarMuMuNPD0PiBeauty2CharmLine',
    'StrippingB2DstarMuMuD2HHHHPiBeauty2CharmLine',
    'StrippingB2DstarMuMuWSD2HHHHPiBeauty2CharmLine',
    'StrippingB2DstarMuMuNPD2HHHHPiBeauty2CharmLine',
    'StrippingB02D0MuMuD2HHBeauty2CharmLine',
    'StrippingB02D0MuMuWSD2HHBeauty2CharmLine',
    'StrippingB02D0MuMuD2HHHHBeauty2CharmLine',
    'StrippingB02D0MuMuWSD2HHHHBeauty2CharmLine',
    'StrippingB02D0MuMuD2KSHHLLBeauty2CharmLine',
    'StrippingB02D0MuMuWSD2KSHHLLBeauty2CharmLine',
    'StrippingB02D0MuMuD2KSHHDDBeauty2CharmLine',
    'StrippingB02D0MuMuWSD2KSHHDDBeauty2CharmLine',
    
    
    #Tim Gershon lines checked
    'StrippingB2DstPiKFavD0PiBeauty2CharmLine',
    'StrippingB2DstPiKFavD2HHHHPiBeauty2CharmLine',
    'StrippingB2DstPiKSupD0PiBeauty2CharmLine',
    'StrippingB2DstPiKSupD2HHHHPiBeauty2CharmLine',
    'StrippingB2DstPipPipD0PiBeauty2CharmLine',
    'StrippingB2DstPipPipD2HHHHPiBeauty2CharmLine',
    'StrippingB2DstPimPipD0PiBeauty2CharmLine',
    'StrippingB2DstPimPipD2HHHHPiBeauty2CharmLine',
    'StrippingB2DstPiKWSD0PiBeauty2CharmLine',
    'StrippingB2DstPiKWSD2HHHHPiBeauty2CharmLine',
    'StrippingB2DstPiPiWSD0PiBeauty2CharmLine',
    'StrippingB2DstPiPiWSD2HHHHPiBeauty2CharmLine',
    
    #Xibc
    'StrippingXibc2LcD0D02KPiBeauty2CharmLine',
    'StrippingXibc2LcD0PiD02KPiBeauty2CharmLine',
    'StrippingXibc2D0D0pD02KPiBeauty2CharmLine', 
    'StrippingXibc2LcPiPiOSLc2PKPiBeauty2CharmLine',
    'StrippingXibc2LcKPiOSLc2PKPiBeauty2CharmLine',
    'StrippingXibc2LcPiKOSLc2PKPiBeauty2CharmLine',
    'StrippingXibc02XiccpPiXiccp2Xic0PiBeauty2CharmLine',
    'StrippingXibc2XiccppPiXiccpp2XicPiBeauty2CharmLine',
    'StrippingXibc2LcPiLc2PKPiBeauty2CharmLine',
    'StrippingXibc2LcKLc2PKPiBeauty2CharmLine',
    'StrippingXibc2XicPiXic2PKPiBeauty2CharmLine',
    'StrippingXibc2Xic0PiXic02PKKPiBeauty2CharmLine',
    'StrippingXibc02XiccpMuXiccp2Xic0PiBeauty2CharmLine',
    'StrippingXibc2XiccppMuXiccpp2XicPiBeauty2CharmLine',
    'StrippingXibc02XiccpMuWSXiccp2Xic0PiBeauty2CharmLine',
    'StrippingXibc2XiccppMuWSXiccpp2XicPiBeauty2CharmLine',
    'StrippingX2Omegac0PiOmegac02PKKPiBeauty2CharmLine',
    'StrippingX2Omegac0KOmegac02PKKPiBeauty2CharmLine',
    'StrippingX2Omegac0KOSOmegac02PKKPiBeauty2CharmLine',
    'StrippingXibc2LcD0MuD02KPiBeauty2CharmLine',
    'StrippingXibc2LcD0MuWSD02KPiBeauty2CharmLine',
    'StrippingXibc2LcDMuD2KPiPiBeauty2CharmLine',
    'StrippingXibc2LcDMuWSD2KPiPiBeauty2CharmLine',
    'StrippingXib2Xic0PiXic02PKKPiBeauty2CharmLine',

    ######################
    ###### DDX lines #####
    ######################
    #### DDK lines ###
    # Lines containing S28 bug for S24r0p1 only
    'StrippingB2DDK_S28BugBeauty2CharmLine',
    'StrippingB2DDKWS_S28BugBeauty2CharmLine',
    'StrippingB02D0DK_S28BugBeauty2CharmLine',
    'StrippingB02D0DK_S28BugD02K3PiBeauty2CharmLine',
    'StrippingB02D0DKWS_S28BugBeauty2CharmLine',
    'StrippingB02D0DKWS_S28BugD02K3PiBeauty2CharmLine',
    'StrippingB2DstDK_S28BugBeauty2CharmLine',
    'StrippingB2DstDK_S28BugDstarD02K3PiBeauty2CharmLine',
    'StrippingB2DstDKWS_S28BugBeauty2CharmLine',
    'StrippingB2DstDKWS_S28BugDstarD02K3PiBeauty2CharmLine',
    'StrippingB2DDPi_S28BugBeauty2CharmLine',
    'StrippingB2DDPiWS_S28BugBeauty2CharmLine',
    'StrippingB02D0DPi_S28BugBeauty2CharmLine',
    'StrippingB02D0DPi_S28BugD02K3PiBeauty2CharmLine',
    'StrippingB02D0DPiWS_S28BugBeauty2CharmLine',
    'StrippingB02D0DPiWS_S28BugD02K3PiBeauty2CharmLine',
    'StrippingB2DstDPi_S28BugBeauty2CharmLine',
    'StrippingB2DstDPi_S28BugDstarD02K3PiBeauty2CharmLine',
    'StrippingB2DstDPiWS_S28BugBeauty2CharmLine',
    'StrippingB2DstDPiWS_S28BugDstarD02K3PiBeauty2CharmLine',
    #

    # LTU line for the DsKPiPi analysis
    'StrippingB02DsKPiPiLTUBD2HHHBeauty2CharmLine',

    # Add for Nathan's filtered MC request
    'StrippingB2Dstar0KDst02D0Pi0D2KSHHDDBeauty2CharmLine',
    'StrippingB2Dstar0KDst02D0Pi0D2KSHHLLBeauty2CharmLine',
    'StrippingB2Dstar0PiDst02D0Pi0D2KSHHDDBeauty2CharmLine',
    'StrippingB2Dstar0PiDst02D0Pi0D2KSHHLLBeauty2CharmLine',

    ]
  },
  'WGs' : [ 'B2OC' ]
}

Lb2LambdacPi  = {
    'BUILDERTYPE'       :  'Lb2LambdacPiConf',
    'CONFIG'    : {
        'KaonCuts'      : "(TRCHI2DOF < 4.) & (PT > 100*MeV) & (P > 1000*MeV) & (MIPCHI2DV(PRIMARY) > 4.) & (TRGHOSTPROB < 0.4) & (PIDK > -10.)",
        'ProtonCuts'      : "(TRCHI2DOF < 4.) & (PT > 100*MeV) & (P > 1000*MeV) & (MIPCHI2DV(PRIMARY) > 4.) & (TRGHOSTPROB < 0.4) & (PIDp > -10.)",
        'PionCuts'      : "(TRCHI2DOF < 4.) & (PT > 100*MeV) & (P > 1000*MeV) & (MIPCHI2DV(PRIMARY) > 4.) & (TRGHOSTPROB < 0.4) & (PIDK < 20.)",
        #'KaonCuts'      : "(PROBNNk > 0.1) & (PT > 300*MeV) & (TRGHOSTPROB<0.4)",
        #'ProtonCuts'    : "(PROBNNp > 0.1) & (PT > 300*MeV) & (P > 10*GeV) & (TRGHOSTPROB<0.4)",        
        #'PionCuts'      : "(PROBNNpi > 0.1) & (PT > 250*MeV) & (TRGHOSTPROB<0.4)",
        'LambdacComAMCuts' : "(AM<2.375*GeV)",
        'LambdacComN4Cuts' : """
                          (in_range(2.195*GeV, AM, 2.375*GeV))
                          & ( (ACHILD(PT,1)+ACHILD(PT,2)+ACHILD(PT,3)+ACHILD(PT,4) ) > 2.5 *GeV)
                          & ( (ACHILD(MIPCHI2DV(), 1) + ACHILD(MIPCHI2DV(), 2) + ACHILD(MIPCHI2DV(), 3) + ACHILD(MIPCHI2DV(), 4))>30)
                          """,
        'LambdacMomN4Cuts' : """
                           (VFASPF(VCHI2/VDOF) < 10.) 
                           & (BPVDIRA> 0.) 
                           & (in_range(2.185*GeV, MM, 2.385*GeV)) 
                           & (BPVVDCHI2>36) 
                           """,
        'LambdacComCuts'   : "(in_range(2.195*GeV, AM, 2.375*GeV))",
        'Lambda0Cuts'        : "(ADMASS('Lambda0') < 50.*MeV) & (PT > 250*MeV) & (BPVVDCHI2 >36)",
        'LbComCuts'     : "(ADAMASS('Lambda_b0') < 500 *MeV)",
        'LbMomCuts'     : """
                          (VFASPF(VCHI2/VDOF) < 10.) 
                          & (BPVDIRA> 0.999) 
                          & (BPVIPCHI2()<25) 
                          & (BPVVDCHI2>250)
                          & (BPVVDRHO>0.1*mm) 
                          & (BPVVDZ>2.0*mm)
                          """,
        'Prescale'      : 1.,
        'RelatedInfoTools': [{
                      'Type'              : 'RelInfoVertexIsolation',
                      'Location'          : 'RelInfoVertexIsolation'
                  }, {
                      'Type'              : 'RelInfoVertexIsolationBDT',
                      'Location'          : 'RelInfoVertexIsolationBDT'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 1.0,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                      'Location'          : 'RelInfoConeVariables_1.0'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 1.5,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                      'Location'          : 'RelInfoConeVariables_1.5'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 2.0,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                      'Location'          : 'RelInfoConeVariables_2.0'
                  }]        
        },
    'STREAMS'           : ['Bhadron' ],
    'WGs'               : ['B2OC'],
    }
