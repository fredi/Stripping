###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
##                          S T R I P P I N G  2 4 r 1                        ##
##                                                                            ##
##  Configuration for B2CC WG                                                 ##
##  Contact person: Xuesong Liu           (xuesong.liu@cern.ch)               ##
################################################################################

from GaudiKernel.SystemOfUnits import *

######################################################################
## StrippingB2JpsiHHBs2Jpsif0PrescaledLine (MicroDST)
## StrippingB2JpsiHHBs2JpsiKstarLin(MicroDST)
## StrippingB2JpsiHHLb2JpsipHLine(MicroDST)
## StrippingB2JpsiHHBs2Jpsif0Line (FullDST)
## StrippingB2JpsiHHBs2Jpsif0KaonLine(FullDST)
## StrippingB2JpsiHHBs2Jpsif0wsLine(FullDST)
## -------------------------------------------------------------------
## Lines defined in StrippingB2JpsiHH.py
## Authors: Liming Zhang, Xuesong Liu
## Last changes made by Xuesong Liu
######################################################################

B2JpsiHH = {
    "BUILDERTYPE": "B2JpsiHHConf",
    "CONFIG"     : {
                   "Bs2Jpsif0Prescale" : 0.3,
                   "HLTCuts"           : "HLT_PASS_RE('Hlt2DiMuonJPsiDecision')",
                   "JpsiMassWindow"    : 80,
                   "MVACut"            : "-0.1",
                   "TRCHI2DOF"         : 5,
                   "VCHI2PDOF"         : 10,
                   "XmlFile"           : "$TMVAWEIGHTSROOT/data/Bs2Jpsif0_BDT_v1r1.xml"
                   },
    "STREAMS"    : {
                   "Dimuon"   : [
                                "StrippingB2JpsiHHBs2Jpsif0Line",
                                "StrippingB2JpsiHHBs2Jpsif0KaonLine",
                                "StrippingB2JpsiHHBs2Jpsif0wsLine"
                                ],
                   "Leptonic" : [
                                "StrippingB2JpsiHHBs2Jpsif0PrescaledLine",
                                "StrippingB2JpsiHHBs2JpsiKstarLine",
                                "StrippingB2JpsiHHLb2JpsipHLine"
                                ]
                   },
    "WGs": [ "B2CC" ]
    }


######################################################################
## StrippingBetaSBs2JpsieePhiDetachedLine (MicroDST)
## StrippingBetaSBs2JpsieePhiFromTracksLine (MicroDST)
## StrippingBetaSBs2JpsieePhiLine (MicroDST)
## -------------------------------------------------------------------
## Lines defined in StrippingBs2JpsieePhi.py
## Authors: Artur Ukleja, Jibo He, Konrad Klimaszewski
## Last changes made by Konrad Klimaszewski
######################################################################

BetaSBs2JpsieePhi = {
    "BUILDERTYPE": "Bs2JpsieePhiConf",
    "CONFIG"     : {
                   "BsDIRA"                      : 0.99,
                   "BsMassMax"                   : 6000.0,
                   "BsMassMaxLoose"              : 6000.0,
                   "BsMassMin"                   : 3600.0,
                   "BsMassMinLoose"              : 3600.0,
                   "BsVertexCHI2pDOF"            : 10.0,
                   "BsVertexCHI2pDOFLoose"       : 10.0,
                   "ElectronPID"                 : 0.0,
                   "ElectronPIDLoose"            : 0.0,
                   "ElectronPT"                  : 500.0,
                   "ElectronPTLoose"             : 500.0,
                   "ElectronTrackCHI2pDOF"       : 5.0,
                   "ElectronTrackCHI2pDOFLoose"  : 5.0,
                   "JpsiMassMax"                 : 3600.0,
                   "JpsiMassMaxLoose"            : 3600.0,
                   "JpsiMassMin"                 : 1700.0,
                   "JpsiMassMinLoose"            : 1700.0,
                   "JpsiPT"                      : 400.0,
                   "JpsiPTLoose"                 : 400.0,

                   "JpsiVertexCHI2pDOF"          : 15.0,
                   "JpsiVertexCHI2pDOFLoose"     : 15.0,
                   "KaonPID"                     : -3.0,
                   "KaonPIDLoose"                : -3.0,
                   "KaonTrackCHI2pDOF"           : 5.0,
                   "KaonTrackCHI2pDOFLoose"      : 5.0,
                   "LifetimeCut"                 : " & (BPVLTIME()>0.3*ps)",
                   "PhiMassMax"                  : 1050.0,
                   "PhiMassMaxLoose"             : 1050.0,
                   "PhiMassMin"                  : 990.0,
                   "PhiMassMinLoose"             : 990.0,
                   "PhiPT"                       : 1000.0,
                   "PhiPTLoose"                  : 1000.0,
                   "PhiVertexCHI2pDOF"           : 15.0,
                   "PhiVertexCHI2pDOFLoose"      : 15.0,
                   "Prescale"                    : 0.1,
                   "PrescaleLoose"               : 1.0
                   },
    "STREAMS"    : [ "Leptonic" ],
    "WGs"        : [ "B2CC" ]
    }

######################################################################
## StrippingBs2EtacPhiBDTLine (MicroDST)
## StrippingBs2EtacPhiBDT_KsKPiLine (MicroDST)
## -------------------------------------------------------------------
## Lines defined in StrippingBs2EtacPhiBDT.py
## Authors: Morgan Martin, Jibo He
## Last changes made by Jibo He
######################################################################

Bs2EtacPhiBDT = {
      "BUILDERTYPE"     : "Bs2EtacPhiBDTConf",
      "CONFIG"          : {  "Bs2EtacPhiMVACut"       : "-0.1",
                             "Bs2EtacPhiXmlFile"      : "$TMVAWEIGHTSROOT/data/Bs2EtacPhi_BDT_v1r0.xml",
                             "BsComCuts"              : "(ADAMASS('B_s0') < 500 *MeV)",
                             "BsMomCuts"              : "(VFASPF(VCHI2/VDOF) < 25.) & (BPVDIRA> 0.99) & (BPVIPCHI2()<25) & (BPVDLS>0)",
                             "EtacComAMCuts"          : "(AM<3.25*GeV)",
                             "EtacComCuts"            : "(in_range(2.75*GeV, AM, 3.25*GeV))",
                             "EtacComN4Cuts"          : "(in_range(2.75*GeV, AM, 3.25*GeV)) & ( (ACHILD(PT,1)+ACHILD(PT,2)+ACHILD(PT,3)+ACHILD(PT,4) ) > 2.5 *GeV) & ( (ACHILD(MIPCHI2DV(), 1) + ACHILD(MIPCHI2DV(), 2) + ACHILD(MIPCHI2DV(), 3) + ACHILD(MIPCHI2DV(), 4))>30) ",
                             "EtacMomN4Cuts"          : "(VFASPF(VCHI2/VDOF) < 9.) & (in_range(2.8*GeV, MM, 3.2*GeV)) & (MIPCHI2DV(PRIMARY) > 2.)",
                             "HLTCuts"                : "(HLT_PASS_RE('Hlt2Topo.*Decision') | HLT_PASS_RE('Hlt2IncPhi.*Decision'))",
                             "KaonCuts"               : "(PROBNNk > 0.13) & (PT > 300*MeV) & (TRGHOSTPROB<0.4)",
                             "KsCuts"                 : "(ADMASS('KS0') < 30.*MeV) & (BPVDLS>5)",
                             "PhiCuts"                : "(MAXTREE(ABSID=='K+',TRGHOSTPROB) < 0.4) & (MM<1.05*GeV)& (PT> 800*MeV)& (MIPCHI2DV(PRIMARY) > 2.) & (VFASPF(VCHI2) < 9.)",
                             "PionCuts"               : "(PROBNNpi > 0.2) & (PT > 250*MeV) & (TRGHOSTPROB<0.4)",
                             "Prescale"               : 1.0
                          },
      "STREAMS"         : [ "Bhadron" ],
      "WGs"             : [ "B2CC" ]
                }

