###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
##                          S T R I P P I N G  2 4 r 1                        ##
##                                                                            ##
##  Configuration for SL WG                                                   ##
##  Contact person: Greg Ciezarek (gregory.max.ciezarek@cern.ch)              ##
################################################################################

from GaudiKernel.SystemOfUnits import *
B2DMuForLNu = {
    "BUILDERTYPE": "B2DMuForLNuconf", 
    "CONFIG": {
        "BDIRA": 0.999, 
        "BVCHI2DOF": 6.0, 
        "DSumPT": 2500.0, 
        "DsAMassWin": 100.0, 
        "DsDIRA": 0.999, 
        "DsFDCHI2": 50.0, 
        "DsMassWin": 80.0, 
        "DsVCHI2DOF": 4.0, 
        "FakePrescale": 0.1, 
        "GhostProb": 0.5, 
        "Hlt2Line": "HLT_PASS_RE('Hlt2Topo.*Decision')", 
        "KPiPT": 500.0, 
        "KaonPIDK": 4.0, 
        "LeptonIPCHI2": 9.0, 
        "LeptonPT": 300, 
        "MINIPCHI2": 16.0, 
        "M_MAX": 2010.0, 
        "M_MIN": 1920.0, 
        "PIDe": 0.0, 
        "PIDmu": 3.0, 
        "PIDmuK": 0.0, 
        "PIDmuP": 0.0, 
        "PionPIDKTight": 2.0, 
        "SPDmax": 600
    }, 
    "STREAMS": [ "Semileptonic" ], 
    "WGs": [ "Semileptonic" ]
}
B2PPbarMuForTauMu = {
    "BUILDERTYPE": "B2PPbarMuForTauMuBuilder", 
    "CONFIG": {
        "BDIRA": 0.999, 
        "BFDCHI2HIGH": 50.0, 
        "BVCHI2DOF": 15.0, 
        "DECAYS": [
            "[B- -> J/psi(1S) mu-]cc"
        ], 
        "GEC_nSPDHits": 600.0, 
        "MuonGHOSTPROB": 0.35, 
        "MuonMINIPCHI2": 16.0, 
        "MuonP": 3000.0, 
        "MuonTRCHI2": 4.0, 
        "PPbarDIRA": 0.999, 
        "PPbarFDCHI2HIGH": 25.0, 
        "PPbarVCHI2DOF": 10.0, 
        "ProtonMINIPCHI2": 9.0, 
        "ProtonP": 15000.0, 
        "ProtonPIDK": 2.0, 
        "ProtonPIDp": 2.0, 
        "ProtonPT": 800.0, 
        "ProtonTRCHI2": 6.0, 
        "SSDECAYS": [
            "[B- -> J/psi(1S) mu-]cc"
        ], 
        "TRGHOSTPROB": 0.35, 
        "pMuMassLower": 1000.0, 
        "ppMuPT": 1500.0
    }, 
    "STREAMS": [ "Semileptonic" ], 
    "WGs": [ "Semileptonic" ]
}

B2XuMuNu = {
    "BUILDERTYPE": "B2XuMuNuBuilder", 
    "CONFIG": {
        "BCorrMHigh": 7000.0, 
        "BCorrMLow": 2500.0, 
        "BDIRA": 0.99, 
        "BDIRAMed": 0.994, 
        "BDIRATight": 0.999, 
        "BFDCHI2ForPhi": 50.0, 
        "BFDCHI2HIGH": 100.0, 
        "BFDCHI2Tight": 120.0, 
        "BVCHI2DOF": 4.0, 
        "BVCHI2DOFTight": 2.0, 
        "Enu": 1850.0, 
        "EnuK": 2000.0, 
        "GEC_nLongTrk": 250.0, 
        "KMuMassLowTight": 1500.0, 
        "KS0DaugMIPChi2": 50.0, 
        "KS0DaugP": 2000.0, 
        "KS0DaugPT": 250.0, 
        "KS0DaugTrackChi2": 4.0, 
        "KS0MIPChi2": 8.0, 
        "KS0PT": 700.0, 
        "KS0VertexChi2": 10.0, 
        "KSLLCutFDChi2": 100.0, 
        "KSLLMassHigh": 536.0, 
        "KSLLMassLow": 456.0, 
        "KSMajoranaCutFDChi2": 100.0, 
        "KaonMINIPCHI2": 16, 
        "KaonP": 3000.0, 
        "KaonPIDK": 5.0, 
        "KaonPIDK_phi": 0.0, 
        "KaonPIDmu": 5.0, 
        "KaonPIDmu_phi": -2.0, 
        "KaonPIDp": 5.0, 
        "KaonPIDp_phi": -2.0, 
        "KaonPT": 500.0, 
        "KaonPTight": 10000.0, 
        "KaonTRCHI2": 6.0, 
        "KsLLCutFD": 20.0, 
        "KsLLMaxDz": 650.0, 
        "KshMuMassLowTight": 3000.0, 
        "KshZ": 0.0, 
        "KstarChPionMINIPCHI2": 9.0, 
        "KstarChPionP": 2000.0, 
        "KstarChPionPIDK": -10.0, 
        "KstarChPionPT": 100.0, 
        "KstarChPionTRCHI2": 10.0, 
        "KstarMuMassLowTight": 2500.0, 
        "MuonGHOSTPROB": 0.35, 
        "MuonMINIPCHI2": 12, 
        "MuonP": 3000.0, 
        "MuonPIDK": 0.0, 
        "MuonPIDmu": 3.0, 
        "MuonPIDp": 0.0, 
        "MuonPT": 1000.0, 
        "MuonPTTight": 1500.0, 
        "MuonPTight": 6000.0, 
        "MuonTRCHI2": 4.0, 
        "PhiDIRA": 0.9, 
        "PhiMINIPCHI2": 9, 
        "PhiMu_MCORR": 2500.0, 
        "PhiPT": 500.0, 
        "PhiUpperMass": 1500.0, 
        "PhiVCHI2DOF": 6, 
        "PiMuNu_prescale": 0.1, 
        "RhoChPionMINIPCHI2": 9.0, 
        "RhoChPionPIDK": -10.0, 
        "RhoChPionPT": 300.0, 
        "RhoChPionTRCHI2": 10.0, 
        "RhoDIRA": 0.9, 
        "RhoLeadingPionPT": 800.0, 
        "RhoMINIPCHI2": 4, 
        "RhoMassWindow": 150.0, 
        "RhoMassWindowMax1SB": 620.0, 
        "RhoMassWindowMax2SB": 1200.0, 
        "RhoMassWindowMin1SB": 300.0, 
        "RhoMassWindowMin2SB": 920.0, 
        "RhoMuMassLowTight": 2000.0, 
        "RhoPT": 500.0, 
        "RhoVCHI2DOF": 6, 
        "TRGHOSTPROB": 0.5, 
        "XMuMassUpper": 5500.0, 
        "XMuMassUpperHigh": 6500.0
    }, 
    'STREAMS' : {
    'Semileptonic' : [
        'StrippingB2XuMuNuB2Phi_FakeMuLine']
    },
    "WGs": [ "Semileptonic" ]
}


bhad2PMuX = {
    "BUILDERTYPE": "bhad2PMuXBuilder", 
    "CONFIG": {
        "BCORRM": 2500.0, 
        "BDIRA": 0.9994, 
        "BFDCHI2HIGH": 150.0, 
        "BVCHI2DOF": 2.0, 
        "DECAYS": [
            "[Lambda_b0 -> p+ mu-]cc"
        ], 
        "GEC_nLongTrk": 250.0, 
        "MuonGHOSTPROB": 0.35, 
        "MuonMINIPCHI2": 16.0, 
        "MuonP": 3000.0, 
        "MuonPT": 1500.0, 
        "MuonTRCHI2": 4.0, 
        "NstarMass": 3000.0, 
        "NstarVCHI2DOF": 2.0, 
        "PPbarDIRA": 0.994, 
        "PPbarFDCHI2HIGH": 150.0, 
        "PPbarPT": 1500.0, 
        "PPbarVCHI2DOF": 4.0, 
        "ProtonMINIPCHI2": 16.0, 
        "ProtonP": 15000.0, 
        "ProtonPIDK": 10.0, 
        "ProtonPIDp": 10.0, 
        "ProtonPT": 1000.0, 
        "ProtonTRCHI2": 6.0, 
        "SSDECAYS": [
            "[Lambda_b0 -> p+ mu+]cc"
        ], 
        "TRGHOSTPROB": 0.35, 
        "pMuMassLower": 1000.0, 
        "pMuPT": 1500.0
    }, 
    "STREAMS": [ "Semileptonic" ], 
    "WGs": [ "Semileptonic" ]
}

