###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
##################################################################################
##                          S T R I P P I N G  21r0p1                           ##
##                                                                              ##
##  Configuration for QEE WG                                                    ##
##  Contact person: Chitsanu Khurewathanakul (chitsanu.khurewathanakul@cern.ch) ##
##################################################################################

from GaudiKernel.SystemOfUnits import *

Ditau = {
    "BUILDERTYPE": "DitauConf", 
    "CONFIG": {
        "CONSTRUCTORS": {
            "EX": {
                "RelatedInfoTools": [
                    {
                        "ConeAngle": 0.5, 
                        "DaughterLocations": {
                            " Z0 -> ^e+   X": "taueplus", 
                            " Z0 -> ^e-   X": "taueminus", 
                            " Z0 -> ^mu+  X": "taumuplus", 
                            " Z0 -> ^mu-  X": "taumuminus", 
                            " Z0 -> ^pi+  X": "tauh1plus", 
                            " Z0 -> ^pi-  X": "tauh1minus", 
                            " Z0 -> ^tau+ X": "tauh3plus", 
                            " Z0 -> ^tau- X": "tauh3minus", 
                            "[Z0 ->  e-   ^e-   ]CC": "taue2", 
                            "[Z0 ->  mu-  ^mu-  ]CC": "taumu2", 
                            "[Z0 ->  pi-  ^pi-  ]CC": "tauh12", 
                            "[Z0 ->  tau- ^tau- ]CC": "tauh32", 
                            "[Z0 -> ^e-    e-   ]CC": "taue1", 
                            "[Z0 -> ^mu-   mu-  ]CC": "taumu1", 
                            "[Z0 -> ^pi-   pi-  ]CC": "tauh11", 
                            "[Z0 -> ^tau-  tau- ]CC": "tauh31"
                        }, 
                        "IgnoreUnmatchedDescriptors": True, 
                        "Type": "RelInfoConeVariables"
                    }
                ], 
                "prescale": 1.0
            }, 
            "EXnoiso": {
                "RelatedInfoTools": [
                    {
                        "ConeAngle": 0.5, 
                        "DaughterLocations": {
                            " Z0 -> ^e+   X": "taueplus", 
                            " Z0 -> ^e-   X": "taueminus", 
                            " Z0 -> ^mu+  X": "taumuplus", 
                            " Z0 -> ^mu-  X": "taumuminus", 
                            " Z0 -> ^pi+  X": "tauh1plus", 
                            " Z0 -> ^pi-  X": "tauh1minus", 
                            " Z0 -> ^tau+ X": "tauh3plus", 
                            " Z0 -> ^tau- X": "tauh3minus", 
                            "[Z0 ->  e-   ^e-   ]CC": "taue2", 
                            "[Z0 ->  mu-  ^mu-  ]CC": "taumu2", 
                            "[Z0 ->  pi-  ^pi-  ]CC": "tauh12", 
                            "[Z0 ->  tau- ^tau- ]CC": "tauh32", 
                            "[Z0 -> ^e-    e-   ]CC": "taue1", 
                            "[Z0 -> ^mu-   mu-  ]CC": "taumu1", 
                            "[Z0 -> ^pi-   pi-  ]CC": "tauh11", 
                            "[Z0 -> ^tau-  tau- ]CC": "tauh31"
                        }, 
                        "IgnoreUnmatchedDescriptors": True, 
                        "Type": "RelInfoConeVariables"
                    }
                ], 
                "prescale": 0.05
            }, 
            "EXssnoiso": {
                "RelatedInfoTools": [
                    {
                        "ConeAngle": 0.5, 
                        "DaughterLocations": {
                            " Z0 -> ^e+   X": "taueplus", 
                            " Z0 -> ^e-   X": "taueminus", 
                            " Z0 -> ^mu+  X": "taumuplus", 
                            " Z0 -> ^mu-  X": "taumuminus", 
                            " Z0 -> ^pi+  X": "tauh1plus", 
                            " Z0 -> ^pi-  X": "tauh1minus", 
                            " Z0 -> ^tau+ X": "tauh3plus", 
                            " Z0 -> ^tau- X": "tauh3minus", 
                            "[Z0 ->  e-   ^e-   ]CC": "taue2", 
                            "[Z0 ->  mu-  ^mu-  ]CC": "taumu2", 
                            "[Z0 ->  pi-  ^pi-  ]CC": "tauh12", 
                            "[Z0 ->  tau- ^tau- ]CC": "tauh32", 
                            "[Z0 -> ^e-    e-   ]CC": "taue1", 
                            "[Z0 -> ^mu-   mu-  ]CC": "taumu1", 
                            "[Z0 -> ^pi-   pi-  ]CC": "tauh11", 
                            "[Z0 -> ^tau-  tau- ]CC": "tauh31"
                        }, 
                        "IgnoreUnmatchedDescriptors": True, 
                        "Type": "RelInfoConeVariables"
                    }
                ], 
                "prescale": 0.05
            }, 
            "HH": {
                "RelatedInfoTools": [
                    {
                        "ConeAngle": 0.5, 
                        "DaughterLocations": {
                            " Z0 -> ^e+   X": "taueplus", 
                            " Z0 -> ^e-   X": "taueminus", 
                            " Z0 -> ^mu+  X": "taumuplus", 
                            " Z0 -> ^mu-  X": "taumuminus", 
                            " Z0 -> ^pi+  X": "tauh1plus", 
                            " Z0 -> ^pi-  X": "tauh1minus", 
                            " Z0 -> ^tau+ X": "tauh3plus", 
                            " Z0 -> ^tau- X": "tauh3minus", 
                            "[Z0 ->  e-   ^e-   ]CC": "taue2", 
                            "[Z0 ->  mu-  ^mu-  ]CC": "taumu2", 
                            "[Z0 ->  pi-  ^pi-  ]CC": "tauh12", 
                            "[Z0 ->  tau- ^tau- ]CC": "tauh32", 
                            "[Z0 -> ^e-    e-   ]CC": "taue1", 
                            "[Z0 -> ^mu-   mu-  ]CC": "taumu1", 
                            "[Z0 -> ^pi-   pi-  ]CC": "tauh11", 
                            "[Z0 -> ^tau-  tau- ]CC": "tauh31"
                        }, 
                        "IgnoreUnmatchedDescriptors": True, 
                        "Type": "RelInfoConeVariables"
                    }
                ], 
                "prescale": 0.1,
            }, 
            "HHnoiso": {
                "RelatedInfoTools": [
                    {
                        "ConeAngle": 0.5, 
                        "DaughterLocations": {
                            " Z0 -> ^e+   X": "taueplus", 
                            " Z0 -> ^e-   X": "taueminus", 
                            " Z0 -> ^mu+  X": "taumuplus", 
                            " Z0 -> ^mu-  X": "taumuminus", 
                            " Z0 -> ^pi+  X": "tauh1plus", 
                            " Z0 -> ^pi-  X": "tauh1minus", 
                            " Z0 -> ^tau+ X": "tauh3plus", 
                            " Z0 -> ^tau- X": "tauh3minus", 
                            "[Z0 ->  e-   ^e-   ]CC": "taue2", 
                            "[Z0 ->  mu-  ^mu-  ]CC": "taumu2", 
                            "[Z0 ->  pi-  ^pi-  ]CC": "tauh12", 
                            "[Z0 ->  tau- ^tau- ]CC": "tauh32", 
                            "[Z0 -> ^e-    e-   ]CC": "taue1", 
                            "[Z0 -> ^mu-   mu-  ]CC": "taumu1", 
                            "[Z0 -> ^pi-   pi-  ]CC": "tauh11", 
                            "[Z0 -> ^tau-  tau- ]CC": "tauh31"
                        }, 
                        "IgnoreUnmatchedDescriptors": True, 
                        "Type": "RelInfoConeVariables"
                    }
                ], 
                "prescale": 0.01
            }, 
            "HHssnoiso": {
                "RelatedInfoTools": [
                    {
                        "ConeAngle": 0.5, 
                        "DaughterLocations": {
                            " Z0 -> ^e+   X": "taueplus", 
                            " Z0 -> ^e-   X": "taueminus", 
                            " Z0 -> ^mu+  X": "taumuplus", 
                            " Z0 -> ^mu-  X": "taumuminus", 
                            " Z0 -> ^pi+  X": "tauh1plus", 
                            " Z0 -> ^pi-  X": "tauh1minus", 
                            " Z0 -> ^tau+ X": "tauh3plus", 
                            " Z0 -> ^tau- X": "tauh3minus", 
                            "[Z0 ->  e-   ^e-   ]CC": "taue2", 
                            "[Z0 ->  mu-  ^mu-  ]CC": "taumu2", 
                            "[Z0 ->  pi-  ^pi-  ]CC": "tauh12", 
                            "[Z0 ->  tau- ^tau- ]CC": "tauh32", 
                            "[Z0 -> ^e-    e-   ]CC": "taue1", 
                            "[Z0 -> ^mu-   mu-  ]CC": "taumu1", 
                            "[Z0 -> ^pi-   pi-  ]CC": "tauh11", 
                            "[Z0 -> ^tau-  tau- ]CC": "tauh31"
                        }, 
                        "IgnoreUnmatchedDescriptors": True, 
                        "Type": "RelInfoConeVariables"
                    }
                ], 
                "prescale": 0.01,
            }, 
            "MX": {
                "RelatedInfoTools": [
                    {
                        "ConeAngle": 0.5, 
                        "DaughterLocations": {
                            " Z0 -> ^e+   X": "taueplus", 
                            " Z0 -> ^e-   X": "taueminus", 
                            " Z0 -> ^mu+  X": "taumuplus", 
                            " Z0 -> ^mu-  X": "taumuminus", 
                            " Z0 -> ^pi+  X": "tauh1plus", 
                            " Z0 -> ^pi-  X": "tauh1minus", 
                            " Z0 -> ^tau+ X": "tauh3plus", 
                            " Z0 -> ^tau- X": "tauh3minus", 
                            "[Z0 ->  e-   ^e-   ]CC": "taue2", 
                            "[Z0 ->  mu-  ^mu-  ]CC": "taumu2", 
                            "[Z0 ->  pi-  ^pi-  ]CC": "tauh12", 
                            "[Z0 ->  tau- ^tau- ]CC": "tauh32", 
                            "[Z0 -> ^e-    e-   ]CC": "taue1", 
                            "[Z0 -> ^mu-   mu-  ]CC": "taumu1", 
                            "[Z0 -> ^pi-   pi-  ]CC": "tauh11", 
                            "[Z0 -> ^tau-  tau- ]CC": "tauh31"
                        }, 
                        "IgnoreUnmatchedDescriptors": True, 
                        "Type": "RelInfoConeVariables"
                    }
                ], 
                "prescale": 1.0
            }, 
            "MXnoiso": {
                "RelatedInfoTools": [
                    {
                        "ConeAngle": 0.5, 
                        "DaughterLocations": {
                            " Z0 -> ^e+   X": "taueplus", 
                            " Z0 -> ^e-   X": "taueminus", 
                            " Z0 -> ^mu+  X": "taumuplus", 
                            " Z0 -> ^mu-  X": "taumuminus", 
                            " Z0 -> ^pi+  X": "tauh1plus", 
                            " Z0 -> ^pi-  X": "tauh1minus", 
                            " Z0 -> ^tau+ X": "tauh3plus", 
                            " Z0 -> ^tau- X": "tauh3minus", 
                            "[Z0 ->  e-   ^e-   ]CC": "taue2", 
                            "[Z0 ->  mu-  ^mu-  ]CC": "taumu2", 
                            "[Z0 ->  pi-  ^pi-  ]CC": "tauh12", 
                            "[Z0 ->  tau- ^tau- ]CC": "tauh32", 
                            "[Z0 -> ^e-    e-   ]CC": "taue1", 
                            "[Z0 -> ^mu-   mu-  ]CC": "taumu1", 
                            "[Z0 -> ^pi-   pi-  ]CC": "tauh11", 
                            "[Z0 -> ^tau-  tau- ]CC": "tauh31"
                        }, 
                        "IgnoreUnmatchedDescriptors": True, 
                        "Type": "RelInfoConeVariables"
                    }
                ], 
                "prescale": 0.1
            }, 
            "MXssnoiso": {
                "RelatedInfoTools": [
                    {
                        "ConeAngle": 0.5, 
                        "DaughterLocations": {
                            " Z0 -> ^e+   X": "taueplus", 
                            " Z0 -> ^e-   X": "taueminus", 
                            " Z0 -> ^mu+  X": "taumuplus", 
                            " Z0 -> ^mu-  X": "taumuminus", 
                            " Z0 -> ^pi+  X": "tauh1plus", 
                            " Z0 -> ^pi-  X": "tauh1minus", 
                            " Z0 -> ^tau+ X": "tauh3plus", 
                            " Z0 -> ^tau- X": "tauh3minus", 
                            "[Z0 ->  e-   ^e-   ]CC": "taue2", 
                            "[Z0 ->  mu-  ^mu-  ]CC": "taumu2", 
                            "[Z0 ->  pi-  ^pi-  ]CC": "tauh12", 
                            "[Z0 ->  tau- ^tau- ]CC": "tauh32", 
                            "[Z0 -> ^e-    e-   ]CC": "taue1", 
                            "[Z0 -> ^mu-   mu-  ]CC": "taumu1", 
                            "[Z0 -> ^pi-   pi-  ]CC": "tauh11", 
                            "[Z0 -> ^tau-  tau- ]CC": "tauh31"
                        }, 
                        "IgnoreUnmatchedDescriptors": True, 
                        "Type": "RelInfoConeVariables"
                    }
                ], 
                "prescale": 0.1
            }
        }, 
        "DITAU": {
            "ee": {
                "ccuts": {
                    "min_AM": 16000.0, 
                    "min_APTMAX": 12000.0,
                }, 
                "dcuts": {
                    "e": {
                        "extracut": "ALL"
                    }
                }, 
                "mcuts": {
                    "extracut": "ALL"
                }
            }, 
            "eh1": {
                "ccuts": {
                    "extracut": "ATRUE", 
                    "min_AM": 16000.0, 
                    "min_APTMAX": 12000.0,
                }, 
                "dcuts": {
                    "e": {
                        "extracut": "ALL"
                    }, 
                    "pi": {
                        "extracut": "ALL"
                    }
                }, 
                "mcuts": {
                    "extracut": "ALL"
                }
            }, 
            "eh3": {
                "ccuts": {
                    "min_AM": 16000.0, 
                    "min_APTMAX": 12000.0,
                }, 
                "dcuts": {
                    "e": {
                        "extracut": "ALL"
                    }, 
                    "tau": {
                        "extracut": "ALL"
                    }
                }, 
                "mcuts": {
                    "extracut": "ALL"
                }
            }, 
            "emu": {
                "ccuts": {
                    "min_AM": 12000.0, 
                    "min_APTMAX": 9000.0,
                }, 
                "dcuts": {
                    "e": {
                        "extracut": "ALL"
                    }, 
                    "mu": {
                        "extracut": "ALL"
                    }
                }, 
                "mcuts": {
                    "extracut": "ALL"
                }
            }, 
            "h1h1": {
                "ccuts": {
                    "min_AM": 35000.0, 
                    "min_APTMAX": 20000.0,
                    "min_APTMIN": 15000.0,
                }, 
                "dcuts": {
                    "pi": {
                        "extracut": "ALL"
                    }
                }, 
                "mcuts": {
                    "extracut": "ALL"
                }
            }, 
            "h1h3": {
                "ccuts": {
                    "min_AM": 25000.0, 
                    "min_APTMAX": 15000.0,
                    "min_APTMIN": 10000.0,
                }, 
                "dcuts": {
                    "pi": {
                        "extracut": "ALL"
                    }, 
                    "tau": {
                        "extracut": "ALL"
                    }
                }, 
                "mcuts": {
                    "extracut": "ALL"
                }
            }, 
            "h1mu": {
                "ccuts": {
                    "min_AM": 16000.0, 
                    "min_APTMAX": 12000.0
                }, 
                "dcuts": {
                    "mu": {
                        "extracut": "ALL"
                    }, 
                    "pi": {
                        "extracut": "ALL"
                    }
                }, 
                "mcuts": {
                    "extracut": "ALL"
                }
            }, 
            "h3h3": {
                "ccuts": {
                    "min_AM": 25000.0, 
                    "min_APTMAX": 15000.0,
                    "min_APTMIN": 10000.0,
                }, 
                "dcuts": {
                    "tau": {
                        "extracut": "ALL"
                    }
                }, 
                "mcuts": {
                    "extracut": "ALL"
                }
            }, 
            "h3mu": {
                "ccuts": {
                    "min_AM": 12000.0, 
                    "min_APTMAX": 9000.0
                }, 
                "dcuts": {
                    "mu": {
                        "extracut": "ALL"
                    }, 
                    "tau": {
                        "extracut": "ALL"
                    }
                }, 
                "mcuts": {
                    "extracut": "ALL"
                }
            }, 
            "mumu": {
                "ccuts": {
                    "min_AM": 8000.0, 
                    "min_APTMAX": 4000.0
                }, 
                "dcuts": {
                    "mu": {
                        "extracut": "ALL"
                    }
                }, 
                "mcuts": {
                    "extracut": "ALL"
                }
            }
        }, 
        "PVRefitter": None, 
        "TES_e": "Phys/StdAllNoPIDsElectrons/Particles", 
        "TES_mu": "Phys/StdAllLooseMuons/Particles", 
        "TES_pi": "Phys/StdAllNoPIDsPions/Particles", 
        "ditau_pcomb": {
            "": "MomentumCombiner:PUBLIC"
        }, 
        "iso_e": {
            "min_PTFrac05C": 0.8,
        }, 
        "iso_h1": {
            "min_PTFrac05C": 0.8
        }, 
        "iso_h3": {
            "min_PTFrac05C": 0.8
        }, 
        "iso_mu": {
            "min_PTFrac05C": 0.8
        }, 
        "preambulo": "\n", 
        "tau_e": {
            "ABSID": 11, 
            "ISMUONLOOSE": False, 
            "extracut": "ALL", 
            "max_ETA": 4.5, 
            "min_CaloPrsE": 50.0, 
            "min_ECALFrac": 0.1, 
            "min_ETA": 2.0, 
            "min_PT": 5000.0, 
            "min_TRPCHI2": 0.01
        }, 
        "tau_h1": {
            "ISMUONLOOSE": False, 
            "ISPIONORKAON": True, 
            "extracut": "ALL", 
            "max_ETA": 3.75, 
            "min_ETA": 2.25, 
            "min_HCALFrac": 0.05, 
            "min_PT": 5000.0, 
            "min_TRPCHI2": 0.01
        }, 
        "tau_h3": {
            "ccuts": {
                "max_AM": 1500.0, 
                "min_AM": 600.0
            }, 
            "dcuts": {
                "ISMUONLOOSE": False, 
                "ISPIONORKAON": True, 
                "extracut": "ALL", 
                "max_ETA": 4.5, 
                "min_ETA": 2.0, 
                "min_PT": 1000.0, 
                "min_TRPCHI2": 0.01
            }, 
            "mcuts": {
                "max_DRTRIOMAX": 0.4, 
                "max_VCHI2PDOF": 10.0, 
                "min_PT": 5000.0
            }
        }, 
        "tau_mu": {
            "ABSID": 13, 
            "ISMUON": True, 
            "extracut": "ALL", 
            "max_ETA": 4.5, 
            "min_ETA": 2.0, 
            "min_PT": 5000.0, 
            "min_TRPCHI2": 0.01,
        }
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

FullDiJets = {
    "BUILDERTYPE": "FullDiJetsConf", 
    "CONFIG": {
        "FullDiJetsLine_Postscale": 1.0, 
        "FullDiJetsLine_Prescale": 0.05, 
        "min_jet_pT": 20000.0
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

Lb2dp = {
    "BUILDERTYPE": "Lb2dpConf", 
    "CONFIG": {
        'Prescale'             : 1.0 ,
        'Prescale_pipi'        : 1.0 ,
        'TrackChi2Ndof'        : 4.0,
        'TrackGhostProb'       : 0.4,
        'TrackIPChi2'          : 16.,
        'TrackIPChi2_pipi'     : 16.,
        'PionPT'               : 500,
        'PionP'                : 1500,
        'PionPIDKpi'           : 2,
        'ProtonPT'             : 500,
        'ProtonP'              : 15000,
        'ProtonPIDppi'         : 10,
        'ProtonPIDpK'          : 10,
        'KaonPT'               : 500,
        'KaonP'                : 20000,
        'KaonP_pipi'           : 35000,
        'SumPT'                : 1000,
        'LbMassMin'            : 5000. ,
        'LbMassMax'            : 7000. ,
        'LbVtxChi2'            : 20. ,
        'LbVtxChi2_pipi'       : 20. ,
        'LbDIRA'               : 0.9999,
        'LbFDChi2'             : 81,
        'LbFDChi2_pipi'        : 150,
        'LbPT'                 : 1500,
        'LbIPChi2_pipi'        : 25, 
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "QEE" ]
}

MicroDiJets = {
    "BUILDERTYPE": "MicroDiJetsConf", 
    "CONFIG": {
        "MicroDiJetsLine_Postscale": 1.0, 
        "MicroDiJetsLine_Prescale": 0.5, 
        "min_jet_pT": 20000.0
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "QEE" ]
}

TaggedJets = {
    "BUILDERTYPE": "TaggedJetsConf", 
    "CONFIG": {
        "DiTaggedJetsPair_Postscale": 1.0, 
        "DiTaggedJetsPair_Prescale": 1.0, 
        "TaggedJetsEight_Postscale": 1.0, 
        "TaggedJetsEight_Prescale": 1.0, 
        "TaggedJetsFour_Postscale": 1.0, 
        "TaggedJetsFour_Prescale": 1.0, 
        "TaggedJetsPairExclusiveDiJet_Postscale": 1.0, 
        "TaggedJetsPairExclusiveDiJet_Prescale": 1.0, 
        "TaggedJetsPair_Postscale": 1.0, 
        "TaggedJetsPair_Prescale": 1.0, 
        "TaggedJetsSix_Postscale": 1.0, 
        "TaggedJetsSix_Prescale": 1.0, 
        "min_jet_pT": 25000.0, 
        "min_jet_pT_ExclusiveDiJet": 20000.0
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "QEE" ]
}

A2MuMu = {
    'BUILDERTYPE' : 'A2MuMuConf',
    'WGs'         : [ 'QEE'],
    'STREAMS'     : [ 'EW' ],
    'CONFIG'      : { 
        'A2MuMu_Prescale'  : 1.0,
        'A2MuMu_Postscale' : 1.0,
        'pT'               : 2.5 * GeV,
        'MMmin'            : 12. * GeV,
        'MMmax'            : 60. * GeV,
    },
}

A2MuMuSameSign = {
    'BUILDERTYPE' : 'A2MuMuSameSignConf',
    'WGs'         : [ 'QEE'],
    'STREAMS'     : [ 'EW' ],
    'CONFIG'      : { 
        'A2MuMuSameSign_Prescale'  : 1.0,
        'A2MuMuSameSign_Postscale' : 1.0,
        'pT'               : 2.5 * GeV,
        'MMmin'            : 12. * GeV,
        'MMmax'            : 60. * GeV,
    },
}

