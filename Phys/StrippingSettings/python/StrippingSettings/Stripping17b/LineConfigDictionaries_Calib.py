###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
MuIDCalib = {
    'BUILDERTYPE' : 'MuIDCalibConf',
    'WGs'         : [ 'ALL' ],
    'STREAMS'     : [ 'PID' ],
    'CONFIG'      : {
    'PromptPrescale'           : 0.0,
    'DetachedPrescale'         : 0.0,
    'DetachedNoMIPPrescale'    : 1.0,
    'DetachedNoMIPHiPPrescale' : 1.0,
    'DetachedNoMIPKPrescale'   : 1.0,
    'FromLambdacPrescale'      : 0.0,
    'KFromLambdacPrescale'     : 0.0,
    'PiFromLambdacPrescale'    : 0.0,
    'PFromLambdacPrescale'     : 0.0,
    'KISMUONFromLambdacPrescale' : 0.0,
    'PiISMUONFromLambdacPrescale': 0.0,
    'PISMUONFromLambdacPrescale' : 0.0 
    }
    }
