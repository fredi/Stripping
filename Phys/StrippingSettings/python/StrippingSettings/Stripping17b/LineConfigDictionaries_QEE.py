###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
A1MuMu = {
    'BUILDERTYPE'  : 'A1MuMuConf',
    'CONFIG'       : {
                'A1MuMu_LinePrescale'  : 1.0,
                'A1MuMu_LinePostscale'              : 1.0,
                'A1MuMu_checkPV'                    : False,
               ###
               'DIMUON_LOW_MASS'                   : '5000.0',    # MeV/c2
               #'DIMUON_HIGH_MASS'                 : not set,     # MeV/c2
               ###
               'PT_MUON_MIN'                       : '2500.0',    # MeV/c
               'P_MUON_MIN'                        : '2500.0',    # MeV/c (de facto no cut)
               'TRACKCHI2_MUON_MAX'                : '10',        # dl
               ###
               'PT_DIMUON_MIN'                     : '7500.0',    # MeV/c
               'VCHI2_DIMUON_MAX'                  : '12'         # dl
                 },
    'WGs' : [ 'QEE' ],
    'STREAMS' : [ 'Dimuon' ]
    }


