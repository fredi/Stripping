###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
##################################################################################
##                        S T R I P P I N G  2 8 r 1 p 1                        ##
##                                                                              ##
##  Configuration for QEE WG                                                    ##
##  Contact person: Matthieu Marinangeli      (matthieu.marinangeli@cern.ch)    ##  
##################################################################################


WMu = {
  "BUILDERTYPE": "WMuConf", 
  "CONFIG": {
    "HLT1_SingleTrackNoBias": "HLT_PASS( 'Hlt1MBNoBiasDecision' )", 
    "HLT2_Control10": "HLT_PASS_RE('Hlt2(EW)?SingleMuon(V)?High.*')", 
    "HLT2_Control4800": "HLT_PASS_RE('Hlt2(EW)?SingleMuonLow.*')", 
    "IsoMax": 4000.0, 
    "MinIP": 0.105, 
    "RawEvents": [
      "Muon", 
      "Calo", 
      "Rich", 
      "Velo", 
      "Tracker", 
      "HC"
    ], 
    "STNB_Prescale": 0.0, 
    "SingMuon10_Prescale": 0.0, 
    "SingMuon10_pT": 10000.0, 
    "SingMuon48_Prescale": 0.0, 
    "SingMuon48_pT": 4800.0, 
    "WMuHighIP_Postscale": 1.0, 
    "WMuHighIP_Prescale": 1.0, 
    "WMuIso_Postscale": 1.0, 
    "WMuIso_Prescale": 1.0, 
    "WMuLow_Prescale": 0.0, 
    "WMu_Postscale": 0.0, 
    "WMu_Prescale": 0.0, 
    "pT": 20000.0, 
    'MaxpT_HighIP': 20000,
    'MaxpTiso': 20000,
    'MinpT_HighIP': 15000,
    'MinpTiso': 15000,  
    "pTlow": 15000.0, 
    "pTvlow": 5000.0
  }, 
  "STREAMS": [ "EW" ], 
  "WGs": [ "QEE" ]
}

Exotica = {
  "BUILDERTYPE": "ExoticaConf", 
  "CONFIG": {
    "Common": {
      "GhostProb": 0.3
    }, 
    "DiRHNu": {
      "PT": 0, 
      "VChi2": 10
    }, 
    "DisplDiE": {
      "EIPChi2": 4, 
      "EProbNNe": 0.1, 
      "FDChi2": 0, 
      "IPChi2": 32, 
      "MaxMM": 500.0, 
      "PT": 500.0, 
      "TisTosSpec": "Hlt1TrackMVA.*Decision"
    }, 
    "DisplDiEHighMass": {
      "EIPChi2": 4, 
      "EProbNNe": 0.1, 
      "FDChi2": 0, 
      "IPChi2": 32, 
      "MinMM": 500.0, 
      "PT": 500.0, 
      "TAU": 0.001, 
      "TisTosSpec": "Hlt1TrackMVA.*Decision"
    }, 
    "DisplDiMuon": {
      "FDChi2": 4, 
      "IPChi2": 16, 
      "MaxMM": 500.0, 
      "MuIPChi2": 4, 
      "MuProbNNmu": 0.5, 
      "PT": 1000.0
    }, 
    "DisplDiMuonHighMass": {
      "FDChi2": 4, 
      "IPChi2": 16, 
      "MinMM": 500.0, 
      "MuIPChi2": 9, 
      "MuProbNNmu": 0.8, 
      "PT": 1000.0, 
      "TAU": 0.001
    }, 
    "DisplDiMuonNoPoint": {
      "FDChi2": 16, 
      "MSwitch": 500.0, 
      "MuIPChi2_highmass": 25, 
      "MuIPChi2_lowmass": 4, 
      "MuProbNNmu_highmass": 0.8, 
      "MuProbNNmu_lowmass": 0.5, 
      "PT": 1000.0, 
      "R": 0.0
    }, 
    "DisplDiMuonNoPointR": {
      "FDChi2": 16, 
      "MSwitch": 500.0, 
      "MuIPChi2_highmass": 25, 
      "MuIPChi2_lowmass": 4, 
      "MuProbNNmu_highmass": 0.8, 
      "MuProbNNmu_lowmass": 0.5, 
      "PT": 1000.0, 
      "R": 2.75
    }, 
    "DisplPhiPhi": {
      "FDChi2": 45, 
      "KIPChi2": 16, 
      "KPT": 500.0, 
      "KProbNNk": 0.1, 
      "PhiMassWindow": 20.0, 
      "PhiPT": 1000.0, 
      "TisTosSpec": "Hlt1IncPhi.*Decision", 
      "VChi2": 10, 
      "input": "Phys/StdLoosePhi2KK/Particles"
    }, 
    "Prescales": {
      "DiRHNu": 1.0, 
      "DisplDiE": 1.0, 
      "DisplDiEHighMass": 1.0, 
      "DisplDiMuon": 0.0, 
      "DisplDiMuonHighMass": 1.0, 
      "DisplDiMuonNoPoint": 0.0, 
      "DisplDiMuonNoPointR": 1.0, 
      "DisplPhiPhi": 0.0, 
      "PrmptDiMuonHighMass": 0.0, 
      "QuadMuonNoIP": 0.0, 
      "RHNu": 0.0, 
      "RHNuHighMass": 1.0
    }, 
    "PrmptDiMuonHighMass": {
      "FDChi2": 45, 
      "M": 3200.0, 
      "M_switch_ab": 740.0, 
      "M_switch_bc": 1100.0, 
      "M_switch_cd": 3000.0, 
      "M_switch_de": 3200.0, 
      "M_switch_ef": 9000.0, 
      "MuIPChi2": 6, 
      "MuP": 10000.0, 
      "MuPT": 500.0, 
      "MuPTPROD": 1000000.0, 
      "MuProbNNmu_a": 0.8, 
      "MuProbNNmu_b": 0.8, 
      "MuProbNNmu_c": 0.95, 
      "MuProbNNmu_d": 2.0, 
      "MuProbNNmu_e": 0.95, 
      "MuProbNNmu_f": 0.9, 
      "PT": 1000.0
    }, 
    "QuadMuonNoIP": {
      "PT": 0, 
      "VChi2": 10
    }, 
    "RHNu": {
      "M": 0, 
      "TAU": 0.001
    }, 
    "RHNuHighMass": {
      "M": 5000.0, 
      "ProbNNmu": 0.5, 
      "TAU": 0.001
    }, 
    "SharedDiENoIP": {
      "DOCA": 0.5, 
      "EP": 5000.0, 
      "EPT": 500.0, 
      "EProbNNe": 0.1, 
      "MM": 0.0, 
      "VChi2": 25, 
      "input": "Phys/StdAllLooseElectrons/Particles"
    }, 
    "SharedDiMuonNoIP": {
      "DOCA": 0.5, 
      "MuP": 10000.0, 
      "MuPT": 500.0, 
      "MuProbNNmu": 0.5, 
      "VChi2": 10, 
      "input": "Phys/StdAllLooseMuons/Particles"
    }, 
    "SharedRHNu": {
      "FDChi2": 45, 
      "IPChi2": 16, 
      "M": 0, 
      "P": 10000.0, 
      "PT": 500.0, 
      "ProbNNmu": 0.5, 
      "TAU": 0.001, 
      "VChi2": 10, 
      "XIPChi2": 16, 
      "input": [
        "Phys/StdAllLooseMuons/Particles", 
        "Phys/StdNoPIDsPions/Particles"
      ]
    }
  }, 
  "STREAMS": {
    "Dimuon": [
      "StrippingExoticaDisplDiMuonLine", 
      "StrippingExoticaDisplDiMuonHighMassLine", 
      "StrippingExoticaDisplDiMuonNoPointLine", 
      "StrippingExoticaDisplDiMuonNoPointRLine"
    ], 
    "EW": [
      "StrippingExoticaDisplPhiPhiLine", 
      "StrippingExoticaRHNuLine", 
      "StrippingExoticaRHNuHighMassLine", 
      "StrippingExoticaDiRHNuLine", 
      "StrippingExoticaPrmptDiMuonHighMassLine", 
      "StrippingExoticaQuadMuonNoIPLine", 
      "StrippingExoticaDisplDiELine", 
      "StrippingExoticaDisplDiEHighMassLine"
    ]
  }, 
  "WGs": [ "QEE" ]
}





