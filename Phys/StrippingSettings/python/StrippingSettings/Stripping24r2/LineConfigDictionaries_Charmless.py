###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

B24p = {
    "BUILDERTYPE": "B24pLinesConf",
    "CONFIG": {
        "B24pPrescale":
        1,
        "B2JpsiKpiPrescale":
        1,
        "B2PhiKhPrescale":
        1,
        "CommonRelInfoTools": [{
            "Location": "VtxIsoInfo",
            "Type": "RelInfoVertexIsolation"
        }, {
            "Location": "VtxIsoInfoBDT",
            "Type": "RelInfoVertexIsolationBDT"
        },
                               {
                                   "Location": "BsMuMuBIsolation",
                                   "Type": "RelInfoBs2MuMuBIsolations",
                                   "Variables": [],
                                   "makeTrackCuts": False,
                                   "tracktype": 3
                               }],
        "MDSTflag":
        False,
        "MaxDoca":
        0.35,
        "MaxIPChi2":
        25,
        "MaxTrChi2Dof":
        4.0,
        "MaxTrGhp":
        0.4,
        "MaxVtxChi2Dof":
        9,
        "MinDira":
        0.0,
        "MinTau":
        1.0,
        "MinTrIPChi2":
        9.0,
        "MinVDChi2":
        100,
        "Postscale":
        1,
        "mDiffb":
        400,
        "mJpsiMax":
        3200,
        "mJpsiMin":
        2990,
        "mKstMax":
        1200
    },
    "STREAMS": ["Bhadron"],
    "WGs": ["BnoC"]
}

B2CharmlessInclusive = {
    "BUILDERTYPE": "B2CharmlessInclusive",
    "CONFIG": {
        "KS0_Child_Trk_Chi2": 6.0,
        "KS0_DD_FDChi2": 90.0,
        "KS0_DD_MassWindow": 50.0,
        "KS0_LL_FDChi2": 10.0,
        "KS0_LL_MassWindow": 40.0,
        "KS0_MinPT": 1500,
        "Photon_CL_Min": 0.2,
        "Photon_Res_PT_Min": 400.0,
        "Pi0_Res_PT_Min": 600.0,
        "Q2BBDIRA": 0.995,
        "Q2BBMaxM3pi": 6700.0,
        "Q2BBMaxM4pi": 5700.0,
        "Q2BBMinM3pi": 4500.0,
        "Q2BBMinM4pi": 4000.0,
        "Q2BBMinPT": 2500.0,
        "Q2BBVtxChi2DOF": 6.0,
        "Q2BPrescale": 1.0,
        "Q2BResMaxMass": 1100.0,
        "Q2BResMinHiPT": 1500.0,
        "Q2BResMinPT": 600,
        "Q2BResVtxChi2DOF": 6.0,
        "Q2BTrkGhostProb": 0.5,
        "Q2BTrkMinHiPT": 1500.0,
        "Q2BTrkMinIPChi2": 16.0,
        "Q2BTrkMinPT": 500
    },
    "STREAMS": ["Bhadron"],
    "WGs": ["BnoC"]
}

B2HHBDT = {
    "BUILDERTYPE": "B2HHBDTLines",
    "CONFIG": {
        "BDIRA": 0.99,
        "BDTCut": -1,
        "BDTWeightsFile": "$TMVAWEIGHTSROOT/data/B2HH_BDT_v1r5.xml",
        "BFDCHI2": 100,
        "BIPCHI2": 9,
        "BMassHigh": 6200,
        "BMassLow": 4800,
        "BMassWinHigh": 6200,
        "BMassWinLow": 4700,
        "BPT": 0,
        "DOCACHI2": 9,
        "PionIPCHI2": 16,
        "PionPT": 1000,
        "PrescaleB2HHBDT": 1.0,
        "SumPT": 4500,
        "TrChi2": 4,
        "TrGhostProb": 3
    },
    "STREAMS": ["BhadronCompleteEvent"],
    "WGs": ["BnoC"]
}

B2HHPi0 = {
    "BUILDERTYPE": "StrippingB2HHPi0Conf",
    "CONFIG": {
        "BMaxIPChi2": 9,
        "BMaxM": 6400,
        "BMinDIRA": 0.99995,
        "BMinM": 4200,
        "BMinPT_M": 3000,
        "BMinPT_R": 2500,
        "BMinVVDChi2": 64,
        "BMinVtxProb": 0.001,
        "MergedLinePostscale": 1.0,
        "MergedLinePrescale": 1.0,
        "Pi0MinPT_M": 2500,
        "Pi0MinPT_R": 1500,
        "PiMaxGhostProb": 0.5,
        "PiMinIPChi2": 25,
        "PiMinP": 5000,
        "PiMinPT": 500,
        "PiMinTrackProb": 1e-06,
        "ResPi0MinGamCL": 0.2,
        "ResolvedLinePostscale": 1.0,
        "ResolvedLinePrescale": 1.0
    },
    "STREAMS": ["Bhadron"],
    "WGs": ["BnoC"]
}

B2KShh = {
    "BUILDERTYPE": "B2KShhConf",
    "CONFIG": {
        "BDaug_DD_IPChi2sum": 50.0,
        "BDaug_DD_PTsum": 4200.0,
        "BDaug_LD_IPChi2sum": 50.0,
        "BDaug_LD_PTsum": 4200.0,
        "BDaug_LL_IPChi2sum": 50.0,
        "BDaug_LL_PTsum": 3000.0,
        "BDaug_MaxDOCAChi2": 25.0,
        "BDaug_MedPT_PT": 800.0,
        "B_APTmin": 1000.0,
        "B_DD_Dira": 0.999,
        "B_DD_FDChi2": 50.0,
        "B_DD_IPChi2": 6.0,
        "B_LD_Dira": 0.999,
        "B_LD_FDChi2": 50.0,
        "B_LD_IPChi2": 7.0,
        "B_LL_Dira": 0.999,
        "B_LL_FDChi2": 50.0,
        "B_LL_IPChi2": 8.0,
        "B_Mhigh": 921.0,
        "B_Mlow": 1279.0,
        "B_PTmin": 1500.0,
        "B_VtxChi2": 12.0,
        "ConeAngles": [1.0, 1.5, 1.7, 2.0],
        "FlavourTagging": True,
        "GEC_MaxTracks": 250,
        "HLT1Dec": "Hlt1(Two)?TrackMVADecision|Hlt1(Phi)?IncPhiDecision",
        "HLT2Dec": "Hlt2Topo[234]BodyDecision|Hlt2(Phi)?IncPhiDecision",
        "KS_DD_FDChi2": 50.0,
        "KS_DD_MassWindow": 30.0,
        "KS_DD_Pmin": 6000.0,
        "KS_DD_VtxChi2": 12.0,
        "KS_FD_Z": 15.0,
        "KS_LD_FDChi2": 50.0,
        "KS_LD_MassWindow": 25.0,
        "KS_LD_Pmin": 6000.0,
        "KS_LD_VtxChi2": 12.0,
        "KS_LL_FDChi2": 80.0,
        "KS_LL_MassWindow": 20.0,
        "KS_LL_Pmin": 0.0,
        "KS_LL_VtxChi2": 12.0,
        "MDST": False,
        "Postscale": 1.0,
        "Prescale": 1.0,
        "Prescale_SameSign": 1.0,
        "Trk_Chi2": 4.0,
        "Trk_GhostProb": 0.5
    },
    "STREAMS": {
        "Bhadron": [
            "StrippingB2KShh_DD_Run2_SS_Line",
            "StrippingB2KShh_LL_Run2_SS_Line",
            "StrippingB2KShh_LD_Run2_SS_Line"
        ],
        "BhadronCompleteEvent": [
            "StrippingB2KShh_DD_Run2_OS_Line",
            "StrippingB2KShh_LL_Run2_OS_Line",
            "StrippingB2KShh_LD_Run2_OS_Line"
        ]
    },
    "WGs": ["BnoC"]
}

B2KShhh = {
    "BUILDERTYPE": "B2KSHHHLines",
    "CONFIG": {
        "CombMassHigh": 7200.0,
        "CombMassLow": 4500.0,
        "HighPIDK": 0,
        "Hlt1Filter": "Hlt1(Two)?TrackMVADecision",
        "Hlt2Filter": "Hlt2Topo[234]BodyDecision",
        "KSCutDIRA_DD": 0.999,
        "KSCutDIRA_LL": 0.999,
        "KSCutFDChi2_DD": 5,
        "KSCutFDChi2_LL": 5,
        "KSCutMass_DD": 50.0,
        "KSCutMass_LL": 35.0,
        "LowPIDK": 0,
        "MassHigh": 7000.0,
        "MassLow": 4700.0,
        "MaxADOCACHI2": 10.0,
        "MaxDz_DD": 9999.0,
        "MaxDz_LL": 9999.0,
        "MaxVCHI2NDOF": 12.0,
        "MinBPVDIRA": 0.99995,
        "MinBPVTAU": 0.0001,
        "MinCombPT": 1500.0,
        "MinDz_DD": 0.0,
        "MinDz_LL": 0.0,
        "MinTrkIPChi2": 4,
        "MinTrkP": 1500.0,
        "MinTrkPT": 500.0,
        "MinVPT": 1500.0,
        "PrescaleB2KKKKSDD": 1,
        "PrescaleB2KKKKSLL": 1,
        "PrescaleB2KKPiKSDD": 1,
        "PrescaleB2KKPiKSLL": 1,
        "PrescaleB2KPiPiKSDD": 1,
        "PrescaleB2KPiPiKSLL": 1,
        "PrescaleB2PiPiPiKSDD": 1,
        "PrescaleB2PiPiPiKSLL": 1,
        "TrChi2": 4,
        "TrGhostProb": 0.4
    },
    "STREAMS": ["Bhadron"],
    "WGs": ["BnoC"]
}

B2Kpi0Lines = {
    "BUILDERTYPE": "B2Kpi0Lines",
    "CONFIG": {
        "B2K0pi0Config": {
            "ASumPT_min": 5000.0,
            "BMass_max": 6200.0,
            "BMass_min": 4000.0,
            "BPT_min": 4000.0,
            "Hlt1Filter": "HLT_PASS_RE('Hlt1(Two)?TrackMVADecision')",
            "Hlt2Filter": None,
            "MTDOCAChi2_max": 10.0,
            "prescale": 1.0
        },
        "B2Kpi0Config": {
            "ASumPT_min": 6500.0,
            "BMass_max": 6200.0,
            "BMass_min": 4000.0,
            "BPT_min": 5000.0,
            "Hlt1Filter": "HLT_PASS('Hlt1TrackMVADecision')",
            "Hlt2Filter": None,
            "MTDOCAChi2_max": 8.0,
            "prescale": 1.0
        },
        "K+Config": {
            "K+IPChi2_min": 50,
            "K+PID_min": -0.5,
            "K+PT_min": 1200.0,
            "K+P_min": 12000.0
        },
        "KS0Config": {
            "KSIPChi2_min": 10,
            "KSMass_delta": 15.0,
            "KSPT_min": 500.0,
            "KSP_min": 8000.0,
            "KSVChi2DOF_max": 15
        },
        "L0Filter": "L0_CHANNEL('Photon')|L0_CHANNEL('Electron')",
        "checkPV": True,
        "pi0Config": {
            "pi0PT_min": 3500.0,
            "pi0P_min": 5000.0
        },
        "pi0VoidFilter": "(CONTAINS('Phys/StdLooseMergedPi0/Particles')>0)"
    },
    "STREAMS": ["BhadronCompleteEvent"],
    "WGs": ["BnoC"]
}

B2Ksthh = {
    "BUILDERTYPE": "B2KsthhConf",
    "CONFIG": {
        "BDaug_MaxPT_IP": 0.05,
        "BDaug_MedPT_PT": 800.0,
        "BDaug_PTsum": 3000.0,
        "B_APTmin": 1000.0,
        "B_Dira": 0.999,
        "B_FDChi2": 50.0,
        "B_FDwrtPV": 1.0,
        "B_IPCHI2sum": 50.0,
        "B_IPCHI2wrtPV": 8.0,
        "B_Mhigh": 921.0,
        "B_Mlow": 1279.0,
        "B_PTmin": 1500.0,
        "B_VtxChi2": 12.0,
        "ConeAngle10": 1.0,
        "ConeAngle15": 1.5,
        "ConeAngle17": 1.7,
        "GEC_MaxTracks": 250,
        "HLT1Dec": "Hlt1(Two)?TrackMVADecision",
        "HLT2Dec": "Hlt2Topo[234]BodyDecision",
        "Kstar_MassHi": 5000.0,
        "Kstar_MassLo": 0.0,
        "Postscale": 1.0,
        "Prescale": 1.0,
        "Trk_Chi2": 4.0,
        "Trk_GhostProb": 0.4
    },
    "STREAMS": ["Bhadron"],
    "WGs": ["BnoC"]
}

B2TwoBaryons = {
    "BUILDERTYPE": "B2TwoBaryonLines",
    "CONFIG": {
        "BDIRA": 0.9997,
        "BDaug_DD_PTsum": 4200.0,
        "BDaug_DD_maxDocaChi2": 5.0,
        "BDaug_LL_PTsum": 3000.0,
        "BDaug_LL_maxDocaChi2": 5.0,
        "BDaug_MaxPT_IP": 0.05,
        "BDaug_MedPT_PT": 800.0,
        "BIPChi2B2PPbar": 16,
        "BPTB2PPbar": 1100,
        "B_2bodyMhigh": 800.0,
        "B_2bodyMlow": 800.0,
        "B_APTmin": 1000.0,
        "B_DD_Dira": 0.995,
        "B_DD_FDChi2": 50.0,
        "B_DD_IPCHI2wrtPV": 8.0,
        "B_DD_PTMin": 500.0,
        "B_FDwrtPV": 1.0,
        "B_LL_Dira": 0.995,
        "B_LL_FDChi2": 50.0,
        "B_LL_IPCHI2wrtPV": 8.0,
        "B_LL_PTMin": 500.0,
        "B_Mhigh": 500.0,
        "B_Mlow": 500.0,
        "B_PTmin": 1500.0,
        "B_VtxChi2": 12.0,
        "Bs0_ADOCAMAX_Long_Max": 5.0,
        "Bs0_AM_Max": 700.0,
        "Bs0_APT_Min": 2000.0,
        "Bs0_BPVDIRA_Long_Min": 0.9,
        "Bs0_BPVIPCHI2_Long_Max": 25,
        "Bs0_BPVVDCHI2_Long_Min": 4,
        "Bs0_VtxChi2_NDF_Long_Max": 16,
        "CombMassWindow": 200,
        "GEC_MaxTracks": 250,
        "Lambda_DD_FD": 300.0,
        "Lambda_DD_FDChi2": 50.0,
        "Lambda_DD_MassWindow": 20.0,
        "Lambda_DD_Pmin": 8000.0,
        "Lambda_DD_VtxChi2": 12.0,
        "Lambda_LL_FDChi2": 50.0,
        "Lambda_LL_MassWindow": 15.0,
        "Lambda_LL_VtxChi2": 12.0,
        "MVAResponseDD": 0.97,
        "MVAResponseLL": 0.95,
        "MaxDaughtPB2PPbar": 300000,
        "MaxGhostProb": 0.4,
        "MaxIPChi2B2PPbar": 25,
        "MaxPTB2PPbar": 2100,
        "MinIPChi2B2PPbar": 10,
        "MinPTB2PPbar": 900,
        "PIDpk": -2,
        "PIDppi": -1,
        "Postscale": 1.0,
        "Prescale": 1.0,
        "PrescaleB2PPbar": 1,
        "Trk_Chi2": 3.0,
        "VertexChi2B2PPbar": 9
    },
    "STREAMS": ["BhadronCompleteEvent"],
    "WGs": ["BnoC"]
}

B2XEta = {
    "BUILDERTYPE": "B2XEtaConf",
    "CONFIG": {
        "BDaug_DD_maxDocaChi2": 15.0,
        "BDaug_LL_maxDocaChi2": 15.0,
        "B_Dira": 0.9995,
        "B_IPCHI2": 20.0,
        "B_MassWindow": 750.0,
        "B_PTmin": 1500.0,
        "B_VtxChi2": 15.0,
        "B_eta_IPCHI2": 6.0,
        "GEC_MaxTracks": 250,
        "KS_DD_FDChi2": 20.0,
        "KS_DD_MassWindow": 23.0,
        "KS_DD_PTmin": 1200.0,
        "KS_DD_VtxChi2": 15.0,
        "KS_LL_FDChi2": 50.0,
        "KS_LL_MassWindow": 14.0,
        "KS_LL_PTmin": 1200.0,
        "KS_LL_VtxChi2": 15.0,
        "Kstar_PTmin": 1200.0,
        "Kstar_ipChi2": 5.0,
        "Kstar_massWdw": 100.0,
        "Kstar_vtxChi2": 9.0,
        "L_DD_MassWindow": 20.0,
        "L_DD_PTmin": 1000.0,
        "L_DD_VtxChi2": 15.0,
        "L_LL_MassWindow": 15.0,
        "L_LL_PTmin": 1000.0,
        "L_LL_VtxChi2": 15.0,
        "LbDaug_DD_maxDocaChi2": 15.0,
        "LbDaug_LL_maxDocaChi2": 15.0,
        "Lb_Dira": 0.9995,
        "Lb_IPCHI2": 20.0,
        "Lb_MassWindow": 750.0,
        "Lb_PTmin": 1000.0,
        "Lb_VtxChi2": 15.0,
        "Lb_eta_IPCHI2": 6.0,
        "Postscale": 1.0,
        "Prescale": 1.0,
        "ProbNNCut": 0.1,
        "Trk_Chi2": 4.0,
        "Trk_GP": 0.5,
        "Trk_PT": 300.0,
        "etaGG_Prescale": 0.0,
        "eta_DOCA": 10.0,
        "eta_MassWindow": 200.0,
        "eta_PT": 2000,
        "eta_prime_DOCA": 10.0,
        "eta_prime_MassWindow": 150.0,
        "eta_prime_PT": 2000.0,
        "eta_prime_vtxChi2": 10.0,
        "eta_vtxChi2": 10.0,
        "etaforetap_MassWindow": 75.0,
        "gamma_PT": 500,
        "kstar_daug_PT": 500.0,
        "pK_IPCHI2": 20.0,
        "pK_PT": 500.0
    },
    "STREAMS": ["Bhadron"],
    "WGs": ["BnoC"]
}

B2hhpipiPhsSpcCut = {
    "BUILDERTYPE": "B2hhpipiPhsSpcCutConf",
    "CONFIG": {
        "2PionCuts":
        "(PT > 200*MeV) & (PROBNNpi > 0.1) & (MIPCHI2DV(PRIMARY)> 2) & (TRGHOSTPROB < 0.4) & (TRCHI2DOF<3)",
        "4PionCuts":
        "(PT > 200*MeV) & (PROBNNpi > 0.03) & (MIPCHI2DV(PRIMARY)> 1.5) & (TRGHOSTPROB < 0.4) & (TRCHI2DOF<3)",
        "B0ComCuts":
        "\n\t\t\t     (ADAMASS('B0') < 300 *MeV)\n\t\t\t     & (AMAXDOCA('') < 0.2 *mm)\n\t\t\t     & (((AMASS(1,2) < 1864.83 *MeV)\n\t\t\t     & (AMASS(3,4) < 1864.83 *MeV))\n\t\t\t     | ((AMASS(1,4) < 1864.83 *MeV)\n\t\t             & (AMASS(2,3) < 1864.83 *MeV))\n\t\t\t     | (AMASS(1,2,3) < 1869.65 *MeV)\n\t\t\t     | (AMASS(1,2,4) < 1869.65 *MeV)\n\t\t\t     | (AMASS(1,3,4) < 1869.65 *MeV)\n\t\t\t     | (AMASS(2,3,4) < 1869.65 *MeV))\n                             ",
        "B0KKpipiComCuts":
        "\n\t\t\t     ((ADAMASS('B0') < 300 *MeV)\n\t\t\t     | (ADAMASS('B_s0') < 300 *MeV))\n\t\t\t     & (AMAXDOCA('') < 0.2 *mm)\n\t\t\t     & (((AMASS(1,2) < 1864.83 *MeV)\n\t\t\t     & (AMASS(3,4) < 1864.83 *MeV))\n\t\t\t     | ((AMASS(1,4) < 1864.83 *MeV)\n\t\t\t     & (AMASS(2,3) < 1864.83 *MeV))\n\t\t\t     | (AMASS(1,2,3) < 1869.65 *MeV)\n\t\t\t     | (AMASS(1,2,4) < 1869.65 *MeV)\n\t\t\t     | (AMASS(1,3,4) < 1869.65 *MeV)\n\t\t\t     | (AMASS(2,3,4) < 1869.65 *MeV))\n                             ",
        "B0MomCuts":
        "\n\t\t             (VFASPF(VCHI2) < 30.)\n\t\t\t     & (BPVDIRA> 0.999)\n\t\t\t     & (BPVVDCHI2 > 10.)\n\t\t\t     & (BPVIPCHI2()<30)\n\t\t\t     & (PT > 2.*GeV )\n\t                     ",
        "B24piMVACut":
        "-.03",
        "B2KKpipiMVACut":
        ".05",
        "B2hhpipiXmlFile":
        "$TMVAWEIGHTSROOT/data/B2hhpipi_v1r0.xml",
        "KaonCuts":
        "(PT > 200*MeV) & (PROBNNk > 0.1) & (MIPCHI2DV(PRIMARY)> 2) & (TRGHOSTPROB < 0.4) & (TRCHI2DOF<3)",
        "Prescale":
        1.0
    },
    "STREAMS": {
        "Bhadron": ["StrippingB2hhpipiPhsSpcCut4piLine"],
        "BhadronCompleteEvent": ["StrippingB2hhpipiPhsSpcCutKKpipiLine"]
    },
    "WGs": ["BnoC"]
}

B2pphh = {
    "BUILDERTYPE": "B2pphhConf",
    "CONFIG": {
        "4h_AMAXDOCA":
        0.25,
        "4h_PTSUM":
        3000.0,
        "B_CHI2":
        25.0,
        "B_DIRA":
        0.9999,
        "B_MIPDV":
        0.2,
        "B_PT":
        1000.0,
        "CombMass123Max":
        5600.0,
        "CombMass12Max_kk":
        4700.0,
        "CombMass12Max_kpi":
        5000.0,
        "CombMass12Max_pipi":
        5350.0,
        "CombMassMax":
        5.6,
        "CombMassMin":
        5.0,
        "KaonCuts":
        "HASRICH & (P > 1500*MeV) & (PT > 300*MeV) & (MIPCHI2DV(PRIMARY) > 5.0) & (TRGHOSTPROB < 0.5) & (PROBNNk > 0.05)",
        "MassMax":
        5.55,
        "MassMin":
        5.05,
        "MaxTrSIZE":
        10000,
        "PionCuts":
        "HASRICH & (P > 1500*MeV) & (PT > 300*MeV) & (MIPCHI2DV(PRIMARY) > 8.0) & (TRGHOSTPROB < 0.5) & (PROBNNpi > 0.10)",
        "Prescaleppkk":
        1.0,
        "Prescaleppkpi":
        1.0,
        "Prescalepppipi":
        1.0,
        "ProtonCuts":
        "HASRICH & (P > 1500*MeV) & (PT > 300*MeV) & (MIPCHI2DV(PRIMARY) > 3.0) & (TRGHOSTPROB < 0.5) & (PROBNNp > 0.05)",
        "d_achi2doca12":
        20.0,
        "d_achi2doca13":
        20.0,
        "d_achi2doca14":
        20.0,
        "d_achi2doca23":
        20.0,
        "d_achi2doca24":
        20.0,
        "d_achi2doca34":
        20.0,
        "p_PMIN":
        4.0,
        "p_PROBNNpPROD":
        0.01,
        "p_PSUM":
        7000.0,
        "p_PTMIN":
        400.0,
        "p_PTSUM":
        750.0
    },
    "STREAMS": {
        "Bhadron": [
            "StrippingB2pphh_kkLine", "StrippingB2pphh_kpiLine",
            "StrippingB2pphh_pipiLine"
        ]
    },
    "WGs": ["BnoC"]
}

Bc2hhh = {
    "BUILDERTYPE": "Bc2hhhBuilder",
    "CONFIG": {
        "KKK_exclLinePostscale": 1.0,
        "KKK_exclLinePrescale": 1.0,
        "KKpi_exclLinePostscale": 1.0,
        "KKpi_exclLinePrescale": 1.0,
        "Kpipi_exclLinePostscale": 1.0,
        "Kpipi_exclLinePrescale": 1.0,
        "MaxTrSIZE": 200,
        "_3h_CHI2": 40.0,
        "_3h_DIRA": 0.9999,
        "_3h_DOCA": 0.2,
        "_3h_FDCHI2": 150.0,
        "_3h_IPCHI2": 10.0,
        "_3h_Mmax": 6502.0,
        "_3h_Mmin": 5998.0,
        "_3h_PT": 1000.0,
        "_3h_PTmax": 1500,
        "_3h_PTsum": 4500.0,
        "_3h_PVIPCHI2sum": 200.0,
        "_3h_Psum": 22000.0,
        "_3h_SVPV": 1.5,
        "_3h_TRKCHIDOFmin": 3.0,
        "_bu3h_Mmax": 5502.0,
        "_bu3h_Mmin": 5098.0,
        "_h_IPCHI2": 1.0,
        "_h_P": 2500.0,
        "_h_PT": 300.0,
        "_h_TRCHI2DOF": 4.0,
        "_h_TRGHP": 0.5,
        "_probnnk": 0.2,
        "_probnnp": 0.05,
        "_probnnpi": 0.15,
        "pipipi_exclLinePostscale": 1.0,
        "pipipi_exclLinePrescale": 1.0,
        "ppK_exclLinePostscale": 1.0,
        "ppK_exclLinePrescale": 1.0,
        "pppi_exclLinePostscale": 1.0,
        "pppi_exclLinePrescale": 1.0
    },
    "STREAMS": ["Bhadron"],
    "WGs": ["BnoC"]
}

BetaSBs2PhiPhi = {
    "BUILDERTYPE": "StrippingBs2PhiPhiConf",
    "CONFIG": {
        "BsMassWindow": 300,
        "BsVertexCHI2pDOF": 15,
        "KaonIPCHI2": 0.0,
        "KaonPT": 400,
        "PhiMassMax": 1090,
        "PhiMassWindow": 25,
        "PhiPT": 0,
        "PhiPTsq": 1.2,
        "PhiVertexCHI2pDOF": 15,
        "WidePrescale": 0.15
    },
    "STREAMS": {
        "Bhadron": ["StrippingBetaSBs2PhiPhiWideLine"],
        "BhadronCompleteEvent": ["StrippingBetaSBs2PhiPhiLine"]
    },
    "WGs": ["BnoC"]
}

Bs2K0stK0st = {
    "BUILDERTYPE": "StrippingBs2Kst_0Kst_0Conf",
    "CONFIG": {
        "BDIRA": 0.99,
        "BDOCA": 0.3,
        "BFDistanceCHI2": 81.0,
        "BIPCHI2": 25.0,
        "BMassWin": 500.0,
        "BVCHI2": 15.0,
        "KaonIPCHI2": 9.0,
        "KaonPIDK": 2.0,
        "KaonPT": 500.0,
        "KstarAPT": 800.0,
        "KstarMassWin": 800.0,
        "KstarPT": 900.0,
        "KstarVCHI2": 9.0,
        "MaxGHOSTPROB": 0.8,
        "PionIPCHI2": 9.0,
        "PionPIDK": 0.0,
        "PionPT": 500.0,
        "SumPT": 5000
    },
    "STREAMS": ["BhadronCompleteEvent"],
    "WGs": ["BnoC"]
}

Bs2KSKS = {
    "BUILDERTYPE": "Bs2KSKSConf",
    "CONFIG": {
        "B_DD_Doca": 4,
        "B_DD_VtxChi2": 40.0,
        "B_Dira": 0.999,
        "B_LD_Doca": 4,
        "B_LD_VtxChi2": 30.0,
        "B_LL_Doca": 1,
        "B_LL_VtxChi2": 20.0,
        "B_M_Max": 6500,
        "B_M_Min": 4000,
        "KS0_DD_FDChi2": 5,
        "KS0_DD_MassWindow": 80.0,
        "KS0_Dira": 0.999,
        "KS0_LL_FDChi2": 5,
        "KS0_LL_MassWindow": 50.0,
        "Trk_Chi2": 4,
        "Trk_Ghost": 0.5
    },
    "STREAMS": ["Bhadron"],
    "WGs": ["BnoC"]
}

Bs2PhiKst = {
    "BUILDERTYPE": "StrippingBs2PhiKstConf",
    "CONFIG": {
        "BDIRA": 0.99,
        "BDOCA": 0.3,
        "BMassWin": 500.0,
        "BVCHI2": 15.0,
        "KaonIPCHI2": 9.0,
        "KaonPIDK": 0.0,
        "KaonPT": 500.0,
        "KstarMassWin": 150.0,
        "KstarPT": 900.0,
        "KstarVCHI2": 9.0,
        "PhiMassWin": 25.0,
        "PhiPT": 900.0,
        "PhiVCHI2": 9.0,
        "PionIPCHI2": 9.0,
        "PionPIDK": 10.0,
        "PionPT": 500.0
    },
    "STREAMS": ["Bhadron"],
    "WGs": ["BnoC"]
}

BsPhiRho = {
    "BUILDERTYPE": "BsPhiRhoConf",
    "CONFIG": {
        "PRBMaxM": 5600.0,
        "PRBMinM": 4800.0,
        "PRBVtxChi2DOF": 9.0,
        "PRIPCHI2": 20.0,
        "PRPrescale": 1.0,
        "PRResMaxMass": 4000.0,
        "PRResMinMass": 0.0,
        "PRResMinP": 1.0,
        "PRResMinPT": 900.0,
        "PRResVtxChiDOF": 9.0,
        "PRTrackChi2DOF": 4.0,
        "PRTrackGhostProb": 0.4,
        "PRTrackIPCHI2": 4.0,
        "PRTrackMinPT": 250
    },
    "STREAMS": ["BhadronCompleteEvent"],
    "WGs": ["BnoC"]
}

Bu2Ksthh = {
    "BUILDERTYPE": "Bu2KsthhConf",
    "CONFIG": {
        "BDaug_DD_PTsum": 3000.0,
        "BDaug_DD_maxDocaChi2": 5.0,
        "BDaug_LL_PTsum": 3000.0,
        "BDaug_LL_maxDocaChi2": 5.0,
        "BDaug_MaxPT_IP": 0.05,
        "BDaug_MedPT_PT": 800.0,
        "B_APTmin": 1000.0,
        "B_DD_Dira": 0.999,
        "B_DD_FDChi2": 50.0,
        "B_DD_FDwrtPV": 1.0,
        "B_DD_IPCHI2sum": 50.0,
        "B_DD_IPCHI2wrtPV": 8.0,
        "B_LL_Dira": 0.9999,
        "B_LL_FDChi2": 50.0,
        "B_LL_FDwrtPV": 1.0,
        "B_LL_IPCHI2wrtPV": 8.0,
        "B_Mhigh": 921.0,
        "B_Mlow": 1279.0,
        "B_PTmin": 1500.0,
        "B_VtxChi2": 12.0,
        "ConeAngle10": 1.0,
        "ConeAngle15": 1.5,
        "ConeAngle17": 1.7,
        "GEC_MaxTracks": 250,
        "HLT1Dec": "Hlt1(Two)?TrackMVADecision",
        "HLT2Dec": "Hlt2Topo[234]BodyDecision",
        "KS_DD_FDChi2": 50.0,
        "KS_DD_MassWindow": 30.0,
        "KS_DD_VtxChi2": 12.0,
        "KS_LL_FDChi2": 80.0,
        "KS_LL_MassWindow": 20.0,
        "KS_LL_VtxChi2": 12.0,
        "Kstar_MassHi": 5000.0,
        "Kstar_MassLo": 0.0,
        "Postscale": 1.0,
        "Prescale": 1.0,
        "Trk_Down_Chi2": 4.0,
        "Trk_Down_GhostProb": 0.5,
        "Trk_Long_Chi2": 4.0,
        "Trk_Long_GhostProb": 0.4
    },
    "STREAMS": ["Bhadron"],
    "WGs": ["BnoC"]
}

Bu2hhh = {
    "BUILDERTYPE": "Bu2hhhBuilder",
    "CONFIG": {
        "KKK_inclLinePostscale": 1.0,
        "KKK_inclLinePrescale": 1.0,
        "KpKpKp_inclLinePostscale": 1.0,
        "KpKpKp_inclLinePrescale": 1.0,
        "MaxTrSIZE": 200,
        "_3hKKK_Mmax": 6300.0,
        "_3hKKK_Mmin": 5050.0,
        "_3h_CHI2": 12.0,
        "_3h_CORRMmax": 7000.0,
        "_3h_CORRMmin": 4000.0,
        "_3h_DIRA": 0.99998,
        "_3h_DOCA": 0.2,
        "_3h_FDCHI2": 500.0,
        "_3h_IPCHI2": 10.0,
        "_3h_PT": 1000.0,
        "_3h_PTmax": 1500,
        "_3h_PTsum": 4500.0,
        "_3h_PVDOCAmin": 3.0,
        "_3h_PVIPCHI2sum": 500.0,
        "_3h_Psum": 20000.0,
        "_3h_TRKCHIDOFmin": 3.0,
        "_3hpph_deltaMmax": 400,
        "_3hpph_deltaMmin": 200,
        "_h_IPCHI2": 1.0,
        "_h_P": 1500.0,
        "_h_PT": 100.0,
        "_h_TRCHI2DOF": 4.0,
        "_h_TRGHP": 0.5,
        "pph_inclLinePostscale": 1.0,
        "pph_inclLinePrescale": 1.0
    },
    "STREAMS": ["Bhadron"],
    "WGs": ["BnoC"]
}

Bu2rho0rhoPlus = {
    "BUILDERTYPE": "StrippingBu2rho0rhoPlusConf",
    "CONFIG": {
        "PrescaleBu2rho0rhoPlusMerged": 1.0,
        "PrescaleBu2rho0rhoPlusResolved": 1.0,
        "PrescaleBu2rho0rhoPlusUpMerged": 1.0,
        "PrescaleBu2rho0rhoPlusUpResolved": 1.0,
        "isMC": False,
        "longLines": {
            "BuMgdCombMassMax": 7150,
            "BuMgdCombMassMin": 3900,
            "BuMgdMaxIPChi2": 450,
            "BuMgdMaxTrIPChi2Min": 20,
            "BuMgdMinDira": 0.9997,
            "BuMgdMinFDChi2": 120,
            "BuMgdMinPT": 4000,
            "BuMgdMinVChi2Dof": 8,
            "BuMgdMothMassMax": 7000,
            "BuMgdMothMassMin": 4000,
            "BuResCombMassWindow": 650,
            "BuResMaxIPChi2": 30,
            "BuResMaxTrIPChi2Min": 40,
            "BuResMaxTrPTMin": 1900,
            "BuResMinDira": 0.9998,
            "BuResMinFDChi2": 120,
            "BuResMinPT": 1000,
            "BuResMinVChi2Dof": 8,
            "BuResMothMassWindow": 600,
            "BuResSumTrPTMin": 3800,
            "pi0MgdMinPT": 1900,
            "pi0ResMinCL": -1000,
            "pi0ResMinP": 3500,
            "pi0ResMinPT": 400,
            "piPRhoPMgdMinPT": 960,
            "piPRhoPResMinIPChi2": 20,
            "rho0MgdDauMinPT": 100,
            "rho0MgdMaxTrIPChi2Min": 20,
            "rho0MgdMinFDChi2": 25,
            "rho0MgdMinIPChi2": 33,
            "rho0MinVChi2Dof": 14,
            "rho0ResMinFDChi2": 27,
            "rho0ResMinIPChi2": 20,
            "rhoCombMassMax": 1300,
            "rhoCombMassMin": 100,
            "rhoMothMassMax": 1200,
            "rhoMothMassMin": 200,
            "rhoPResMinP": 7000,
            "rhoPResMinPT": 1000
        },
        "refitPVs": True,
        "trMaxChi2Dof": 3.0,
        "trMaxGhostProb": 0.5,
        "trMinIPChi2": 4,
        "trMinProbNNpi": 0.0,
        "trUpMinIPChi2": 8,
        "upstreamLines": {
            "BuMgdCombMassMax": 7150,
            "BuMgdCombMassMin": 3900,
            "BuMgdMaxIPChi2": 55,
            "BuMgdMaxTrIPChi2Min": 55,
            "BuMgdMinDira": 0.999,
            "BuMgdMinFDChi2": 55,
            "BuMgdMinPT": 5000,
            "BuMgdMinVChi2Dof": 8,
            "BuMgdMothMassMax": 7000,
            "BuMgdMothMassMin": 4000,
            "BuResCombMassWindow": 650,
            "BuResMaxIPChi2": 20,
            "BuResMaxTrIPChi2Min": 0,
            "BuResMaxTrPTMin": 1600,
            "BuResMinDira": 0.9998,
            "BuResMinFDChi2": 55,
            "BuResMinPT": 1500,
            "BuResMinVChi2Dof": 8,
            "BuResMothMassWindow": 600,
            "BuResSumTrPTMin": 3500,
            "pi0MgdMinPT": 0,
            "pi0ResMinCL": -1000,
            "pi0ResMinP": 1000,
            "pi0ResMinPT": 400,
            "piPRhoPMgdMinPT": 0,
            "piPRhoPResMinIPChi2": 10,
            "rho0MgdDauMinPT": 0,
            "rho0MgdMaxTrIPChi2Min": 55,
            "rho0MgdMinFDChi2": 55,
            "rho0MgdMinIPChi2": 55,
            "rho0MinVChi2Dof": 14,
            "rho0ResMinFDChi2": 55,
            "rho0ResMinIPChi2": 55,
            "rhoCombMassMax": 1300,
            "rhoCombMassMin": 100,
            "rhoMothMassMax": 1200,
            "rhoMothMassMin": 200,
            "rhoPResMinP": 9000,
            "rhoPResMinPT": 1400
        },
        "vetoTrISMUON": True
    },
    "STREAMS": {
        "Bhadron": [
            "StrippingBu2rho0rhoPlusMergedLine",
            "StrippingBu2rho0rhoPlusResolvedLine",
            "StrippingBu2rho0rhoPlusUpMergedLine",
            "StrippingBu2rho0rhoPlusUpResolvedLine"
        ]
    },
    "WGs": ["BnoC"]
}

Buto5h = {
    "BUILDERTYPE": "Buto5hBuilder",
    "CONFIG": {
        "5pi_exclLinePostscale": 1.0,
        "5pi_exclLinePrescale": 1.0,
        "K4pi_exclLinePostscale": 1.0,
        "K4pi_exclLinePrescale": 1.0,
        "MaxTrSIZE": 200,
        "_5h_CHI2": 12.0,
        "_5h_DIRA": 0.99999,
        "_5h_DOCA": 0.14,
        "_5h_FDCHI2": 500.0,
        "_5h_Mmax": 5679.0,
        "_5h_Mmin": 5079.0,
        "_5h_PT": 1000.0,
        "_5h_PVIPCHI2sum": 400.0,
        "_h_IPCHI2": 6.0,
        "_h_PT": 250.0,
        "_h_TRCHI2DOF": 1.7,
        "_h_TRGHP": 0.2,
        "_probnnk": 0.2,
        "_probnnp": 0.05,
        "_probnnpi": 0.15,
        "pp3pi_exclLinePostscale": 1.0,
        "pp3pi_exclLinePrescale": 1.0,
        "ppKpipi_exclLinePostscale": 1.0,
        "ppKpipi_exclLinePrescale": 1.0
    },
    "STREAMS": ["Bhadron"],
    "WGs": ["BnoC"]
}

D2HHBDT = {
    "BUILDERTYPE": "D2HHBDTLines",
    "CONFIG": {
        "BDTCut": -0.3,
        "BDTWeightsFile": "$TMVAWEIGHTSROOT/data/B2HH_BDT_v1r4.xml",
        "BIP": 0.12,
        "BPT": 1200,
        "BTAU": 0.0,
        "CombMassHigh": 2800,
        "CombMassLow": 1000,
        "DOCA": 0.1,
        "MassHigh": 2600,
        "MassLow": 1800,
        "MinIP": 0.12,
        "MinPT": 1000,
        "PostscaleD02HH": 0.1,
        "PostscaleDSt": 0.3,
        "PrescaleD2HHBDT": 1.0,
        "PrescaleDSt": 1.0,
        "TrChi2": 3,
        "TrGhostProb": 0.5,
        "VertexChi2": 64
    },
    "STREAMS": ["Charm"],
    "WGs": ["BnoC"]
}

Hb2Charged2Body = {
    "BUILDERTYPE": "Hb2Charged2BodyLines",
    "CONFIG": {
        "BIPChi2B2Charged2Body": 12,
        "BPT": 1200,
        "BTAU": 0.0006,
        "CombMassHigh": 6000,
        "CombMassLow": 4600,
        "DOCA": 0.08,
        "MassHigh": 5800,
        "MassLow": 4800,
        "MaxIPChi2B2Charged2Body": 40,
        "MaxPTB2Charged2Body": 1400,
        "MinIPChi2B2Charged2Body": 12,
        "MinPTB2Charged2Body": 1000,
        "PrescaleB2Charged2Body": 1,
        "TrChi2": 4,
        "TrGhostProb": 0.5
    },
    "STREAMS": ["BhadronCompleteEvent"],
    "WGs": ["BnoC"]
}

Hb2V0V0h = {
    "BUILDERTYPE": "Hb2V0V0hConf",
    "CONFIG": {
        "B_APTmin":
        1000.0,
        "B_Dira":
        0.999,
        "B_FDChi2":
        50.0,
        "B_IPCHI2wrtPV":
        12.0,
        "B_Mhigh":
        921.0,
        "B_Mlow":
        1279.0,
        "B_VtxChi2":
        12.0,
        "GEC_MaxTracks":
        250,
        "HLT1Dec":
        "Hlt1(Two)?TrackMVADecision",
        "HLT2Dec":
        "Hlt2Topo[234]BodyDecision",
        "Hb_Mhigh":
        400.0,
        "Hb_Mlow":
        400.0,
        "Postscale":
        1.0,
        "Prescale":
        1.0,
        "Prescale_SameSign":
        1.0,
        "RelatedInfoTools":
        [{
            "ConeAngle": 1.0,
            "Location": "P2ConeVar1",
            "Type": "RelInfoConeVariables",
            "Variables": ["CONEANGLE", "CONEMULT", "CONEPTASYM"]
        },
         {
             "ConeAngle": 1.5,
             "Location": "P2ConeVar2",
             "Type": "RelInfoConeVariables",
             "Variables": ["CONEANGLE", "CONEMULT", "CONEPTASYM"]
         },
         {
             "ConeAngle": 1.7,
             "Location": "P2ConeVar3",
             "Type": "RelInfoConeVariables",
             "Variables": ["CONEANGLE", "CONEMULT", "CONEPTASYM"]
         }, {
             "Location": "VtxIsolationInfo",
             "Type": "RelInfoVertexIsolation"
         }],
        "Trk_Chi2":
        4.0,
        "Trk_GhostProb":
        0.5,
        "V0_DD_FDChi2":
        50.0,
        "V0_DD_MassWindow":
        30.0,
        "V0_DD_VtxChi2":
        12.0,
        "V0_LL_FDChi2":
        80.0,
        "V0_LL_MassWindow":
        20.0,
        "V0_LL_VtxChi2":
        12.0
    },
    "STREAMS": ["Bhadron"],
    "WGs": ["BnoC"]
}

Lb2V04h = {
    "BUILDERTYPE": "Lb2V04hConf",
    "CONFIG": {
        "GEC_MaxTracks":
        250,
        "HLT1Dec":
        "Hlt1(Two)?TrackMVADecision",
        "HLT2Dec":
        "Hlt2Topo[234]BodyDecision",
        "Lambda_DD_FD":
        300.0,
        "Lambda_DD_FDChi2":
        50.0,
        "Lambda_DD_MassWindow":
        25.0,
        "Lambda_DD_Pmin":
        2000.0,
        "Lambda_DD_VtxChi2":
        15.0,
        "Lambda_LD_FD":
        300.0,
        "Lambda_LD_FDChi2":
        50.0,
        "Lambda_LD_MassWindow":
        25.0,
        "Lambda_LD_Pmin":
        5000.0,
        "Lambda_LD_VtxChi2":
        20.0,
        "Lambda_LL_FDChi2":
        80.0,
        "Lambda_LL_MassWindow":
        20.0,
        "Lambda_LL_VtxChi2":
        15.0,
        "LbDaug_DD_PTsum":
        1500.0,
        "LbDaug_DD_maxDocaChi2":
        10.0,
        "LbDaug_LD_PTsum":
        1500.0,
        "LbDaug_LD_maxDocaChi2":
        10.0,
        "LbDaug_LL_PTsum":
        1500.0,
        "LbDaug_LL_maxDocaChi2":
        10.0,
        "LbDaug_MaxPT_IP":
        0.05,
        "LbDaug_MedPT_PT":
        800.0,
        "Lb_2bodyMhigh":
        800.0,
        "Lb_2bodyMlow":
        800.0,
        "Lb_APTmin":
        250.0,
        "Lb_DD_Dira":
        0.995,
        "Lb_DD_FDChi2":
        30.0,
        "Lb_DD_IPCHI2wrtPV":
        20.0,
        "Lb_FDwrtPV":
        1.0,
        "Lb_LD_Dira":
        0.995,
        "Lb_LD_FDChi2":
        30.0,
        "Lb_LD_IPCHI2wrtPV":
        20.0,
        "Lb_LL_Dira":
        0.995,
        "Lb_LL_FDChi2":
        30.0,
        "Lb_LL_IPCHI2wrtPV":
        20.0,
        "Lb_Mhigh":
        600.0,
        "Lb_Mlow":
        1319.0,
        "Lb_PTmin":
        250.0,
        "Lb_VtxChi2":
        12.0,
        "Lbh_DD_PTMin":
        500.0,
        "Lbh_LD_PTMin":
        500.0,
        "Lbh_LL_PTMin":
        500.0,
        "Postscale":
        1.0,
        "Prescale":
        1.0,
        "RelatedInfoTools":
        [{
            "ConeAngle": 1.7,
            "Location": "ConeVar17",
            "Type": "RelInfoConeVariables",
            "Variables": ["CONEANGLE", "CONEMULT", "CONEPTASYM"]
        },
         {
             "ConeAngle": 1.5,
             "Location": "ConeVar15",
             "Type": "RelInfoConeVariables",
             "Variables": ["CONEANGLE", "CONEMULT", "CONEPTASYM"]
         },
         {
             "ConeAngle": 1.0,
             "Location": "ConeVar10",
             "Type": "RelInfoConeVariables",
             "Variables": ["CONEANGLE", "CONEMULT", "CONEPTASYM"]
         },
         {
             "ConeAngle": 0.8,
             "Location": "ConeVar08",
             "Type": "RelInfoConeVariables",
             "Variables": ["CONEANGLE", "CONEMULT", "CONEPTASYM"]
         }, {
             "Location": "VtxIsolationVar",
             "Type": "RelInfoVertexIsolation"
         }],
        "Trk_Chi2":
        3.0,
        "Trk_GhostProb":
        0.5
    },
    "STREAMS": ["Bhadron"],
    "WGs": ["BnoC"]
}

Lb2V0h = {
    "BUILDERTYPE": "Lb2V0hhConf",
    "CONFIG": {
        "GEC_MaxTracks":
        250,
        "HLT1Dec":
        "Hlt1(Two)?TrackMVADecision",
        "HLT2Dec":
        "Hlt2Topo[234]BodyDecision",
        "Lambda_DD_FD":
        300.0,
        "Lambda_DD_FDChi2":
        50.0,
        "Lambda_DD_MassWindow":
        25.0,
        "Lambda_DD_Pmin":
        5000.0,
        "Lambda_DD_VtxChi2":
        15.0,
        "Lambda_LD_FD":
        300.0,
        "Lambda_LD_FDChi2":
        50.0,
        "Lambda_LD_MassWindow":
        25.0,
        "Lambda_LD_Pmin":
        5000.0,
        "Lambda_LD_VtxChi2":
        15.0,
        "Lambda_LL_FDChi2":
        80.0,
        "Lambda_LL_MassWindow":
        20.0,
        "Lambda_LL_VtxChi2":
        15.0,
        "LbDaug_DD_PTsum":
        4200.0,
        "LbDaug_DD_maxDocaChi2":
        5.0,
        "LbDaug_LD_PTsum":
        4200.0,
        "LbDaug_LD_maxDocaChi2":
        5.0,
        "LbDaug_LL_PTsum":
        3000.0,
        "LbDaug_LL_maxDocaChi2":
        5.0,
        "LbDaug_MaxPT_IP":
        0.05,
        "LbDaug_MedPT_PT":
        800.0,
        "Lb_2bodyMhigh":
        800.0,
        "Lb_2bodyMlow":
        800.0,
        "Lb_APTmin":
        1000.0,
        "Lb_DD_Dira":
        0.995,
        "Lb_DD_FDChi2":
        30.0,
        "Lb_DD_IPCHI2wrtPV":
        15.0,
        "Lb_FDwrtPV":
        1.0,
        "Lb_LD_Dira":
        0.995,
        "Lb_LD_FDChi2":
        30.0,
        "Lb_LD_IPCHI2wrtPV":
        15.0,
        "Lb_LL_Dira":
        0.995,
        "Lb_LL_FDChi2":
        30.0,
        "Lb_LL_IPCHI2wrtPV":
        15.0,
        "Lb_Mhigh":
        600.0,
        "Lb_Mlow":
        1319.0,
        "Lb_PTmin":
        800.0,
        "Lb_VtxChi2":
        12.0,
        "Lbh_DD_PTMin":
        500.0,
        "Lbh_LD_PTMin":
        500.0,
        "Lbh_LL_PTMin":
        500.0,
        "Postscale":
        1.0,
        "Prescale":
        1.0,
        "RelatedInfoTools":
        [{
            "ConeAngle": 1.7,
            "Location": "ConeVar17",
            "Type": "RelInfoConeVariables",
            "Variables": ["CONEANGLE", "CONEMULT", "CONEPTASYM"]
        },
         {
             "ConeAngle": 1.5,
             "Location": "ConeVar15",
             "Type": "RelInfoConeVariables",
             "Variables": ["CONEANGLE", "CONEMULT", "CONEPTASYM"]
         },
         {
             "ConeAngle": 1.0,
             "Location": "ConeVar10",
             "Type": "RelInfoConeVariables",
             "Variables": ["CONEANGLE", "CONEMULT", "CONEPTASYM"]
         },
         {
             "ConeAngle": 0.8,
             "Location": "ConeVar08",
             "Type": "RelInfoConeVariables",
             "Variables": ["CONEANGLE", "CONEMULT", "CONEPTASYM"]
         }, {
             "Location": "VtxIsolationVar",
             "Type": "RelInfoVertexIsolation"
         }],
        "Trk_Chi2":
        3.0,
        "Trk_GhostProb":
        0.5
    },
    "STREAMS": ["Bhadron"],
    "WGs": ["BnoC"]
}

Lb2V0p = {
    "BUILDERTYPE": "Lb2V0ppConf",
    "CONFIG": {
        "GEC_MaxTracks":
        250,
        "Lambda_DD_FD":
        300.0,
        "Lambda_DD_FDChi2":
        50.0,
        "Lambda_DD_MassWindow":
        20.0,
        "Lambda_DD_Pmin":
        5000.0,
        "Lambda_DD_VtxChi2":
        9.0,
        "Lambda_LD_FD":
        300.0,
        "Lambda_LD_FDChi2":
        50.0,
        "Lambda_LD_MassWindow":
        25.0,
        "Lambda_LD_Pmin":
        5000.0,
        "Lambda_LD_VtxChi2":
        16.0,
        "Lambda_LL_FDChi2":
        0.0,
        "Lambda_LL_MassWindow":
        20.0,
        "Lambda_LL_VtxChi2":
        9.0,
        "LbDaug_DD_PTsum":
        2000.0,
        "LbDaug_DD_maxDocaChi2":
        16.0,
        "LbDaug_LD_PTsum":
        4200.0,
        "LbDaug_LD_maxDocaChi2":
        5.0,
        "LbDaug_LL_PTsum":
        3000.0,
        "LbDaug_LL_maxDocaChi2":
        5.0,
        "LbDaug_MaxPT_IP":
        0.05,
        "LbDaug_MedPT_PT":
        450.0,
        "Lb_2bodyMhigh":
        800.0,
        "Lb_2bodyMlow":
        800.0,
        "Lb_APTmin":
        1000.0,
        "Lb_DD_Dira":
        0.999,
        "Lb_DD_FDChi2":
        0.5,
        "Lb_DD_IPCHI2wrtPV":
        25.0,
        "Lb_FDwrtPV":
        0.8,
        "Lb_LD_Dira":
        0.999,
        "Lb_LD_FDChi2":
        30.0,
        "Lb_LD_IPCHI2wrtPV":
        15.0,
        "Lb_LL_Dira":
        0.999,
        "Lb_LL_FDChi2":
        0.5,
        "Lb_LL_IPCHI2wrtPV":
        25.0,
        "Lb_Mhigh":
        581.0,
        "Lb_Mlow":
        419.0,
        "Lb_PTmin":
        1050,
        "Lb_VtxChi2":
        16.0,
        "Lbh_DD_PTMin":
        0.0,
        "Lbh_LD_PTMin":
        500.0,
        "Lbh_LL_PTMin":
        0.0,
        "Postscale":
        1.0,
        "Prescale":
        1.0,
        "RelatedInfoTools":
        [{
            "ConeAngle": 1.7,
            "Location": "ConeVar17",
            "Type": "RelInfoConeVariables",
            "Variables": ["CONEANGLE", "CONEMULT", "CONEPTASYM"]
        },
         {
             "ConeAngle": 1.5,
             "Location": "ConeVar15",
             "Type": "RelInfoConeVariables",
             "Variables": ["CONEANGLE", "CONEMULT", "CONEPTASYM"]
         },
         {
             "ConeAngle": 1.0,
             "Location": "ConeVar10",
             "Type": "RelInfoConeVariables",
             "Variables": ["CONEANGLE", "CONEMULT", "CONEPTASYM"]
         },
         {
             "ConeAngle": 0.8,
             "Location": "ConeVar08",
             "Type": "RelInfoConeVariables",
             "Variables": ["CONEANGLE", "CONEMULT", "CONEPTASYM"]
         }, {
             "Location": "VtxIsolationVar",
             "Type": "RelInfoVertexIsolation"
         }],
        "Trk_Chi2":
        3.0,
        "Trk_GhostProb":
        0.5
    },
    "STREAMS": ["Bhadron"],
    "WGs": ["BnoC"]
}

Xb23ph = {
    "BUILDERTYPE": "Xb23phConf",
    "CONFIG": {
        "ConeAngles": [0.8, 1.0, 1.3, 1.7],
        "ConeInputs": {
            "Displaced": ["/Event/Phys/StdNoPIDsPions"],
            "Long": ["/Event/Phys/StdAllNoPIDsPions"]
        },
        "ConeVariables": ["CONEANGLE", "CONEMULT", "CONEPTASYM"],
        "Postscale": 1.0,
        "Prescale": 1.0,
        "Trk_MaxChi2Ndof": 4.0,
        "Trk_MaxGhostProb": 0.4,
        "Trk_MinIPChi2": 16.0,
        "Trk_MinP": 1500.0,
        "Trk_MinProbNNp": 0.05,
        "Xb_MaxDOCAChi2": 20.0,
        "Xb_MaxIPChi2": 16.0,
        "Xb_MaxM_4body": 6405.0,
        "Xb_MaxVtxChi2": 20.0,
        "Xb_MinDira": 0.9999,
        "Xb_MinFDChi2": 50.0,
        "Xb_MinM_4body": 5195.0,
        "Xb_MinPT_4body": 1500.0,
        "Xb_MinSumPT_4body": 3500.0,
        "Xb_MinSumPTppi": 1500.0,
        "Xb_MinSumPTppipi": 2500.0
    },
    "STREAMS": ["Bhadron"],
    "WGs": ["BnoC"]
}

Xb2phh = {
    "BUILDERTYPE": "Xb2phhConf",
    "CONFIG": {
        "Postscale": 1.0,
        "Prescale": 1.0,
        "Trk_MaxChi2Ndof": 3.0,
        "Trk_MaxGhostProb": 0.4,
        "Trk_MinIPChi2": 16.0,
        "Trk_MinP": 1500.0,
        "Trk_MinProbNNp": 0.05,
        "Xb_MaxDOCAChi2": 20.0,
        "Xb_MaxIPChi2": 16.0,
        "Xb_MaxM": 6405.0,
        "Xb_MaxVtxChi2": 20.0,
        "Xb_MinDira": 0.9999,
        "Xb_MinFDChi2": 50.0,
        "Xb_MinM": 5195.0,
        "Xb_MinPT": 1500.0,
        "Xb_MinSumPT": 3500.0,
        "Xb_MinSumPTppi": 1500.0
    },
    "STREAMS": ["Bhadron"],
    "WGs": ["BnoC"]
}

Xb2phhh = {
    "BUILDERTYPE": "Xb2phhhConf",
    "CONFIG": {
        "ConeAngles": [0.8, 1.0, 1.3, 1.7],
        "ConeInputs": {
            "Displaced": ["/Event/Phys/StdNoPIDsPions"],
            "Long": ["/Event/Phys/StdAllNoPIDsPions"]
        },
        "ConeVariables": ["CONEANGLE", "CONEMULT", "CONEPTASYM"],
        "Postscale": 1.0,
        "Prescale": 1.0,
        "Trk_MaxChi2Ndof": 4.0,
        "Trk_MaxGhostProb": 0.4,
        "Trk_MinIPChi2": 16.0,
        "Trk_MinP": 1500.0,
        "Trk_MinProbNNp": 0.05,
        "Xb_MaxDOCAChi2": 20.0,
        "Xb_MaxIPChi2": 16.0,
        "Xb_MaxM_4body": 6405.0,
        "Xb_MaxVtxChi2": 20.0,
        "Xb_MinDira": 0.9999,
        "Xb_MinFDChi2": 50.0,
        "Xb_MinM_4body": 5195.0,
        "Xb_MinPT_4body": 1500.0,
        "Xb_MinSumPT_4body": 3500.0,
        "Xb_MinSumPTppi": 1500.0,
        "Xb_MinSumPTppipi": 2500.0
    },
    "STREAMS": ["Bhadron"],
    "WGs": ["BnoC"]
}
