###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


# R. Lambert 
B0q2DplusMuX = {
    'BUILDERTYPE' : 'B0q2DplusMuXAllLinesConf',
    'CONFIG' : { 
    
                 'Tuned'   : { 'Prescale'    : 1.0 ,
                               'Postscale'   : 1.0 ,
                               'MuPT'        : 500, #MeV
                               'MuPidPi'     : -1.,
                               'MuPidK'      : -5,
                               'MuTrChi2'    : 5,
                               'KPT'         : 400, #MeV
                               'KIP'         : 0.04, #mm
                               'KPidPi'      : 0,
                               'KPidMu'      : 5,
                               'KPidP'       : -10,
                               'KTrChi2'     : 5,
                               'KIPChi2'     : 4,
                               'PiPidK'      : -10,
                               'PiIP'        : 0.04, #mm
                               'PiPidMu'     : -5,
                               'PiTrChi2'    : 10,
                               'PiIPChi2'    : 7,
                               'DPT'         : 1500, #MeV
                               'D_APT'       : 1200, #MeV
                               'D_VPCHI2'    : 0.010,
                               'D_BPVVDCHI2' : 144,
                               'B_VPCHI2'    : 0.010,
                               'B_BPVDIRA'   : 0.9980
                               }
                 
                 },
    'STREAMS' : [ 'Semileptonic' ],
    'WGs'    : ['Semileptonic'] 
    }
