###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

Ccbar2Baryons = {
    'BUILDERTYPE'  : 'Ccbar2BaryonsConf',
    'CONFIG'       : {
    'TRCHI2DOF'        :    5.   ,
    'CombMaxMass'      :  4100.  , # MeV, before Vtx fit
    'CombMinMass'      :  2750.  , # MeV, before Vtx fit
    'MaxMass'          :  4000.  , # MeV, after Vtx fit
    'MinMass'          :  2800.    # MeV, after Vtx fit
    },
    'STREAMS' : [ 'Charm' ],
    'WGs'    : ['FWG']
    }
