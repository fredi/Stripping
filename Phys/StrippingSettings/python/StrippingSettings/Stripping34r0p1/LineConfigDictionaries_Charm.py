###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
B2ppipiSigmacmm_Lcpi = {
    "BUILDERTYPE": "StrippingB2ppipiSigmacmm_Lcpi_Conf", 
    "CONFIG": {
        "ApplyDeltaMCut": False, 
        "ApplyGECs": False, 
        "ApplyGhostProbCut": True, 
        "ApplyPionPIDK": True, 
        "BDIRA": 0.9995, 
        "BFDChi2": 64.0, 
        "BMAXDOCA": 0.2, 
        "BMassWind": 200.0, 
        "BMassWindClosed": 1000.0, 
        "BVtxChi2DOF": 5.0, 
        "CheckPV": True, 
        "ConeAngles": {
            "11": 1.1, 
            "13": 1.3, 
            "15": 1.5, 
            "17": 1.7, 
            "19": 1.9
        }, 
        "ConeVariables": [
            "CONEANGLE", 
            "CONEMULT", 
            "CONEPTASYM"
        ], 
        "DelmLower": 0.0, 
        "DelmUpper": 3000.0, 
        "GhostProbCut": 0.5, 
        "Hlt2TOS": {
            "Hlt2.*Decision%TOS": 0
        }, 
        "LcFDChi2": 64.0, 
        "LcMINDauIPCHI2": 9.0, 
        "LcMINDauPT": 500.0, 
        "LcMINIPCHI2": 9.0, 
        "LcMINPT": 300.0, 
        "LcMassWind": 100.0, 
        "LcVtxChi2DOF": 5.0, 
        "MaxLongTracks": 200, 
        "NoPIDs": False, 
        "PionPIDK": 0.0, 
        "PostscaleComplete": 1.0, 
        "PostscaleCompleteNorm": 1.0, 
        "Postscalefourpart": 1.0, 
        "PrescaleComplete": 1.0, 
        "PrescaleCompleteNorm": 1.0, 
        "Prescalefourpart": 1.0, 
        "SigmaFDChi2": 64.0, 
        "SigmaLcDeltaMhigh": 1000.0, 
        "SigmaLcDeltaMlow": 0.0, 
        "SigmaMAXDOCA": 0.2, 
        "SigmaPT": 400.0, 
        "SigmaVtxChi2DOF": 5.0, 
        "TrackChi2DOF": 3.0, 
        "UseTOS": True, 
        "fourpartFDChi2": 49.0, 
        "fourpartMAXDOCA": 0.15, 
        "fourpartMassHigh": 3000.0, 
        "fourpartMassLow": 1800.0, 
        "fourpartMinIPChi2": 6.0, 
        "fourpartPT": 1000.0, 
        "fourpartVtxChi2DOF": 5.0, 
        "kaonLcMINPIDK": 0.0, 
        "piSigmaMAXP": 10000000.0, 
        "piSigmaMAXPT": 1000000.0, 
        "piSigmaMINIPCHI2": 8.0, 
        "piSigmaMINP": 2000.0, 
        "piSigmaMINPT": 200.0, 
        "pionMINIPCHI2": 8.0, 
        "pionMINP": 5000.0, 
        "pionMINPT": 400.0, 
        "protonLcMINPIDp": 0.0, 
        "protonLcMINPIDp_K": 0.0, 
        "protonMINIPCHI2": 8.0, 
        "protonMINP": 10000.0, 
        "protonMINPIDp": 10.0, 
        "protonMINPIDp_K": 0.0, 
        "protonMINPT": 500.0, 
        "threepartFDChi2": 49.0, 
        "threepartMAXDOCA": 0.15, 
        "threepartMassHigh": 2800.0, 
        "threepartMassLow": 1500.0, 
        "threepartMinIPChi2": 6.0, 
        "threepartPT": 1000.0, 
        "threepartVtxChi2DOF": 5.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

CharmFromBSemi = {
    "BUILDERTYPE": "CharmFromBSemiAllLinesConf", 
    "CONFIG": {
        "BDIRA": 0.999, 
        "BVCHI2DOF": 6.0, 
        "CombMassHigh_HH": 1820, 
        "CombMassLow_HH": 0, 
        "D02HHGammaAMassWin": 220, 
        "D02HHGammaMassWin": 210, 
        "D02HHHHPrescale": 1, 
        "D02HHPi0AMassWin": 220, 
        "D02HHPi0DocaCut": 6.0, 
        "D02HHPi0MassWin": 210, 
        "D02KSHHPi0AMassWin": 220, 
        "D02KSHHPi0MassWin": 210, 
        "D02KSHHPi0_D0PT": 1000, 
        "D02KSHHPi0_D0PTComb": 1000, 
        "D02KSHHPi0_PTSUMLoose": 1000, 
        "D0decaysPrescale": 1, 
        "D0radiativePrescale": 1, 
        "DDocaChi2Max": 20, 
        "DDocaChi2MaxLoose": 100, 
        "DZ": 0, 
        "Ds2HHHHPrescale": 1, 
        "DsAMassWin": 100.0, 
        "DsDIRA": 0.99, 
        "DsFDCHI2": 100.0, 
        "DsIP": 7.4, 
        "DsMassWin": 80.0, 
        "DsVCHI2DOF": 6.0, 
        "DsdecaysPrescale": 1, 
        "Dstar_Chi2": 8.0, 
        "Dstar_SoftPion_PIDe": 2.0, 
        "Dstar_SoftPion_PT": 80.0, 
        "Dstar_wideDMCutLower": 0.0, 
        "Dstar_wideDMCutUpper": 170.0, 
        "Dto4hADocaChi2Max": 7, 
        "Dto4h_AMassWin": 65.0, 
        "Dto4h_MassWin": 60.0, 
        "DtoXgammaADocaChi2Max": 10, 
        "GEC_nLongTrk": 250, 
        "HLT2": "HLT_PASS_RE('Hlt2.*SingleMuon.*Decision') | HLT_PASS_RE('Hlt2Topo.*Decision')", 
        "KPiPT": 250.0, 
        "KSCutDIRA": 0.99, 
        "KSCutZFDFromD": 10.0, 
        "KSDDCutFDChi2": 100, 
        "KSDDCutMass": 30, 
        "KSDDPMin": 3000, 
        "KSDDPTMin": 250, 
        "KSDaugTrackChi2": 4, 
        "KSLLCutFDChi2": 100, 
        "KSLLCutMass": 30, 
        "KSLLPMin": 2000, 
        "KSLLPTMin": 250, 
        "KSVertexChi2": 6, 
        "KaonPIDK": 4.0, 
        "KaonPIDKloose": -5, 
        "LambdaCutDIRA": 0.99, 
        "LambdaDDCutFDChi2": 100, 
        "LambdaDDCutMass": 30, 
        "LambdaDDPMin": 3000, 
        "LambdaDDPTMin": 800, 
        "LambdaDaugTrackChi2": 4, 
        "LambdaLLCutFDChi2": 100, 
        "LambdaLLCutMass": 30, 
        "LambdaLLPMin": 2000, 
        "LambdaLLPTMin": 500, 
        "LambdaVertexChi2": 6, 
        "Lc2HHHPrescale": 1, 
        "Lc2HHPrescale": 1, 
        "Lc2pKKpi0_PTSUMLoose": 1000.0, 
        "MINIPCHI2": 9.0, 
        "MINIPCHI2Loose": 4.0, 
        "MassHigh_HH": 1810, 
        "MassLow_HH": 0, 
        "MaxBMass": 6000, 
        "MaxConvPhDDChi": 9, 
        "MaxConvPhDDMass": 100, 
        "MaxConvPhLLChi": 9, 
        "MaxConvPhLLMass": 100, 
        "MaxVCHI2NDOF_HH": 10.0, 
        "MinBMass": 2500, 
        "MinConvPhDDPT": 800, 
        "MinConvPhLLPT": 800, 
        "MinVDCHI2_HH": 1000.0, 
        "MuonIPCHI2": 4.0, 
        "MuonPT": 800.0, 
        "PIDmu": -0.0, 
        "PTSUM": 1800.0, 
        "PTSUMLoose": 1400.0, 
        "PTSUM_HHGamma": 1800.0, 
        "PTSUM_HHPi0": 1800.0, 
        "PhotonCL": 0.25, 
        "PhotonPT": 1500, 
        "Pi0PMin": 3000, 
        "Pi0PMin_loose": 1000, 
        "Pi0PtMin": 1000, 
        "Pi0PtMin_loose": 0, 
        "PionPIDK": 10.0, 
        "PionPIDKTight": 4.0, 
        "TRCHI2": 4.0, 
        "TTSpecs": {
            "Hlt1.*Track.*Decision%TOS": 0, 
            "Hlt2.*SingleMuon.*Decision%TOS": 0, 
            "Hlt2Global%TIS": 0, 
            "Hlt2Topo.*Decision%TOS": 0
        }, 
        "TrGhostProbMax": 0.5, 
        "b2DstarMuXPrescale": 1
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

D2HHHGamma = {
    "BUILDERTYPE": "D2HHHGammaLines", 
    "CONFIG": {
        "CombMassHigh": 2100.0, 
        "CombMassLow": 1665.0, 
        "Gamma_CL": 0.2, 
        "HighPIDK": -1, 
        "Hlt1Filter": None, 
        "Hlt2Filter": None, 
        "LowPIDK": 5, 
        "MassHigh": 2100.0, 
        "MassLow": 1685.0, 
        "MaxADOCACHI2": 10.0, 
        "MaxMass_CNV_DD": 100.0, 
        "MaxMass_CNV_LL": 100.0, 
        "MaxVCHI2NDOF": 12.0, 
        "MaxVCHI2_CNV_DD": 9, 
        "MaxVCHI2_CNV_LL": 9, 
        "MinBPVDIRA": 0.99998, 
        "MinBPVTAU": 0.0003, 
        "MinCombPT": 3000.0, 
        "MinPT_CNV_DD": 1000.0, 
        "MinPT_CNV_LL": 1000.0, 
        "MinTrkIPChi2": 5, 
        "MinTrkPT": 300.0, 
        "PrescaleD2KKPiGamma": 1, 
        "PrescaleD2KKPiGamma_CNVDD": 1, 
        "PrescaleD2KKPiGamma_CNVLL": 1, 
        "PrescaleD2KPiPiGamma": 1, 
        "PrescaleD2KPiPiGamma_CNVDD": 1, 
        "PrescaleD2KPiPiGamma_CNVLL": 1, 
        "PrescaleD2PiPiPiGamma": 1, 
        "PrescaleD2PiPiPiGamma_CNVDD": 1, 
        "PrescaleD2PiPiPiGamma_CNVLL": 1, 
        "TrChi2": 4, 
        "TrGhostProb": 0.5, 
        "photonPT": 2000.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

CharmWeakDecays = {
    'BUILDERTYPE'       : 'CharmWeakDecaysConf',
    'CONFIG'    : {
        #D0 and D0*
        'MinTrackPT'         : 800.      # MeV
        ,'DaugPtMax'         : 1500.     # MeV
        ,'TrGhostProb'       : 0.3       # Dimensionless
        ,'MinTrackIPchi2'    : 9         # Dimensionless
        ,'KaonPID'        : 7.        # Dimensionless
        ,'PionPID'        : 0.        # Dimensionless
        ,'MinD0stMass'       : 1750      # MeV
        ,'MaxD0stMass'       : 2120      # MeV
        ,'MinD0stPT'         : 2000      # MeV
        ,'MaxD0stVertChi2DOF': 16        # Dimensionless 
        #Ds1
        ,'MinKaonPT'         : 250       # MeV
        ,'Ds1PT'             : 4000      # MeV 
        ,'QValueDs1Decay'    : 80        # MeV
        ,'MaxDs1VertChi2DOF' : 9         # Dimensionless 
        # Pre- and postscales
        ,'CharmWeakDecaysPreScale'     : 1.0
        ,'CharmWeakDecaysPostScale'    : 1.0
        },
    'STREAMS' : [ 'Charm' ],
    'WGs'    : [ 'Charm' ]
    }

DstarD2XGamma = {
    "BUILDERTYPE": "DstarD2XGammaLines", 
    "CONFIG": {
        "CombMassHigh": 2130.0, 
        "CombMassHigh_HH": 1865.0, 
        "CombMassLow": 1610.0, 
        "CombMassLow_HH": 0.0, 
        "Daug_TRCHI2DOF_MAX": 3, 
        "Dstar_AMDiff_MAX": 180.0, 
        "Dstar_MDiff_MAX": 163.0, 
        "Dstar_VCHI2VDOF_MAX": 15.0, 
        "HighPIDK": 0, 
        "Hlt1Filter": None, 
        "Hlt1Tos": {
            "Hlt1.*Track.*Decision%TOS": 0, 
            "Hlt1IncPhi.*Decision%TOS": 0
        }, 
        "Hlt2Filter": None, 
        "Hlt2Tos": {
            "Hlt2CharmHadDst2PiD02.*Gamma.*Decision%TOS": 0, 
            "Hlt2CharmHadInclDst2PiD02HHXBDTDecision%TOS": 0, 
            "Hlt2PhiIncPhiDecision%TOS": 0
        }, 
        "LowPIDK": 5, 
        "MassHigh": 2100.0, 
        "MassHigh_HH": 1865.0, 
        "MassLow": 1640.0, 
        "MassLow_HH": 0.0, 
        "MaxADOCACHI2": 10.0, 
        "MaxIPChi2": 25.0, 
        "MaxMass_CNV_DD": 100.0, 
        "MaxMass_CNV_LL": 100.0, 
        "MaxVCHI2NDOF": 12.0, 
        "MaxVCHI2NDOF_HH": 16.0, 
        "MaxVCHI2_CNV_DD": 9, 
        "MaxVCHI2_CNV_LL": 9, 
        "MinBPVDIRA": 0.99995, 
        "MinBPVTAU": 0.0001, 
        "MinCombPT": 0.0, 
        "MinPT": 2000.0, 
        "MinPT_CNV_DD": 1000.0, 
        "MinPT_CNV_LL": 1000.0, 
        "MinPhotonCL": 0.25, 
        "MinTrkIPChi2": 6, 
        "MinTrkPT": 500.0, 
        "MinVDCHI2_HH": 1000.0, 
        "MinVDCHI2_HHComb": 1000.0, 
        "PrescaleDstarD2KKGamma": 1, 
        "PrescaleDstarD2KKGamma_CNVDD": 1, 
        "PrescaleDstarD2KKGamma_CNVLL": 1, 
        "PrescaleDstarD2KPiGamma": 1, 
        "PrescaleDstarD2KPiGamma_CNVDD": 1, 
        "PrescaleDstarD2KPiGamma_CNVLL": 1, 
        "PrescaleDstarD2PiPiGamma": 1, 
        "PrescaleDstarD2PiPiGamma_CNVDD": 1, 
        "PrescaleDstarD2PiPiGamma_CNVLL": 1, 
        "TrChi2": 4, 
        "TrGhostProb": 0.5, 
        "photonPT": 1700.0
    }, 
    "STREAMS": [ "CharmCompleteEvent" ], 
    "WGs": [ "Charm" ]
}
