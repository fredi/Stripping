###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
##                       S T R I P P I N G  3 4 r 0 p 1                       ##
##                                                                            ##
##  Configuration for B2CC WG                                                 ##
##  Contact person: Xuesong Liu           (xuesong.liu@cern.ch)               ##
################################################################################

from GaudiKernel.SystemOfUnits import *

######################################################################
## StrippingBetaSBd2JpsieeKstarDetachedLine (MicroDST)
## StrippingBetaSBd2JpsieeKstarLine (MicroDST)
## -------------------------------------------------------------------
## Lines defined in StrippingBd2JpsieeKstar.py
## Authors: Artur Ukleja, Jibo He, Konrad Klimaszewski, Varvara Batozskay
## Last changes made by Varvara Batozskay
######################################################################

BetaSBd2JpsieeKstar = {
    "BUILDERTYPE": "Bd2JpsieeKstarConf", 
    "CONFIG": {
        "BdDIRA": 0.99, 
        "BdMassMax": 6000.0, 
        "BdMassMaxLoose": 6000.0, 
        "BdMassMin": 4000.0, 
        "BdMassMinLoose": 4300.0, 
        "BdVertexCHI2pDOF": 10.0, 
        "BdVertexCHI2pDOFLoose": 10.0, 
        "ElectronPID": 0.0, 
        "ElectronPIDLoose": 1.0, 
        "ElectronPT": 500.0, 
        "ElectronPTLoose": 500.0, 
        "ElectronTrackCHI2pDOF": 5.0, 
        "ElectronTrackCHI2pDOFLoose": 3.0, 
        "JpsiMassMax": 3600.0, 
        "JpsiMassMaxLoose": 3500.0, 
        "JpsiMassMin": 1900.0, 
        "JpsiMassMinLoose": 2200.0, 
        "JpsiPT": 400.0, 
        "JpsiPTLoose": 400.0, 
        "JpsiVertexCHI2pDOF": 15.0, 
        "JpsiVertexCHI2pDOFLoose": 11.0, 
        "KaonPID": -3.0, 
        "KaonPIDLoose": 0.0, 
        "KaonTrackCHI2pDOF": 5.0, 
        "KaonTrackCHI2pDOFLoose": 3.0, 
        "KstMassWindow": 150.0, 
        "KstMassWindowLoose": 100.0, 
        "KstPT": 1000.0, 
        "KstPTLoose": 1000.0, 
        "KstVertexCHI2pDOF": 15.0, 
        "KstVertexCHI2pDOFLoose": 10.0, 
        "LifetimeCut": " & (BPVLTIME()>0.3*ps)", 
        "PionPID": 10.0, 
        "PionPIDLoose": 5.0, 
        "PionTrackCHI2pDOF": 5.0, 
        "PionTrackCHI2pDOFLoose": 3.0, 
        "Prescale": 0.05, 
        "PrescaleLoose": 1.0
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "B2CC" ]
}

######################################################################
## StrippingB2JpsiX0Bs2JpsiPi0RLine (FullDST)
## StrippingB2JpsiX0B02JpsiPi0MLine (FullDST)
## StrippingB2JpsiX0Bu2JpsiKstRLine (FullDST)
## StrippingB2JpsiX0Bu2JpsiKstMLine (FullDST)
## StrippingB2JpsiX0Bu2JpsiKLine (FullDST)
## StrippingB2JpsiX0Bs2JpsiEtaRLine (FullDST)
## StrippingB2JpsiX0Bs2JpsiEta2PiPiPi0RLine (FullDST)
## StrippingB2JpsiX0Bs2JpsiEta2PiPiPi0MLine (FullDST)
## StrippingB2JpsiX0Bs2JpsiEta2PiPiGammaLine (FullDST)
## StrippingB2JpsiX0Bs2JpsiEtap2RhoGammaLine (FullDST)
## StrippingB2JpsiX0Bs2JpsiEtap2EtaPiPiLine (FullDST)
## -------------------------------------------------------------------
## Lines defined in StrippingB2JpsiPi0.py
## Authors: Maximilien Chefdeville
## Last changes made by Maximilien Chefdeville
######################################################################

B2JpsiX0 = {
    "BUILDERTYPE": "B2JpsiX0Conf", 
    "CONFIG": {
        "B02JpsiPi0RMVACut": "-0.56", 
        "B02JpsiPi0RXmlFile": "$TMVAWEIGHTSROOT/data/B2JpsiX0/B02JpsiPi0R_fisher_run2_v1r1.xml", 
        "Bs2JpsiEtaRMVACut": "-0.73", 
        "Bs2JpsiEtaRXmlFile": "$TMVAWEIGHTSROOT/data/B2JpsiX0/Bs2JpsiEtaR_fisher_run2_v1r1.xml", 
        "Bu2JpsiKstRMVACut": "-1.19", 
        "Bu2JpsiKstRXmlFile": "$TMVAWEIGHTSROOT/data/B2JpsiX0/Bu2JpsiKstR_fisher_run2_v1r1.xml", 
        "DIRACut": 0.9995, 
        "IPCHI2Cut": 20, 
        "IPCut": 0.2, 
        "JpsiMassWindow": 80, 
        "LTimeCut": 0.2, 
        "PrescaleB02JpsiPi0M": 1, 
        "PrescaleB02JpsiPi0R": 1, 
        "PrescaleBs2JpsiEta2PiPiGamma": 1, 
        "PrescaleBs2JpsiEta2PiPiPi0M": 1, 
        "PrescaleBs2JpsiEta2PiPiPi0R": 1, 
        "PrescaleBs2JpsiEtaR": 1, 
        "PrescaleBs2JpsiEtap2EtaPiPi": 1, 
        "PrescaleBs2JpsiEtap2RhoGamma": 1, 
        "PrescaleBu2JpsiK": 0.1, 
        "PrescaleBu2JpsiKstM": 1, 
        "PrescaleBu2JpsiKstR": 1, 
        "VCHI2PDOFCut": 10
    }, 
    "STREAMS": [ "Dimuon" ], 
    "WGs": [ "B2CC" ]
}
