###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__ = "Regis Lefevre, Daniel O'Hanlon"
__date__ = '20/10/2017'
__version__ = '$Revision: 2.0 $'

'''
Stripping selection for B -> h h pi0
'''
#################################################################
#  This strip is used both for Bd -> pi pi pi0 and Bs -> K pi pi0
#  B Mass window : 4200 to 6400 MeV/c2
#  2 lines : one for merged, one for resolved pi0
#################################################################

__all__ = ('StrippingB2HHPi0Conf',
           'makeB2HHPi0R',
           'makeB2HHPi0M',
           'makePi02Conv',
           'makePi01Conv',
           'makeB2HHPi02Conv',
           'makeB2HHPi01Conv',
           'default_config')

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdNoPIDsPions,StdLooseMergedPi0,StdLooseResolvedPi0
from StandardParticles import StdAllLooseGammaDD,StdAllLooseGammaLL,StdLooseAllPhotons

default_config = {
    'NAME'        : 'B2HHPi0',
    'WGs'         : ['BnoC'],
    'BUILDERTYPE' : 'StrippingB2HHPi0Conf',
    'CONFIG'      : { 'PiMinPT'              : 500,
                      'PiMinP'               : 5000,
                      'PiMinTrackProb'       : 0.000001,
                      'PiMaxGhostProb'       : 0.5,
                      'PiMinIPChi2'          : 25,
                      'Pi0MinPT_M'           : 2500,
                      'Pi0MinPT_R'           : 1500,
                      'ResPi0MinGamCL'       : 0.2,
                      'BMinM'                : 4200,
                      'BMaxM'                : 6400,
                      'BMinPT_M'             : 3000,
                      'BMinPT_R'             : 2500,
                      'BMinVtxProb'          : 0.001,
                      'BMaxIPChi2'           : 9,
                      'BMaxIPChi2Conv'       : 20,
                      'BMinDIRA'             : 0.99995,
                      'BMinVVDChi2'          : 64,
                      'BMinVVDChi2Conv'      : 32,
                      'MergedLinePrescale'   : 1.,
                      'MergedLinePostscale'  : 1.,
                      'ResolvedLinePrescale' : 1.,
                      'ResolvedLinePostscale': 1.,
                      'ConvLinePrescale'     : 1.,
                      'ConvLinePostscale'    : 1.,
                      },
    'STREAMS'     : ['Bhadron']
    }

class StrippingB2HHPi0Conf(LineBuilder) :

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :

        LineBuilder.__init__(self, name, config)

        myPions       = StdNoPIDsPions
        myMergedPi0   = StdLooseMergedPi0
        myResolvedPi0 = StdLooseResolvedPi0

        myDDConvPhotons = StdAllLooseGammaDD
        myLLConvPhotons = StdAllLooseGammaLL
        myPhotons = StdLooseAllPhotons

        #---------------------------------------
        # ExtraInfo

        ExtraInfoTools = [{'Type' : 'ConeVariables',
                           'ConeNumber' : 1,
                           'ConeAngle' : 1.0,
                           'Variables' : ['angle', 'mult','p','pt',
                                          'ptasy','pasy']},
                          {'Type' : 'ConeVariables',
                           'ConeNumber' : 2,
                           'ConeAngle' : 1.5,
                           'Variables' : ['angle', 'mult','p','pt',
                                          'ptasy','pasy']},
                          {'Type' : 'ConeVariables',
                           'ConeNumber' : 3,
                           'ConeAngle' : 2.0,
                           'Variables' : ['angle', 'mult','p','pt',
                                          'ptasy','pasy']},
                          {'Type' : 'VertexIsolation'}]

        #---------------------------------------
        # pi0 -> gamma gamma selections
        convertedPhotons = {"DD" : [myDDConvPhotons],
                            "DL" : [myDDConvPhotons, myLLConvPhotons],
                            "LL" : [myLLConvPhotons]}

        self.my2ConvPi0s = {}

        for tracks, inputs in convertedPhotons.iteritems():
            self.my2ConvPi0s[tracks] = makePi02Conv(name + '2ConvPi0' + tracks,
                                                    config,
                                                    DecayDescriptor = 'pi0 -> gamma gamma',
                                                    inputSel = inputs)

        self.my1ConvPi0s = {}

        self.my1ConvPi0s['DD'] = makePi01Conv(name + '1ConvPi0' + 'DD',
                                                config,
                                                DecayDescriptor = 'pi0 -> gamma gamma',
                                                inputSel = convertedPhotons['DD'] + [myPhotons])

        self.my1ConvPi0s['LL'] = makePi01Conv(name + '1ConvPi0' + 'LL',
                                                config,
                                                DecayDescriptor = 'pi0 -> gamma gamma',
                                                inputSel = convertedPhotons['LL'] + [myPhotons])
        # B -> HHPi0 selections
        self.selresolved = makeB2HHPi0R( name + 'R',
	                                 config,
                                         DecayDescriptor = 'B0 -> pi+ pi- pi0',
					                     inputSel = [myPions, myResolvedPi0]
                                         )
        self.selmerged = makeB2HHPi0M( name + 'M',
	                               config,
                                       DecayDescriptor = 'B0 -> pi+ pi- pi0',
                                       inputSel = [myPions, myMergedPi0]
                                       )

        self.sel2Conv = {}

        for tracks, pi0inputs in self.my2ConvPi0s.iteritems():
            self.sel2Conv[tracks] = makeB2HHPi02Conv(name + '2ConvPi0' + tracks,
                                                     config,
                                                     DecayDescriptor = 'B0 -> pi+ pi- pi0',
                                                     inputSel = [myPions, pi0inputs])


        self.sel1Conv = {}

        self.sel1Conv['DD'] = makeB2HHPi01Conv(name + '1ConvPi0' + 'DD',
                                                 config,
                                                 DecayDescriptor = 'B0 -> pi+ pi- pi0',
                                                 inputSel = [myPions, self.my1ConvPi0s['DD']])

        self.sel1Conv['LL'] = makeB2HHPi01Conv(name + '1ConvPi0' + 'LL',
                                                 config,
                                                 DecayDescriptor = 'B0 -> pi+ pi- pi0',
                                                 inputSel = [myPions, self.my1ConvPi0s['LL']])
        #---------------------------------------
        # Stripping lines
        self.B2HHPi0R_line = StrippingLine(name + "_R" %locals()['config'],
                                           prescale = config['ResolvedLinePrescale'],
                                           postscale = config['ResolvedLinePostscale'],
                                           selection = self.selresolved,
                                           EnableFlavourTagging = True,
                                           RequiredRawEvents = ["Calo"],
                                           MDSTFlag = True,
                                           ExtraInfoTools = ExtraInfoTools
                                           )
        self.B2HHPi0M_line = StrippingLine(name + "_M" %locals()['config'],
                                           prescale = config['MergedLinePrescale'],
                                           postscale = config['MergedLinePostscale'],
                                           selection = self.selmerged,
                                           EnableFlavourTagging = True,
                                           RequiredRawEvents = ["Calo"],
                                           MDSTFlag = True,
                                           ExtraInfoTools = ExtraInfoTools
                                           )

        self.convLines = {}

        for tracks, line in self.sel2Conv.iteritems():
            self.convLines['2Conv' + tracks] = StrippingLine(name + "_2Conv" + tracks,
                                                             prescale = config['ConvLinePrescale'],
                                                             postscale = config['ConvLinePostscale'],
                                                             selection = line,
                                                             EnableFlavourTagging = True,
                                                             RequiredRawEvents = ["Calo"],
                                                             MDSTFlag = True,
                                                             ExtraInfoTools = ExtraInfoTools
                                                            )

        self.convLines['1Conv' + 'DD'] = StrippingLine(name + "_1Conv" + "DD",
                                                         prescale = config['ConvLinePrescale'],
                                                         postscale = config['ConvLinePostscale'],
                                                         selection = self.sel1Conv['DD'],
                                                         EnableFlavourTagging = True,
                                                         RequiredRawEvents = ["Calo"],
                                                         MDSTFlag = True,
                                                         ExtraInfoTools = ExtraInfoTools
                                                       )
        self.convLines['1Conv' + 'LL'] = StrippingLine(name + "_1Conv" + "LL",
                                                         prescale = config['ConvLinePrescale'],
                                                         postscale = config['ConvLinePostscale'],
                                                         selection = self.sel1Conv['LL'],
                                                         EnableFlavourTagging = True,
                                                         RequiredRawEvents = ["Calo"],
                                                         MDSTFlag = True,
                                                         ExtraInfoTools = ExtraInfoTools
                                                       )
        # register lines
        self.registerLine(self.B2HHPi0R_line)
        self.registerLine(self.B2HHPi0M_line)
        for line in self.convLines.values():
            self.registerLine(line)


##############################################################
def makeB2HHPi0R( name,
                  config,
                  DecayDescriptor,
                  inputSel
                ) :

    _piCuts = "(PT>%(PiMinPT)s *MeV) & (P>%(PiMinP)s *MeV) & (TRPCHI2>%(PiMinTrackProb)s) & (TRGHOSTPROB<%(PiMaxGhostProb)s) & (MIPCHI2DV(PRIMARY)>%(PiMinIPChi2)s)" % locals()['config']
    _pi0Cuts = "(PT>%(Pi0MinPT_R)s *MeV) & (CHILD(CL,1)>%(ResPi0MinGamCL)s) & (CHILD(CL,2)>%(ResPi0MinGamCL)s)" % locals()['config']
    _daughterCuts = { 'pi+' : _piCuts, 'pi-' : _piCuts, 'pi0' : _pi0Cuts }
    _combCuts = "(AM>%(BMinM)s *MeV) & (AM<%(BMaxM)s *MeV)" % locals()['config']
    _motherCuts = "(PT>%(BMinPT_R)s *MeV) & (VFASPF(VPCHI2)>%(BMinVtxProb)s) & (BPVVDCHI2>%(BMinVVDChi2)s) & (BPVIPCHI2()<%(BMaxIPChi2)s) & (BPVDIRA>%(BMinDIRA)s)" % locals()['config']

    _B = CombineParticles( DecayDescriptor = DecayDescriptor,
                           MotherCut = _motherCuts,
                           CombinationCut = _combCuts,
                           DaughtersCuts = _daughterCuts,
                           ReFitPVs = True
                           )

    return Selection( name+'Sel',
                      Algorithm = _B,
                      RequiredSelections = inputSel
                      )
##############################################################
def makeB2HHPi0M( name,
                  config,
                  DecayDescriptor,
                  inputSel
                ) :

    _piCuts = "(PT>%(PiMinPT)s *MeV) & (P>%(PiMinP)s *MeV) & (TRPCHI2>%(PiMinTrackProb)s) & (TRGHOSTPROB<%(PiMaxGhostProb)s) & (MIPCHI2DV(PRIMARY)>%(PiMinIPChi2)s)" % locals()['config']
    _pi0Cuts = "(PT>%(Pi0MinPT_M)s *MeV)" % locals()['config']
    _daughterCuts = { 'pi+' : _piCuts, 'pi-' : _piCuts, 'pi0' : _pi0Cuts }
    _combCuts = "(AM>%(BMinM)s *MeV) & (AM<%(BMaxM)s *MeV)" % locals()['config']
    _motherCuts = "(PT>%(BMinPT_M)s *MeV) & (VFASPF(VPCHI2)>%(BMinVtxProb)s) & (BPVVDCHI2>%(BMinVVDChi2)s) & (BPVIPCHI2()<%(BMaxIPChi2)s) & (BPVDIRA>%(BMinDIRA)s)" % locals()['config']

    _B = CombineParticles( DecayDescriptor = DecayDescriptor,
                           MotherCut = _motherCuts,
                           CombinationCut = _combCuts,
                           DaughtersCuts = _daughterCuts,
                           ReFitPVs = True
                           )

    return Selection( name+'Sel',
                      Algorithm = _B,
                      RequiredSelections = inputSel
                      )
##############################################################
def makePi02Conv( name,
                  config,
                  DecayDescriptor,
                  inputSel
                ) :

    _combCuts = "(AM>90*MeV) & (AM<170*MeV)"
    _motherCuts = "(PT>%(Pi0MinPT_R)s *MeV)" % locals()['config']

    _pi0 = CombineParticles( DecayDescriptor = DecayDescriptor,
                           MotherCut = _motherCuts,
                           CombinationCut = _combCuts,
                           Preambulo = ["long = switch(INTES('StdAllLooseGammaLL',False),1,0)"],
                           ReFitPVs = True
                           )

    # If LD, make sure exactly one is long
    if 'DL' in name[-2:]:
        _pi0.CombinationCut += ' & ( 1 == ACHILD(1,long)+ACHILD(2,long) )'

    return Selection( name,
                      Algorithm = _pi0,
                      RequiredSelections = inputSel
                      )
##############################################################
def makePi01Conv( name,
                  config,
                  DecayDescriptor,
                  inputSel
                ) :

    _combCuts = "(AM>90*MeV) & (AM<170*MeV) & ( 1 == ACHILD(1,cnv)+ACHILD(2,cnv) )"
    _motherCuts = "(PT>%(Pi0MinPT_R)s *MeV)" % locals()['config']

    _pi0 = CombineParticles( DecayDescriptor = DecayDescriptor,
                             MotherCut = _motherCuts,
                             CombinationCut = _combCuts,
                             Preambulo = ["cnv = switch(INTES('StdLooseAllPhotons',False),1,0)"],
                           )

    _pi0.ParticleCombiners = { '' : 'MomentumCombiner:PUBLIC'}

    return Selection( name,
                      Algorithm = _pi0,
                      RequiredSelections = inputSel
                      )
##############################################################
def makeB2HHPi02Conv( name,
                      config,
                      DecayDescriptor,
                      inputSel
                    ) :

    _piCuts = "(PT>%(PiMinPT)s *MeV) & (P>%(PiMinP)s *MeV) & (TRPCHI2>%(PiMinTrackProb)s) & (TRGHOSTPROB<%(PiMaxGhostProb)s) & (MIPCHI2DV(PRIMARY)>%(PiMinIPChi2)s)" % locals()['config']
    _daughterCuts = { 'pi+' : _piCuts, 'pi-' : _piCuts}
    _combCuts = "(AM>%(BMinM)s *MeV) & (AM<%(BMaxM)s *MeV)" % locals()['config']
    _motherCuts = "(PT>%(BMinPT_R)s *MeV) & (BPVVDCHI2>%(BMinVVDChi2Conv)s) & (BPVIPCHI2()<%(BMaxIPChi2Conv)s) & (BPVDIRA>%(BMinDIRA)s)" % locals()['config']

    _B = CombineParticles( DecayDescriptor = DecayDescriptor,
                           MotherCut = _motherCuts,
                           CombinationCut = _combCuts,
                           DaughtersCuts = _daughterCuts,
                           ReFitPVs = True
                           )

    return Selection( name+'Sel',
                      Algorithm = _B,
                      RequiredSelections = inputSel
                      )
##############################################################
def makeB2HHPi01Conv( name,
                      config,
                      DecayDescriptor,
                      inputSel
                    ) :

    _piCuts = "(PT>%(PiMinPT)s *MeV) & (P>%(PiMinP)s *MeV) & (TRPCHI2>%(PiMinTrackProb)s) & (TRGHOSTPROB<%(PiMaxGhostProb)s) & (MIPCHI2DV(PRIMARY)>%(PiMinIPChi2)s)" % locals()['config']
    _pi0Cuts = "(PT>%(Pi0MinPT_M)s *MeV)" % locals()['config']
    _daughterCuts = { 'pi+' : _piCuts, 'pi-' : _piCuts}
    _combCuts = "(AM>%(BMinM)s *MeV) & (AM<%(BMaxM)s *MeV)" % locals()['config']
    _motherCuts = "(PT>%(BMinPT_R)s *MeV) & (BPVVDCHI2>%(BMinVVDChi2Conv)s) & (BPVIPCHI2()<%(BMaxIPChi2Conv)s) & (BPVDIRA>%(BMinDIRA)s)" % locals()['config']

    _B = CombineParticles( DecayDescriptor = DecayDescriptor,
                           MotherCut = _motherCuts,
                           CombinationCut = _combCuts,
                           DaughtersCuts = _daughterCuts,
                           ReFitPVs = True
                           )

    return Selection( name+'Sel',
                      Algorithm = _B,
                      RequiredSelections = inputSel
                      )
##############################################################
