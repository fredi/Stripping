###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__ = 'Simone Stracka'
__date__ = '16/07/2014'
__version__ = '$Revision: 1.6 $'

'''
Stripping selection 
'''
####################################################################
#
# Copy (with tightened selection) of D+ -> eta' pi+
# (eta' -> pi+ pi- gamma) line from Charm stream, to be used for
# calibration.
# 
# contacts: Simone Stracka, Max Chefdeville
# 
####################################################################

__all__ = ('StrippingDp2EtaPrimePipConf',
           'makeEtap',
           'makeD2EtapPi',
           'default_config')

from Gaudi.Configuration import *

from PhysSelPython.Wrappers import Selection, DataOnDemand

from StandardParticles import StdAllNoPIDsPions, StdLooseAllPhotons
from GaudiKernel.SystemOfUnits import MeV, GeV, mm, mrad, picosecond
from StrippingUtils.Utils import LineBuilder
from StrippingConf.StrippingLine import StrippingLine

#from Configurables import CombineParticles, FilterDesktop
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles, ConjugateNeutralPID, DaVinci__N3BodyDecays

default_config = {
    'Dp2EtaPrimePip': {
        'WGs'         : ['ALL'],
        'BUILDERTYPE' : 'StrippingDp2EtaPrimePipConf',
        'STREAMS'     : ["Calibration"], 
        'CONFIG': {
              'Bachelor_IPCHI2_MIN'      : 25.0,        # 25
              'Bachelor_PT_MIN'          : 600  *MeV,   # 600
              'Bachelor_P_MIN'           : 1000 *MeV,
              'Bachelor_PIDK_MIN'        : -999.0, # not used
              'Daug_IPCHI2_MIN'      : 25.0,        # 25 --> 16
              'Daug_PT_MIN'          : 500  *MeV,   # 600 --> 500
              'Daug_P_MIN'           : 1000 *MeV,
              'Daug_P_MAX'           : 100000 *MeV,
              'Daug_ETA_MIN'         : 2.0,
              'Daug_ETA_MAX'         : 5.0,
              'Daug_TRCHI2DOF_MAX'   : 5,
              'Daug_TRGHOSTPROB_MAX' : 0.5,
              'Pi_PIDK_MAX'        : 0,
              'Neut_PT_MIN'        : 1000  *MeV,
              'Dipion_Mass_MIN'    : 200 *MeV,
              'Dipion_Mass_MAX'    : 1200 *MeV,
              'Dipion_DOCACHI2_MAX': 15,
              'Res_Mass_MIN'       : 900   *MeV,  # 900 --> 800
              'Res_Mass_MAX'       : 1050  *MeV,   # 990 --> 1050
              'D_PT_Min'           : 2000 *MeV,   # 2000
              'D_Mass_MIN'          : 1750 *MeV,
              'D_Mass_MAX'          : 2100 *MeV,
              'D_VCHI2PDOF_MAX'         : 5,
              'D_BPVLTIME_MIN'       : 0.25*picosecond,
              'DTF_CHI2NDOF_MAX'   : 5,
              'D_BPVDIRA_MIN'   : 0.999975,
              'D_IP_MAX'   : 0.05 * mm,
              'D_IPCHI2_MAX'   : 10,
              'Hlt1Filter'         : None,
              #'Hlt2Filter'      : "HLT_PASS_RE('Hlt2CharmHadDp2Etap.*Decision') | HLT_PASS_RE('Hlt2Topo.*Decision')",
              'Hlt2Filter'      : "HLT_PASS_RE('Hlt2.*D.*Etap.*Decision') | HLT_PASS_RE('Hlt2Topo.*Decision')",
              'PrescaleDp2PipEtaPrimePPG' : 1,
              'PostscaleDp2PipEtaPrimePPG': 1
              }
        }
    }


class StrippingDp2EtaPrimePipConf(LineBuilder) :

    """Creates LineBuilder object containing the stripping lines."""
    # Allowed configuration keys
    __configuration_keys__ = default_config['Dp2EtaPrimePip']['CONFIG'].keys()
    
    # Decay descriptors
    D2PiEtaPrime = ["[D+ -> eta_prime pi+]cc"]    
    EtaPrimePPG = ["eta_prime -> pi+ pi- gamma"]
    
    def __init__(self, name, config):
        """Initialise this LineBuilder instance."""
        self.name = name
        self.config = config
        LineBuilder.__init__(self, name, config)
        
        etaprimeppg_name = '{0}EtaPrimePPG'.format(name)
        d2pietaprimeppg_name = '{0}Calib'.format(name)
        
        self.selEtaPrimePPG = makeResonance(
            etaprimeppg_name,
            config,
            inputSel=[StdAllNoPIDsPions,StdLooseAllPhotons],
            decDescriptors=self.EtaPrimePPG
            )
        
        self.selD2PiEtaPrimePPG = makeD(
            d2pietaprimeppg_name,
            config,
            inputSel=[self.selEtaPrimePPG, StdAllNoPIDsPions],
            decDescriptors=self.D2PiEtaPrime,
            useBachelorPID=False,
            )
        
        self.line_D2PiEtaPrimePPG = make_line(
            self,
            name='{0}Line'.format(d2pietaprimeppg_name),
            prescale=config['PrescaleDp2PipEtaPrimePPG'],
            postscale=config['PostscaleDp2PipEtaPrimePPG'],
            selection=self.selD2PiEtaPrimePPG,
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter'],
            RequiredRawEvents = ["Calo","Trigger","Velo","Tracker"],
            )
        
def make_line(self, name, selection, prescale, postscale, **kwargs):
    """Create the stripping line defined by the selection.
    
    Keyword arguments:
    name -- Base name for the Line
    selection -- Selection instance
    prescale -- Fraction of candidates to randomly drop before stripping
    postscale -- Fraction of candidates to randomly drop after stripping
    **kwargs -- Keyword arguments passed to StrippingLine constructor
    """
    # Only create the line with positive pre- and postscales
    # You can disable each line by setting either to a negative value
    if prescale > 0 and postscale > 0:
        line = StrippingLine(
            name,
            selection=selection,
            prescale=prescale,
            postscale=postscale,
            **kwargs
            )
        self.registerLine(line)
        return line
    else:
        return False

def makeResonance( name, config, inputSel, decDescriptors):  

    daugCuts = (
        '(PT > {0[Daug_PT_MIN]})'
        '& (P > {0[Daug_P_MIN]}) '
        '& (MIPCHI2DV(PRIMARY) > {0[Daug_IPCHI2_MIN]})'
        '& (TRCHI2DOF < {0[Daug_TRCHI2DOF_MAX]}) '
        '& (TRGHOSTPROB < {0[Daug_TRGHOSTPROB_MAX]}) '
        ).format(config)
    
    pidFiducialCuts = (
        '(in_range({0[Daug_P_MIN]}, P, {0[Daug_P_MAX]}))'
        '& (in_range({0[Daug_ETA_MIN]}, ETA, {0[Daug_ETA_MAX]}))'
        ).format(config)
    
    pionPIDCuts = (
        pidFiducialCuts +
        '& (PIDK-PIDpi < {0[Pi_PIDK_MAX]})'
        ).format(config)
    
    combCuts = "in_range( {0[Res_Mass_MIN]},AM,{0[Res_Mass_MAX]} )".format(config)
    
    gammaCuts = (
        "(PT > {0[Neut_PT_MIN]})"
        ).format(config)
    
    resCuts = "ALL"
    
    comb12Cuts = (
        "( in_range( {0[Dipion_Mass_MIN]},AM,{0[Dipion_Mass_MAX]}) )"
        "& ( ACUTDOCACHI2( {0[Dipion_DOCACHI2_MAX]} , '') ) "
        ).format(config)
    
    piCuts = ''
    if (config['Pi_PIDK_MAX'] < 10):
        piCuts = ('{0} & {1}'.format(daugCuts, pionPIDCuts))
    else:
        piCuts = ('{0}'.format(daugCuts))
        
    _combiner = DaVinci__N3BodyDecays(
        Combination12Cut = comb12Cuts,
        DecayDescriptors=decDescriptors,
        DaughtersCuts={
        'pi+'  : '{0}'.format(piCuts),
        'gamma': '{0}'.format(gammaCuts),
        },
        CombinationCut=combCuts,
        MotherCut=resCuts
        )
    
    return Selection(name, Algorithm=_combiner, RequiredSelections=inputSel)


def makeD(name, config, inputSel, decDescriptors, useBachelorPID):        
    
    
    pidFiducialCuts = ( 
        '(in_range({0[Daug_P_MIN]}, P, {0[Daug_P_MAX]}))'
        '& (in_range({0[Daug_ETA_MIN]}, ETA, {0[Daug_ETA_MAX]}))'
        ).format(config)
    
    bachelorPIDCuts = (  
        pidFiducialCuts +
        '& (PIDK-PIDpi > {0[Bachelor_PIDK_MIN]})'
        ).format(config)
    
    daugCuts = (
        '(PT > {0[Bachelor_PT_MIN]})'
        '& (P > {0[Bachelor_P_MIN]}) '
        '& (MIPCHI2DV(PRIMARY) > {0[Bachelor_IPCHI2_MIN]})'
        '& (TRCHI2DOF < {0[Daug_TRCHI2DOF_MAX]}) ' # common TRCHI2DOF and TRGHOSTPROB for all pions
        '& (TRGHOSTPROB < {0[Daug_TRGHOSTPROB_MAX]}) '
        ).format(config)
    
    bachelorCuts = ''
    if (useBachelorPID):
        bachelorCuts = ('{0} & {1}'.format(daugCuts, bachelorPIDCuts))
    else:
        bachelorCuts = ('{0}'.format(daugCuts))
        
    combCuts = (
        "(APT > {0[D_PT_Min]})"
        "& ( in_range( {0[D_Mass_MIN]},AM,{0[D_Mass_MAX]}) )"
        ).format(config)
    
    dCuts = (
        '(VFASPF(VCHI2PDOF) < {0[D_VCHI2PDOF_MAX]})'
        '& (BPVDIRA> {0[D_BPVDIRA_MIN]})'
        '& (BPVIP()<{0[D_IP_MAX]})'
        '& (BPVIPCHI2()<{0[D_IPCHI2_MAX]})'
        '& (BPVLTIME() > {0[D_BPVLTIME_MIN]})'
        '& (DTF_CHI2NDOF(True) < {0[DTF_CHI2NDOF_MAX]})' 
        ).format(config)
    
    _D = CombineParticles(
        #name='Combine{0}'.format(name),
        DecayDescriptors=decDescriptors,
        DaughtersCuts={
        'pi+': '{0}'.format(bachelorCuts),
        'K+': '{0}'.format(bachelorCuts)
        },
        CombinationCut=combCuts,
        MotherCut=dCuts
        )
    
    return Selection(name, Algorithm=_D, RequiredSelections=inputSel)


