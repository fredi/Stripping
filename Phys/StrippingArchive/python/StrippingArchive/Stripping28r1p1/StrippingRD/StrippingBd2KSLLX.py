###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__  = 'SB'
__date__    = '08/10/2017'
__version__ = '$Revision: 1 $'

__all__ = ( 'Bd2KSLLXConf', 'default_config' ) 

"""
  Bd --> ll KS X selections
"""

default_config = {
    'NAME'                       : 'Bd2KSLLX',
    'BUILDERTYPE'                : 'Bd2KSLLXConf',
    'CONFIG'                     :
        {                       
        'BFlightCHI2'            : 10   
        , 'BdIPCHI2'             : 200    
        , 'BVertexCHI2'          : 12    
        , 'DiLeptonPT'           : 1000.0    
        , 'DiLeptonFDCHI2'       : 10   
        , 'DiLeptonIPCHI2'       : 3.5   
        , 'DiLeptonVCHI2'       : 5   
        , 'LeptonIPCHI2'         : 4.0  
        , 'LeptonPT'             : 300  
        , 'KaonPT'               : 300  
        , 'KaonIPCHI2'           : 3
        , 'DiHadronMass'         : 2600
        , 'KSIPCHI2'             : 4.0
        , 'KSVCHI'               : 5
        , 'UpperMass'            : 5500
        , 'CorrMmin_ee'          : 3500
        , 'CorrMmin_mm'          : 3500
        , 'CorrMmax_ee'          : 7000
        , 'CorrMmax_mm'          : 7000
        , 'PIDe'                 : 0
        , 'Trk_Chi2'             : 3.5
        , 'Trk_GhostProb'        : 0.4
        , 'BdTransvMom'          : 1500.0
        },
    'WGs'     : [ 'RD' ],
    'STREAMS' : [ 'Leptonic' ]
    }
    

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles, DaVinci__N3BodyDecays
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection, AutomaticData
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

class Bd2KSLLXConf(LineBuilder) :
    # now just define keys. Default values are fixed later
    __configuration_keys__ = default_config['CONFIG'].keys()
    
    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)

        self._name = name

        mmXLine_name   = name + "_mm"
        eeXLine_name   = name + "_ee"

        from StandardParticles import StdLoosePions as Pions
        from StandardParticles import StdLooseKaons as Kaons
        from StandardParticles import StdLooseKstar2Kpi as Kstars
        from StandardParticles import StdLoosePhi2KK as Phis
        from StandardParticles import StdLooseKsLL as KshortsLL 
        from StandardParticles import StdLooseKsDD as KshortsDD    
        from StandardParticles import StdLooseLambdaLL as LambdasLL
        from StandardParticles import StdLooseLambdaDD as LambdasDD  
        from StandardParticles import StdLooseLambdastar2pK as Lambdastars  


        SelKshortsLL = self._filterHadron( name   = "KshortsLLFor" + self._name,
                                           sel    =  KshortsLL, 
                                           params = config )
        SelKshortsDD = self._filterHadron( name   = "KshortsDDFor" + self._name,
                                           sel    =  KshortsDD,
                                           params = config )
 
        from StandardParticles import StdDiElectronFromTracks as DiElectronsFromTracks
        from StandardParticles import StdLooseDiElectron as DiElectrons
        from StandardParticles import StdLooseDiMuon as DiMuons 

        ElecID = "(PIDe > %(PIDe)s)" % config
        MuonID = "(HASMUON)&(ISMUON)"

        DiElectronID = "(2 == NINTREE((ABSID==11)&(PIDe > %(PIDe)s)))" % config
        DiMuonID     = "(2 == NINTREE((ABSID==13)&(HASMUON)&(ISMUON)))"
                
        
        SelDiElectron = self._filterDiLepton( "SelDiElectronFor" + self._name, 
                                              dilepton = DiElectrons,
                                              params   = config,
                                              idcut    = DiElectronID )
        
        SelDiMuon = self._filterDiLepton( "SelDiMuonsFor" + self._name, 
                                          dilepton = DiMuons,
                                          params   = config,
                                          idcut    = DiMuonID )
        
        SelB2eeX = self._makeB2LLX(eeXLine_name,
                                   dilepton = SelDiElectron,
                                   hadrons  = [ SelKshortsLL, SelKshortsDD ],
                                   params   = config,
                                   masscut  = "in_range(%(CorrMmin_ee)s *MeV, BPVCORRM,  %(CorrMmax_ee)s *MeV)" % config )

        SelB2mmX = self._makeB2LLX(mmXLine_name,
                                   dilepton = SelDiMuon,
                                   hadrons  = [ SelKshortsLL, SelKshortsDD ],
                                   params   = config,
                                   masscut  = "in_range(%(CorrMmin_mm)s *MeV, BPVCORRM,  %(CorrMmax_mm)s *MeV)" % config )

        SPDFilter = {
            'Code'      : " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < 600 )" ,
            'Preambulo' : [ "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb" ]
            }
        
        self.B2eeXLine = StrippingLine(eeXLine_name + "Line",
                                       prescale          = 1,
                                       postscale         = 1,
                                       selection         = SelB2eeX,
                                       FILTER            = SPDFilter, 
                                       RequiredRawEvents = ['Trigger', 'Muon', 'Calo', 'Rich', 'Velo', 'Tracker'],
                                       MDSTFlag          = False )

        self.B2mmXLine = StrippingLine(mmXLine_name + "Line",
                                       prescale          = 1,
                                       postscale         = 1,
                                       selection         = SelB2mmX,
                                       FILTER            = SPDFilter, 
                                       RequiredRawEvents = ['Trigger', 'Muon', 'Calo', 'Rich', 'Velo', 'Tracker'],
                                       MDSTFlag          = False )

        self.registerLine( self.B2eeXLine )
        self.registerLine( self.B2mmXLine )

#####################################################
    def _filterHadron( self, name, sel, params ):
        """
        Filter for all hadronic final states
        """

        # requires all basic particles to have IPCHI2 > KaonIPCHI2
        # and hadron PT > KaonPT
        # need to add the ID here
        _Code = "(PT > %(KaonPT)s *MeV) & " \
                "(M < %(DiHadronMass)s*MeV) & " \
                "(VFASPF(VCHI2) < %(KSVCHI)s) & "\
                "(BPVIPCHI2() > %(KSIPCHI2)s) & "\
                "((ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s)) | " \
                "(NDAUGHTERS == NINTREE(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))))" % params

        _Filter = FilterDesktop( Code = _Code )

        return Selection( name, Algorithm = _Filter, RequiredSelections = [ sel ] )
#####################################################
    def _filterDiLepton( self, name, dilepton, params, idcut = None ) :
        """
        Handy interface for dilepton filter
        """

        _Code = "(ID=='J/psi(1S)') & "\
                "(PT > %(DiLeptonPT)s *MeV) & "\
                "(MM < %(UpperMass)s *MeV) & "\
                "(MINTREE(ABSID<14,PT) > %(LeptonPT)s *MeV) & "\
                "(MINTREE(ABSID<14,MIPCHI2DV(PRIMARY)) > %(LeptonIPCHI2)s) & "\
                "(VFASPF(VCHI2) < %(DiLeptonVCHI2)s) & (BPVVDCHI2 > %(DiLeptonFDCHI2)s) & "\
                "(MIPCHI2DV(PRIMARY) > %(DiLeptonIPCHI2)s)" % params

        # add additional cut on PID if requested
        if idcut : _Code += ( " & " + idcut ) 

        _Filter = FilterDesktop( Code = _Code )
    
        return Selection(name, Algorithm = _Filter, RequiredSelections = [ dilepton ] )
#####################################################
    def _makeB2LLX( self, name, dilepton, hadrons, params, masscut):
        """
        CombineParticles / Selection for the B 
        """

        _Decays = [ "B0 -> J/psi(1S) KS0"]
        
        _Cut = "((VFASPF(VCHI2/VDOF) < %(BVertexCHI2)s) & (BPVIPCHI2()<%(BdIPCHI2)s)"\
               "& (PT>%(BdTransvMom)s) & (BPVVDCHI2 > %(BFlightCHI2)s)" % params
        _Cut += "& (%s))" % masscut
        
        _Combine = CombineParticles( DecayDescriptors = _Decays,
                                     MotherCut        = _Cut )
        
        _Merge = MergedSelection( "Merge" + name, RequiredSelections = hadrons )

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ dilepton, _Merge ] ) 
