###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Set of lines for beta and beta_s
B0 -> Jpsi pi0
Bs -> Jpsi eta/etap with different final states
B+ -> Jpsi K*[Kpi0] for calibration
'''

__author__ = ['Max Chefdeville']
__date__ = '22/01/2018'

__all__ = ('B2JpsiX0Conf','default_config')

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from CommonParticles.Utils import updateDoD
from StandardParticles import StdLoosePions
from StandardParticles import StdLooseKaons
from StandardParticles import StdLooseProtons
from StandardParticles import StdAllLooseKaons
from StandardParticles import StdAllLooseProtons
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import MeV

default_config = {
    'NAME'              : 'B2JpsiX0',
    'WGs'               : ['B2CC'],
    'BUILDERTYPE'       : 'B2JpsiX0Conf',
    'CONFIG'            : {
    'JpsiMassWindow'         : 80,
    'LTimeCut'               : 0.2,

    'DIRACut'                : 0.9995,
    'IPCut'                  : 0.2,
    'IPCHI2Cut'              : 20,
    'VCHI2PDOFCut'           : 10,

    'PrescaleB02JpsiPi0R'    : 1,
    'PrescaleB02JpsiPi0M'    : 1,

    'PrescaleBu2JpsiKstR'    : 1,
    'PrescaleBu2JpsiKstM'    : 1,

    'PrescaleBu2JpsiK'       : 0.1,

    'PrescaleBs2JpsiEtaR'            : 1,
    'PrescaleBs2JpsiEta2PiPiGamma'   : 1,
    'PrescaleBs2JpsiEta2PiPiPi0R'    : 1,
    'PrescaleBs2JpsiEta2PiPiPi0M'    : 1,

    'PrescaleBs2JpsiEtap2RhoGamma'   : 1,
    'PrescaleBs2JpsiEtap2EtaPiPi'    : 1,
    
    'B02JpsiPi0RMVACut'      : "-0.56",
    'B02JpsiPi0RXmlFile'     :  "$TMVAWEIGHTSROOT/data/B2JpsiX0/B02JpsiPi0R_fisher_run2_v1r1.xml",

    'Bu2JpsiKstRMVACut'      : "-1.19",
    'Bu2JpsiKstRXmlFile'     :  "$TMVAWEIGHTSROOT/data/B2JpsiX0/Bu2JpsiKstR_fisher_run2_v1r1.xml",

    'Bs2JpsiEtaRMVACut'      : "-0.73",
    'Bs2JpsiEtaRXmlFile'     :  "$TMVAWEIGHTSROOT/data/B2JpsiX0/Bs2JpsiEtaR_fisher_run2_v1r1.xml",
    },
    'STREAMS'           : ['Dimuon']
    }

 ### Lines stored in this file:

class B2JpsiX0Conf(LineBuilder) :

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :

        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config
        
        self.WideJpsiList = DataOnDemand(Location = "Phys/StdMassConstrainedJpsi2MuMu/Particles")
        self.DetachedJpsiList = self.createSubSel( OutputList = 'DetachedJpsi' + self.name,
                                                   InputList  = self.WideJpsiList,
                                                   Cuts       = "(PFUNA(ADAMASS('J/psi(1S)')) < %(JpsiMassWindow)s * MeV) & (BPVLTIME() > %(LTimeCut)s *ps)" %self.config)
        
        self.GammaList = DataOnDemand(Location = "Phys/StdLooseAllPhotons/Particles")
        self.Pi0RList = DataOnDemand(Location = "Phys/StdLooseResolvedPi0/Particles")
        self.Pi0MList = DataOnDemand(Location = "Phys/StdLooseMergedPi0/Particles")
        self.EtaRList = DataOnDemand(Location = "Phys/StdLooseEta2gg/Particles")
        self.PionList = DataOnDemand(Location = "Phys/StdLoosePions/Particles")
        self.KaonList = DataOnDemand(Location = "Phys/StdLooseKaons/Particles")

        #-------------

        B02JpsiPi0R = self.createCombinationSel( OutputList      = "B02JpsiPi0R" + self.name,
                                                 DecayDescriptor = "B0 -> J/psi(1S) pi0",
                                                 DaughterLists   = [ self.DetachedJpsiList, self.Pi0RList],
                                                 DaughterCuts    = {"pi0" : "(CHILD(CL,1) > 0.05) & (CHILD(CL,2) > 0.05) & (PT>1500*MeV)"},
                                                 PreVertexCuts   = "in_range(4500,AM,6500)",
                                                 PostVertexCuts  = "(BPVDIRA > %(DIRACut)s) & (BPVIP() < %(IPCut)s) & (BPVIPCHI2() < %(IPCHI2Cut)s) & (VFASPF(VCHI2PDOF) < %(VCHI2PDOFCut)s )" % self.config )

        self.B02JpsiPi0RVars = {"log(B0_pi0_PT)"                  : "log(CHILD(PT,2))",
                                "log(B0_Jpsi_PT)"                 : "log(CHILD(PT,1))",
                                "log(1-fabs(B0_Jpsi_DIRA_OWNPV))" : "log(1-abs(CHILD(BPVDIRA,1)))",
                                "log(B0_Jpsi_IP_OWNPV)"           : "log(CHILD(BPVIP(),1))",
                                "log(1-fabs(B0_DIRA_OWNPV))"      : "log(1-abs(BPVDIRA))",
                                "log(B0_IP_OWNPV)"                : "log(BPVIP())",
                                "log(B0_ENDVERTEX_CHI2)"          : "log(VFASPF(VCHI2))"}

        self.MvaB02JpsiPi0R = self.applyMVA(self.name + "MvaB02JpsiPi0R",
                                            SelB = B02JpsiPi0R,
                                            MVAVars = self.B02JpsiPi0RVars,
                                            MVACutValue = config['B02JpsiPi0RMVACut'],
                                            MVAxmlFile  = config['B02JpsiPi0RXmlFile'])
        
        B02JpsiPi0RLine  = StrippingLine( self.name + "B02JpsiPi0RLine",
                                          prescale  = config['PrescaleB02JpsiPi0R'],
                                          algos = [ self.MvaB02JpsiPi0R ],
                                          RequiredRawEvents = ["Calo"])
        
        self.registerLine(B02JpsiPi0RLine)

        #-------------

        B02JpsiPi0M = self.createCombinationSel( OutputList      = "B02JpsiPi0M" + self.name,
                                                 DecayDescriptor = "B0 -> J/psi(1S) pi0",
                                                 DaughterLists   = [ self.DetachedJpsiList, self.Pi0MList],
                                                 PreVertexCuts   = "in_range(4500,AM,6500)",
                                                 PostVertexCuts  = "(BPVDIRA > %(DIRACut)s) & (BPVIP() < %(IPCut)s) & (BPVIPCHI2() < %(IPCHI2Cut)s) & (VFASPF(VCHI2PDOF) < %(VCHI2PDOFCut)s )" % self.config )

        B02JpsiPi0MLine  = StrippingLine( self.name + "B02JpsiPi0MLine",
                                          prescale  = config['PrescaleB02JpsiPi0M'],
                                          algos = [ B02JpsiPi0M ],
                                          RequiredRawEvents = ["Calo"])                                          

        self.registerLine(B02JpsiPi0MLine)

        #-------------
        
        self.Kst2KPi0R = self.createCombinationSel( OutputList      = "Kst2KPi0R" + self.name,
                                                    DecayDescriptor = "[K*(892)+ -> K+ pi0]cc",
                                                    DaughterLists   = [ self.KaonList, self.Pi0RList],
                                                    DaughterCuts    = {"pi0" : "(CHILD(CL,1)>0.05) & (CHILD(CL,2)>0.05) & (PT>800*MeV)" , "K+" : "(PROBNNk>0.1)" },
                                                    PreVertexCuts   = "(ADAMASS('K*(892)+')< 150*MeV)",
                                                    PostVertexCuts  = "ALL" )

        Bu2JpsiKstR = self.createCombinationSel( OutputList      = "Bu2JpsiKstR" + self.name,
                                                 DecayDescriptor = "[B+ -> J/psi(1S) K*(892)+]cc",
                                                 DaughterLists   = [ self.DetachedJpsiList, self.Kst2KPi0R],
                                                 PreVertexCuts   = "in_range(4500,AM,6500)",
                                                 PostVertexCuts  = "(BPVDIRA > %(DIRACut)s) & (BPVIP() < %(IPCut)s) & (BPVIPCHI2() < %(IPCHI2Cut)s) & (VFASPF(VCHI2PDOF) < %(VCHI2PDOFCut)s )" % self.config )

        self.Bu2JpsiKstRVars = {"log(Bplus_pi0_PT)"                  : "log(CHILD(CHILD(PT,2),2))",
                                "log(Bplus_Kst_PT)"                  : "log(CHILD(PT,2))",
                                "log(Bplus_Jpsi_PT)"                 : "log(CHILD(PT,1))",
                                "log(1-fabs(Bplus_Jpsi_DIRA_OWNPV))" : "log(1-abs(CHILD(BPVDIRA,1)))",
                                "log(Bplus_Jpsi_IP_OWNPV)"           : "log(CHILD(BPVIP(),1))",
                                "log(Bplus_Jpsi_ENDVERTEX_CHI2)"     : "log(CHILD(VFASPF(VCHI2),1))",
                                "log(1-fabs(Bplus_DIRA_OWNPV))"      : "log(1-abs(BPVDIRA))",
                                "log(Bplus_IP_OWNPV)"                : "log(BPVIP())",
                                "log(Bplus_ENDVERTEX_CHI2)"          : "log(VFASPF(VCHI2))"}

        self.MvaBu2JpsiKstR = self.applyMVA(self.name + "MvaBu2JpsiKstR",
                                            SelB = Bu2JpsiKstR,
                                            MVAVars = self.Bu2JpsiKstRVars,
                                            MVACutValue = config['Bu2JpsiKstRMVACut'],
                                            MVAxmlFile  = config['Bu2JpsiKstRXmlFile'])
        
        Bu2JpsiKstRLine  = StrippingLine(self.name + "Bu2JpsiKstRLine",
                                         prescale  = config['PrescaleBu2JpsiKstR'],
                                         algos = [ self.MvaBu2JpsiKstR ],
                                         RequiredRawEvents = ["Calo"])
        
        self.registerLine(Bu2JpsiKstRLine)

        #-------------

        self.Kst2KPi0M = self.createCombinationSel( OutputList      = "Kst2KPi0M" + self.name,
                                                    DecayDescriptor = "[K*(892)+ -> K+ pi0]cc",
                                                    DaughterLists   = [ self.KaonList, self.Pi0MList],
                                                    DaughterCuts    = {"K+" : "(PROBNNk>0.1)" },
                                                    PreVertexCuts   = "(ADAMASS('K*(892)+')< 150*MeV)",
                                                    PostVertexCuts  = "ALL" )

        Bu2JpsiKstM = self.createCombinationSel( OutputList      = "Bu2JpsiKstM" + self.name,
                                                 DecayDescriptor = "[B+ -> J/psi(1S) K*(892)+]cc",
                                                 DaughterLists   = [ self.DetachedJpsiList, self.Kst2KPi0M],
                                                 PreVertexCuts   = "in_range(4500,AM,6500)",
                                                 PostVertexCuts  = "(BPVDIRA > %(DIRACut)s) & (BPVIP() < %(IPCut)s) & (BPVIPCHI2() < %(IPCHI2Cut)s) & (VFASPF(VCHI2PDOF) < %(VCHI2PDOFCut)s )" % self.config )

        Bu2JpsiKstMLine  = StrippingLine( self.name + "Bu2JpsiKstMLine",
                                          prescale  = config['PrescaleBu2JpsiKstM'],
                                          algos = [ Bu2JpsiKstM ],
                                          RequiredRawEvents = ["Calo"])

        self.registerLine(Bu2JpsiKstMLine)

        #-------------

        Bu2JpsiK = self.createCombinationSel( OutputList      = "Bu2JpsiK" + self.name,
                                              DecayDescriptor = "[B+ -> J/psi(1S) K+]cc",
                                              DaughterLists   = [ self.DetachedJpsiList, self.KaonList],
                                              PreVertexCuts   = "in_range(5100,AM,5500)",
                                              PostVertexCuts  = "(BPVDIRA > %(DIRACut)s) & (BPVIP() < %(IPCut)s) & (BPVIPCHI2() < %(IPCHI2Cut)s) & (VFASPF(VCHI2PDOF) < %(VCHI2PDOFCut)s )" % self.config )

        Bu2JpsiKLine  = StrippingLine( self.name + "Bu2JpsiKLine",
                                       prescale  = config['PrescaleBu2JpsiK'],
                                       algos = [ Bu2JpsiK ])

        self.registerLine(Bu2JpsiKLine)

        #-------------

        Bs2JpsiEtaR = self.createCombinationSel( OutputList      = "Bs2JpsiEtaR" + self.name,
                                                 DecayDescriptor = "B_s0 -> J/psi(1S) eta",
                                                 DaughterLists   = [ self.DetachedJpsiList, self.EtaRList],
                                                 DaughterCuts    = {"eta" : "(CHILD(CL,1) > 0.05) & (CHILD(CL,2) > 0.05) & (PT>1500*MeV) & in_range(475,M,625)"},
                                                 PreVertexCuts   = "in_range(4500,AM,6500)",
                                                 PostVertexCuts  = "(BPVDIRA > %(DIRACut)s) & (BPVIP() < %(IPCut)s) & (BPVIPCHI2() < %(IPCHI2Cut)s) & (VFASPF(VCHI2PDOF) < %(VCHI2PDOFCut)s )" % self.config )

        self.Bs2JpsiEtaRVars = {"log(Bs_eta_PT)"                  : "log(CHILD(PT,2))",
                                "log(Bs_Jpsi_PT)"                 : "log(CHILD(PT,1))",
                                "log(1-fabs(Bs_Jpsi_DIRA_OWNPV))" : "log(1-abs(CHILD(BPVDIRA,1)))",
                                "log(Bs_Jpsi_IP_OWNPV)"           : "log(CHILD(BPVIP(),1))",
                                "log(1-fabs(Bs_DIRA_OWNPV))"      : "log(1-abs(BPVDIRA))",
                                "log(Bs_IP_OWNPV)"                : "log(BPVIP())",
                                "log(Bs_ENDVERTEX_CHI2)"          : "log(VFASPF(VCHI2))"}
        
        self.MvaBs2JpsiEtaR = self.applyMVA(self.name + "MvaBs2JpsiEtaR",
                                            SelB = Bs2JpsiEtaR,
                                            MVAVars = self.Bs2JpsiEtaRVars,
                                            MVACutValue = config['Bs2JpsiEtaRMVACut'],
                                            MVAxmlFile  = config['Bs2JpsiEtaRXmlFile'])
        
        Bs2JpsiEtaRLine  = StrippingLine( self.name + "Bs2JpsiEtaRLine",
                                          prescale  = config['PrescaleBs2JpsiEtaR'],
                                          algos = [ self.MvaBs2JpsiEtaR ],
                                          RequiredRawEvents = ["Calo"])

        self.registerLine(Bs2JpsiEtaRLine)

        #-------------

        self.Eta2PiPiPi0R = self.createCombinationSel( OutputList      = "Eta2PiPiPi0R" + self.name,
                                                       DecayDescriptor = "eta -> pi+ pi- pi0",
                                                       DaughterLists   = [ self.PionList, self.Pi0RList],
                                                       DaughterCuts    = {"pi0" : "(CHILD(CL,1)>0.05) & (CHILD(CL,2)>0.05)" },
                                                       PreVertexCuts   = "(ADAMASS('eta')<100*MeV) & (APT>1500*MeV)",
                                                       PostVertexCuts  = "(BPVVDZ>0) & (VFASPF(VCHI2)<9) & (BPVDIRA>0.95) & (BPVVDCHI2>25)")

        Bs2JpsiEta2PiPiPi0R = self.createCombinationSel( OutputList      = "Bs2JpsiEta2PiPiPi0R" + self.name,
                                                         DecayDescriptor = "B_s0 -> J/psi(1S) eta",
                                                         DaughterLists   = [ self.DetachedJpsiList, self.Eta2PiPiPi0R],
                                                         PreVertexCuts   = "in_range(4500,AM,6500)",
                                                         PostVertexCuts  = "(BPVDIRA > %(DIRACut)s) & (BPVIP() < %(IPCut)s) & (BPVIPCHI2() < %(IPCHI2Cut)s) & (VFASPF(VCHI2PDOF) < %(VCHI2PDOFCut)s )" % self.config )

        Bs2JpsiEta2PiPiPi0RLine  = StrippingLine( self.name + "Bs2JpsiEta2PiPiPi0RLine",
                                                  prescale  = config['PrescaleBs2JpsiEta2PiPiPi0R'],
                                                  algos = [ Bs2JpsiEta2PiPiPi0R ],
                                                  RequiredRawEvents = ["Calo"])

        self.registerLine(Bs2JpsiEta2PiPiPi0RLine)

        #-------------

        self.Eta2PiPiPi0M = self.createCombinationSel( OutputList      = "Eta2PiPiPi0M" + self.name,
                                                       DecayDescriptor = "eta -> pi+ pi- pi0",
                                                       DaughterLists   = [ self.PionList, self.Pi0MList],
                                                       PreVertexCuts   = "(ADAMASS('eta')<100*MeV) & (APT>1500*MeV)",
                                                       PostVertexCuts  = "(BPVVDZ>0) & (VFASPF(VCHI2)<9) & (BPVDIRA>0.95) & (BPVVDCHI2>25)" )

        Bs2JpsiEta2PiPiPi0M = self.createCombinationSel( OutputList      = "Bs2JpsiEta2PiPiPi0M" + self.name,
                                                         DecayDescriptor = "B_s0 -> J/psi(1S) eta",
                                                         DaughterLists   = [ self.DetachedJpsiList, self.Eta2PiPiPi0M],
                                                         PreVertexCuts   = "in_range(4500,AM,6500)",
                                                         PostVertexCuts  = "(BPVDIRA > %(DIRACut)s) & (BPVIP() < %(IPCut)s) & (BPVIPCHI2() < %(IPCHI2Cut)s) & (VFASPF(VCHI2PDOF) < %(VCHI2PDOFCut)s )" % self.config )

        Bs2JpsiEta2PiPiPi0MLine  = StrippingLine( self.name + "Bs2JpsiEta2PiPiPi0MLine",
                                                  prescale  = config['PrescaleBs2JpsiEta2PiPiPi0M'],
                                                  algos = [ Bs2JpsiEta2PiPiPi0M ],
                                                  RequiredRawEvents = ["Calo"])

        self.registerLine(Bs2JpsiEta2PiPiPi0MLine)

        #-------------

        self.Eta2PiPiGamma = self.createCombinationSel( OutputList      = "Eta2PiPiGamma" + self.name,
                                                        DecayDescriptor = "eta -> pi+ pi- gamma",
                                                        DaughterLists   = [ self.PionList, self.GammaList],
                                                        DaughterCuts    = {"gamma" : "(CL>0.05)" },
                                                        PreVertexCuts   = "(ADAMASS('eta')<100*MeV) & (APT>1500*MeV)",
                                                        PostVertexCuts  = "(BPVVDZ>0) & (VFASPF(VCHI2)<9) & (BPVDIRA>0.95) & (BPVVDCHI2>25)" )

        Bs2JpsiEta2PiPiGamma = self.createCombinationSel( OutputList      = "Bs2JpsiEta2PiPiGamma" + self.name,
                                                          DecayDescriptor = "B_s0 -> J/psi(1S) eta",
                                                          DaughterLists   = [ self.DetachedJpsiList, self.Eta2PiPiGamma],
                                                          PreVertexCuts   = "in_range(4500,AM,6500)",
                                                          PostVertexCuts  = "(BPVDIRA > %(DIRACut)s) & (BPVIP() < %(IPCut)s) & (BPVIPCHI2() < %(IPCHI2Cut)s) & (VFASPF(VCHI2PDOF) < %(VCHI2PDOFCut)s )" % self.config )

        Bs2JpsiEta2PiPiGammaLine  = StrippingLine( self.name + "Bs2JpsiEta2PiPiGammaLine",
                                                   prescale  = config['PrescaleBs2JpsiEta2PiPiGamma'],
                                                   algos = [ Bs2JpsiEta2PiPiGamma ],
                                                   RequiredRawEvents = ["Calo"])

        self.registerLine(Bs2JpsiEta2PiPiGammaLine)

        #-------------

        self.Rho2PiPi = self.createCombinationSel( OutputList      = "Rho2PiPi" + self.name,
                                                   DecayDescriptor = "rho(770)0 -> pi+ pi-",
                                                   DaughterLists   = [ self.PionList ],
                                                   PreVertexCuts   = "(AM>600*MeV) & (AM<900*MeV) & (ADOCACHI2CUT(15, ''))",
                                                   PostVertexCuts  = "(BPVVDZ>0) & (VFASPF(VCHI2)<9) & (BPVDIRA>0.95) & (BPVVDCHI2>25)")

        self.Etap2RhoGamma = self.createCombinationSel( OutputList      = "Etap2RhoGamma" + self.name,
                                                        DecayDescriptor = "eta_prime -> rho(770)0 gamma",
                                                        DaughterLists   = [ self.Rho2PiPi, self.GammaList],
                                                        DaughterCuts    = {"gamma" : "(CL>0.05)" },
                                                        PreVertexCuts   = "(ADAMASS('eta_prime')<100*MeV) & (APT>1500*MeV)",
                                                        PostVertexCuts  = "ALL" )

        Bs2JpsiEtap2RhoGamma = self.createCombinationSel( OutputList      = "Bs2JpsiEtap2RhoGamma" + self.name,
                                                          DecayDescriptor = "B_s0 -> J/psi(1S) eta_prime",
                                                          DaughterLists   = [ self.DetachedJpsiList, self.Etap2RhoGamma],
                                                          PreVertexCuts   = "in_range(4500,AM,6500)",
                                                          PostVertexCuts  = "(BPVDIRA > %(DIRACut)s) & (BPVIP() < %(IPCut)s) & (BPVIPCHI2() < %(IPCHI2Cut)s) & (VFASPF(VCHI2PDOF) < %(VCHI2PDOFCut)s )" % self.config )

        Bs2JpsiEtap2RhoGammaLine  = StrippingLine( self.name + "Bs2JpsiEtap2RhoGammaLine",
                                                   prescale  = config['PrescaleBs2JpsiEtap2RhoGamma'],
                                                   algos = [ Bs2JpsiEtap2RhoGamma ],
                                                   RequiredRawEvents = ["Calo"])

        self.registerLine(Bs2JpsiEtap2RhoGammaLine)

        #-------------

        self.Etap2EtaPiPi = self.createCombinationSel( OutputList      = "Etap2EtaPiPi" + self.name,
                                                       DecayDescriptor = "eta_prime -> pi+ pi- eta",
                                                       DaughterLists   = [ self.PionList, self.EtaRList],
                                                       DaughterCuts    = { "eta" : "(CHILD(CL,1)>0.05) & (CHILD(CL,2)>0.05) & (PT>1000*MeV)" },
                                                       PreVertexCuts   = "(ADAMASS('eta_prime')<150*MeV) & (APT>1500*MeV)",
                                                       PostVertexCuts  = "(BPVVDZ>0) & (VFASPF(VCHI2)<9) & (BPVDIRA>0.95) & (BPVVDCHI2>25)" )
        
        Bs2JpsiEtap2EtaPiPi = self.createCombinationSel( OutputList      = "Bs2JpsiEtap2EtaPiPi" + self.name,
                                                         DecayDescriptor = "B_s0 -> J/psi(1S) eta_prime",
                                                         DaughterLists   = [ self.DetachedJpsiList, self.Etap2EtaPiPi],
                                                         PreVertexCuts   = "in_range(4500,AM,6500)",
                                                         PostVertexCuts  = "(BPVDIRA > %(DIRACut)s) & (BPVIP() < %(IPCut)s) & (BPVIPCHI2() < %(IPCHI2Cut)s) & (VFASPF(VCHI2PDOF) < %(VCHI2PDOFCut)s )" % self.config )

        Bs2JpsiEtap2EtaPiPiLine  = StrippingLine( self.name + "Bs2JpsiEtap2EtaPiPiLine",
                                                  prescale  = config['PrescaleBs2JpsiEtap2EtaPiPi'],
                                                  algos = [ Bs2JpsiEtap2EtaPiPi ],
                                                  RequiredRawEvents = ["Calo"])

        self.registerLine(Bs2JpsiEtap2EtaPiPiLine)

        #-------------

    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                          Algorithm = filter,
                          RequiredSelections = [ InputList ] )

    def createCombinationSel( self, OutputList,
                              DecayDescriptor,
                              DaughterLists,
                              DaughterCuts = {} ,
                              PreVertexCuts = "ALL",
                              PostVertexCuts = "ALL",
                              ReFitPVs = True ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                                     DaughtersCuts = DaughterCuts,
                                     MotherCut = PostVertexCuts,
                                     CombinationCut = PreVertexCuts,
                                     ReFitPVs = ReFitPVs)
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)
        
    def applyMVA( self, name, 
                  SelB,
                  MVAVars,
                  MVAxmlFile,
                  MVACutValue
                  ):
        from MVADictHelpers import addTMVAclassifierValue
        from Configurables import FilterDesktop as MVAFilterDesktop

        _FilterB = MVAFilterDesktop( name + "Filter",
                                     Code = "VALUE('LoKi::Hybrid::DictValue/" + name + "')>" + MVACutValue  )
        
        addTMVAclassifierValue( Component = _FilterB,
                                XMLFile   = MVAxmlFile,
                                Variables = MVAVars,
                                ToolName  = name )
        return Selection( name,
                          Algorithm =  _FilterB,
                          RequiredSelections = [ SelB ] )
