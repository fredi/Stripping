###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for Charmonium->Ks K pi, including three lines
1. Normal line
2. Exclusive
3. Detached

'''

__author__ = ['Valeriia Zhovkovska']
__date__ = '07/03/2019'
__version__ = '$Revision: 1.0 $'

__all__ = ('Ccbar2KsKpiConf', 'default_config')

# If you have several configs in one module your default_config should
# be a dict of configs with the names of the configs as keys. The configs
# themselves then shouldn't have a 'NAME' element.
# M.A. 2017/05/02.
default_config = {
    'Ccbar2KsKpi': {
        'BUILDERTYPE': 'Ccbar2KsKpiConf',
        'CONFIG': {
            'LinePrescale':
            1.,
            'LinePostscale':
            1.,
            'SpdMult':
            450.,  # dimensionless, Spd Multiplicy cut
            'KaonCuts':
            "(PROBNNk > 0.2)  & (PT > 1500*MeV) & (TRGHOSTPROB<0.4) & (TRCHI2DOF < 5)",
            'PionCuts':
            "(PROBNNpi > 0.2) & (PT > 1500*MeV) & (TRGHOSTPROB<0.4) & (TRCHI2DOF < 5)",
            'KsCuts':
            "(ADMASS('KS0') < 30.*MeV) & (BPVDLS>5) & (PT > 1500*MeV) & (MAXTREE('pi-'==ABSID, PROBNNpi) > 0.2) & (MAXTREE('pi-'==ABSID, TRGHOSTPROB) < 0.4) & (MAXTREE('pi-'==ABSID, TRCHI2DOF) < 5) & (MAXTREE('pi-'==ABSID, MIPCHI2DV(PRIMARY)) > 4)",
            'CombCuts':
            "in_range(2.7*GeV, AM, 4.4*GeV)",
            'MomCuts':
            "in_range(2.7*GeV, MM, 4.4*GeV) & (VFASPF(VCHI2/VDOF) < 9.)",
            'CCCut':
            ""
        },
        'STREAMS': ['Charm'],
        'WGs': ['BandQ']
    }
}

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand, AutomaticData
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder


class Ccbar2KsKpiConf(LineBuilder):

    __configuration_keys__ = ('LinePrescale', 'LinePostscale', 'SpdMult',
                              'KaonCuts', 'PionCuts', 'KsCuts', 'CombCuts',
                              'MomCuts', 'CCCut')

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)

        self.name = name

        from PhysSelPython.Wrappers import MergedSelection

        self.InputKs = MergedSelection(
            self.name + "InputKs",
            RequiredSelections=[
                DataOnDemand(Location="Phys/StdLooseKsDD/Particles"),
                DataOnDemand(Location="Phys/StdVeryLooseKsLL/Particles")
            ])
        #DataOnDemand(Location = "Phys/StdLooseKsLL/Particles")] )

        self.SelKs = self.createSubSel(
            OutputList=self.name + "SelKs",
            InputList=self.InputKs,
            Cuts=config['KsCuts'])

        self.SelKaons = self.createSubSel(
            OutputList=self.name + "SelKaons",
            InputList=DataOnDemand(Location='Phys/StdLooseKaons/Particles'),
            Cuts=config['KaonCuts'])

        self.SelPions = self.createSubSel(
            OutputList=self.name + "SelPions",
            InputList=DataOnDemand(
                Location='Phys/StdAllNoPIDsPions/Particles'),
            Cuts=config['PionCuts'])

        # Eta_c -> KS0 K Pi
        self.SelEtac2KsKPi = self.createCombinationSel(
            OutputList=self.name + "SelEtac2KsKPi",
            DecayDescriptor="[eta_c(1S) -> KS0 K+ pi-]cc",
            DaughterLists=[self.SelKs, self.SelKaons, self.SelPions],
            PreVertexCuts=config['CombCuts'],
            PostVertexCuts=config['MomCuts'])

        SpdMultForCcbarCut = config['SpdMult']

        self.line = StrippingLine(
            self.name + "Line",
            prescale=config['LinePrescale'],
            postscale=config['LinePostscale'],
            FILTER={
                'Code':
                " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMultForCcbarCut)s )"
                % locals(),
                'Preambulo': [
                    "from LoKiNumbers.decorators import *",
                    "from LoKiCore.basic import LHCb"
                ]
            },
            checkPV=True,
            selection=self.SelEtac2KsKPi)

        self.registerLine(self.line)

    def createSubSel(self, OutputList, InputList, Cuts):
        '''create a selection using a FilterDesktop'''
        filt = FilterDesktop(Code=Cuts)
        return Selection(
            OutputList, Algorithm=filt, RequiredSelections=[InputList])

    def createCombinationSel(self,
                             OutputList,
                             DecayDescriptor,
                             DaughterLists,
                             DaughterCuts={},
                             PreVertexCuts="ALL",
                             PostVertexCuts="ALL",
                             ReFitPVs=True):
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles(
            DecayDescriptor=DecayDescriptor,
            DaughtersCuts=DaughterCuts,
            MotherCut=PostVertexCuts,
            CombinationCut=PreVertexCuts,
            ReFitPVs=False)
        return Selection(
            OutputList, Algorithm=combiner, RequiredSelections=DaughterLists)
