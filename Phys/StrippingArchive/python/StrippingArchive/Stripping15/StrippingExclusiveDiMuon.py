###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#Stripping Lines for Low Multiplicity Processes.
#Electroweak Group (Convenor: Tara Shears)
#Written by Will Barter

# Accepts events with few tracks. Used to study background.


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder


confdict_ExclusiveDiMuon={
    'ExclusiveDiMuonPrescale'    : 1.0 
    ,  'ExclusiveDiMuonPostscale'   : 1.0
    }

name = "ExclusiveDiMuon"

class ExclusiveDiMuonConf(LineBuilder) :

    __configuration_keys__ = ('ExclusiveDiMuonPrescale',
                              'ExclusiveDiMuonPostscale',
                              )
    
    def __init__(self, name, config) :
        LineBuilder.__init__(self, name, config)

        self._myname = name
        
        ExclusiveDiMuonNumTracksGEC = {'Code' : "(TrNUM('Rec/Track/Best', TrLONG) <= 5) & (TrNUM('Rec/Track/Best', TrLONG) >= 1)",
                                       'Preambulo' : ["from LoKiTracks.decorators import *"]}
        


        self.ExclusiveDiMuon_line = StrippingLine(self._myname+"BackgroundLine",
                                                  prescale = config['ExclusiveDiMuonPrescale'],
                                                  postscale = config['ExclusiveDiMuonPostscale'],
                                                  checkPV = False,
                                                  HLT = "HLT_PASS('Hlt1MBNoBiasDecision')",
                                                  FILTER =  ExclusiveDiMuonNumTracksGEC
                                                  )
        
        self.registerLine(self.ExclusiveDiMuon_line)
        
      
