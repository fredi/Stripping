###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Stripping line to mimic Electron Track Reconstruction Efficiency HLT2 line
Tag and probe with VELO info for J/psi -> ee
'''

__author__=['Adam Davis','Laurent Dufour','V. V. Gligorov']
__date__='13/10/2017'
__version__='$Revision: 1.0 $'

__all__ = (
    'StrippingElectronRecoEffLines',
    'default_config',
    'TOSFilter'
    )

from Gaudi.Configuration import *
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from StrippingUtils.Utils import LineBuilder
from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from PhysSelPython.Wrappers import Selection#, Hlt1Selection, Hlt2Selection
from StrippingConf.StrippingLine import StrippingLine
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop


## for velo tracking
from SelPy.utils import ( UniquelyNamedObject,
                          ClonableObject,
                          SelectionBase )
from Configurables import (DecodeVeloRawBuffer, FastVeloTracking, TrackPrepareVelo, 
                           NoPIDsParticleMaker, DataOnDemandSvc, ChargedProtoParticleMaker, 
                           PrTrackAssociator, DelegatingTrackSelector, TrackContainerCopy, TrackAssociator,
                          TrackStateInitAlg, TrackStateInitTool) 
from TrackFitter.ConfiguredFitters import (ConfiguredEventFitter,
                                           ConfiguredForwardStraightLineEventFitter)

default_config = {'NAME': 'ElectronRecoEff',
                  'WGs' : ['ALL'],
                  'BUILDERTYPE':'StrippingElectronRecoEffLines',
                  'CONFIG': {# from HLT line
    'TrackGEC'             : 120,
    # #velo options
    'DoVeloDecoding'       : False,
    "VeloFitter"           : "SimplifiedGeometry",
    "VeloMINIP"            : 0.05 * mm,
    "VeloTrackChi2"        : 3.5,
    "EtaMinVelo"           : 1.9,
    "EtaMaxVelo"           : 5.1,
    #
    'SharedChild'          : {'TrChi2Mu'   :   5,
                              'TrChi2Ele'  :   5,
                              'TrChi2Kaon' :   5,
                              'TrChi2Pion' :   5,
                              'IPMu'       :   0.0 * mm,
                              'IPEle'      :   0.0 * mm,
                              'IPKaon'     :   0.0 * mm,
                              'IPPion'     :   0.0 * mm,
                              'IPChi2Mu'   :   16,
                              'IPChi2Ele'  :   16,
                              'IPChi2Kaon' :   16,
                              'IPChi2Pion' :   36,
                              'EtaMinMu'   :   1.8,
                              'EtaMinEle'  :   1.8,
                              'EtaMaxMu'   :   3.5,
                              'EtaMaxEle'  :   3.5,
                              'ProbNNe'    :   0.2,
                              'ProbNNmu'   :   0.5,
                              'ProbNNk'    :   0.2,
                              'ProbNNpi'   :   0.8,
                              'PtMu'       :   3000 * MeV,
                              'PtEle'      :   2500 * MeV,
                              'PtKaon'     :   500 * MeV,
                              'PtPion'     :   1000 * MeV },

    'DetachedEEK'          : {'AMTAP'      :   6000*MeV,
                              'VCHI2TAP'   :   20,
                              'MLOW'       :   5000.*MeV,
                              'MHIGH'      :   5700.*MeV,
                              'bmass_ip_constraint': -3.0,
                              "bCandFlightDist"          : 5.0*mm,
                              'overlapCut' :   0.95,
                              'TisTosSpec' :   {"Hlt1TrackMVA.*Decision%TOS":0}
                              },
    'DetachedEK'           : {'AM'         :   5000*MeV,
                              'VCHI2'      :   15,
                              'VDCHI2'     :   36,
                              'DIRA'       :   0.95,
                              'EKIP'       :   -1.0*mm,
                              "bCandFlightDist"          : 5.0*mm,
                              'TisTosSpec' :   {"Hlt1TrackMVA.*Decision%TOS":0}
                              },

    # 'L0Req'                : {'DetachedMuMuK': "L0_CHANNEL('Muon')",
    #                           'DetachedEEK'  : "L0_CHANNEL('Electron')",
    #                           'DetachedEK'   : "L0_CHANNEL('Electron')",
    #                           'DetachedEPi'  : "L0_CHANNEL('Electron')",
    #                           'DetachedMuK'  : "L0_CHANNEL('Muon')",
    #                           'DetachedMuPi' : "L0_CHANNEL('Muon')" },
    'Hlt1Req'              : {#'DetachedMuMuK': "HLT_PASS_RE('Hlt1Track(Muon)?MVA.*Decision')",
                              'DetachedEEK'  : "HLT_PASS_RE('Hlt1TrackMVA.*Decision')",
                              'DetachedEK'   : "HLT_PASS_RE('Hlt1TrackMVA.*Decision')",
                              },
    
    'Hlt2Req'               : {'DetachedEK' : "HLT_PASS_RE('Hlt2.*Topo.*Decision')"}
                  
    },
                  'STREAMS' : ['BhadronCompleteEvent']
                  }



from StandardParticles import StdNoPIDsMuons as Hlt2Muons
from StandardParticles import StdNoPIDsElectrons as Hlt2Electrons
from StandardParticles import StdAllLooseKaons as Hlt2Kaons
from StandardParticles import StdAllLoosePions as Hlt2Pions

class StrippingElectronRecoEffLines(LineBuilder):
    """
    Stripping 'replica' for HLT electron reconstruction efficiency trigger
    """

    __configuration_keys__ = default_config['CONFIG'].keys()
    def __init__(self,name,config):        
        LineBuilder.__init__(self, name, config)
        from PhysSelPython.Wrappers import Selection, DataOnDemand
        self._config = config
        self.name = name
        
        self.GECs = { "Code":"( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %(TrackGEC)s )" % config,
                      "Preambulo": ["from LoKiTracks.decorators import *"]}
        #print 's1'
        self._DetachedElectrons=None
        self._DetachedKaons=None

        ###### the velo tracking (from D2K3pi line from mika)        
        self.VeloProtoOutputLocation = 'Rec/ProtoP/VeloProtosFor%s'%self.name
        self.VeloTrackOutputLocation="Rec/Track/MyVeloFor%s"%self.name
        self.FittedVeloTrackOutputLocation = "Rec/Track/PreparedVeloFor%s"%self.name        
        self.VeloTracks = self.MakeVeloTracks([])

        self.Hlt2ProbeElectrons = self.MakeVeloParticles("VeloElectrons","electrons",self.VeloTracks)

        ###rest of it.
        self.DetachedEKPair("DetachedEKPair")
        #actual registration of the lines

        self.registerLine(self.DetachedEEKPair("DetachedEEKPair"))
        
    ##definitions of inputs


    def _DetachedElectronFilter(self):
        if not self._DetachedElectrons:
            from PhysSelPython.Wrappers import Selection
            code = "(MIPDV(PRIMARY)>%(IPEle)s) & (MIPCHI2DV(PRIMARY)>%(IPChi2Ele)s) & (PT> %(PtEle)s) & \
            (TRCHI2DOF<%(TrChi2Ele)s)  & in_range(%(EtaMinEle)s, ETA, %(EtaMaxEle)s) & (PROBNNe > %(ProbNNe)s)"%self._config['SharedChild']
            _DetachedElectrons = Selection("DetachedElectrons_For_"+self.name,
                                           Algorithm = FilterDesktop(Code = code),
                                           RequiredSelections = [Hlt2Electrons]
                                           )
            self._DetachedElectrons  = _DetachedElectrons
        return self._DetachedElectrons

    def _DetachedKaonFilter(self):
        if not self._DetachedKaons:
            from PhysSelPython.Wrappers import Selection
            code = ("(MIPDV(PRIMARY)>%(IPKaon)s) & (MIPCHI2DV(PRIMARY)>%(IPChi2Kaon)s) & (PT> %(PtKaon)s) & \
            (TRCHI2DOF<%(TrChi2Kaon)s) & (PROBNNk > %(ProbNNk)s)")%self._config['SharedChild']
            _DetachedKaons = Selection("DetachedKaons_For_"+self.name,
                                       Algorithm = FilterDesktop(Code = code),
                                       RequiredSelections = [Hlt2Kaons])
            self._DetachedKaons = _DetachedKaons
        return self._DetachedKaons
    
    
    def DetachedEKPair(self,_name):
        dc = {'K+' : "ALL", 'e+' : "ALL"}
        cc = ("(AM < %(AM)s)")%self._config['DetachedEK']
        mc = ("(VFASPF(VCHI2) < %(VCHI2)s) & (MIPDV(PRIMARY)>%(EKIP)s) & (BPVVDCHI2 > %(VDCHI2)s) & (VFASPF(VMINVDDV(PRIMARY)) > %(bCandFlightDist)s )")%self._config['DetachedEK']        
        _DetachedEKPair = CombineParticles(
            DecayDescriptors = ['[J/psi(1S) -> e+ K-]cc','[J/psi(1S) -> e+ K+]cc'],
            DaughtersCuts = dc,
            CombinationCut = cc,
            MotherCut = mc,
            )
        #EK_HLT1Selection = Hlt1Selection('EK_HLT1Selection','%(DetachedEK)s'%self._config['Hlt1Req'])
        #EK_HLT2Selection = Hlt2Selection('EK_HLT2Selection',"%(DetachedEK)s"%self._config['Hlt2Req'])
        self._DetachedEKPairSel = Selection("SelKE_for_"+_name,
                                            Algorithm = _DetachedEKPair,
                                            RequiredSelections = [self._DetachedKaonFilter(),
                                                                  self._DetachedElectronFilter(),
                                                                  #EK_HLT1Selection,
                                                                  #EK_HLT2Selection
                                                                  ]
                                            )
        self._EK_TOSFilter = TOSFilter(_name,self._DetachedEKPairSel,self._config['DetachedEK']['TisTosSpec'])
        

    ###definition of stripping lines

    def DetachedEEKPair(self,_name):
        dc = {"e+" : "ALL", "J/psi(1S)" : "ALL"}
        cc = "(AM < %(AMTAP)s)"%self._config['DetachedEEK']
        mc = "(VFASPF(VCHI2) < %(VCHI2TAP)s) & (log(B_MASS_CONSTRAINT_IP) < %(bmass_ip_constraint)s) & in_range(%(MLOW)s, BMassFromConstraint, %(MHIGH)s) "%self._config['DetachedEEK']
        #(MAXOVERLAP( (ABSID == 'e+') | (ABSID=='K-') ) < %(overlapCut)s) is broken in this DV version

        #inputs = [DetachedEKPair('DetachedEK'),Hlt2ProbeElectrons]
        preambulo =  [ # With thanks to L. Dufour!
            'from numpy import inner', #if you want you can also calculate all inner products yourself.
            'TagKaonMomentumVector    = [CHILD(CHILD(PX,2), 1), CHILD(CHILD(PY,2),1), CHILD(CHILD(PZ,2),1)]',
            'TagKaonEnergy            = CHILD(CHILD(E,2),1)',
            #
            'TagElectronMomentumVector    = [CHILD(CHILD(PX,1), 1), CHILD(CHILD(PY,1),1), CHILD(CHILD(PZ,1),1)]',
            'TagElectronEnergy = CHILD(CHILD(E,1), 1)',
            #
            'ProbeElectron    = [CHILD(PX,2),CHILD(PY,2),CHILD(PZ,2)]',
            'ProbeUnnormalised = ProbeElectron[:]',
            'ProbeElectron = [ProbeUnnormalised[i]/CHILD(P,2) for i in range (0,3)]',
            #
            'TagPCosineTheta = inner(ProbeElectron, TagElectronMomentumVector)', # |p_tag| Cos(Theta)
            #
            'Electron_M = 0.511', # in MeV
            #
            # ideally would replace the 3096.9 with a functor to get the PDG mass for the J/Psi(1S) in MeV
            # (there must be a functor for this PDG mass...)
            'Probe_Momentum_From_Mass_constraint = 0.5*(3096.9**2 - Electron_M**2 - Electron_M**2)/(TagElectronEnergy - TagPCosineTheta)',
            'JPsi_momentum = [ Probe_Momentum_From_Mass_constraint*ProbeElectron[i] + TagElectronMomentumVector[i] for i in range (0,3)]',
            'JPsi_energy = TagElectronEnergy + math.sqrt(Electron_M**2 + Probe_Momentum_From_Mass_constraint**2)',
            'BMassFromConstraint = math.sqrt( (JPsi_energy+TagKaonEnergy)**2 - (JPsi_momentum[0]+TagKaonMomentumVector[0])**2 -(JPsi_momentum[1]+TagKaonMomentumVector[1])**2 - (JPsi_momentum[2]+TagKaonMomentumVector[2])**2)',
            ##new from Laurent
            "mass_constraint_b_momentum_vector = [ TagKaonMomentumVector[i] + JPsi_momentum[i] for i in range (0,3)]",
            "mass_constraint_b_momentum = math.sqrt(mass_constraint_b_momentum_vector[0]**2 + mass_constraint_b_momentum_vector[1]**2 + mass_constraint_b_momentum_vector[2]**2)",
            "normalised_mass_constraint_b_momentum_vector = [mass_constraint_b_momentum_vector[i]/mass_constraint_b_momentum for i in range (0,3)]",
            "B_ENDVERTEX_POSITION = [VFASPF(VX), VFASPF(VY), VFASPF(VZ)]",
            "B_PV_POSITION = [BPV(VX),BPV(VY),BPV(VZ)]",

            "LambdaFactor = - (inner([VFASPF(VX)-BPV(VX), VFASPF(VY)-BPV(VY), VFASPF(VZ)-BPV(VZ)], normalised_mass_constraint_b_momentum_vector))",
            "B_MASS_CONSTRAINT_IP_VECTOR = [(B_ENDVERTEX_POSITION[i] + LambdaFactor * normalised_mass_constraint_b_momentum_vector[i]) - B_PV_POSITION[i] for i in range (0,3)]",
            "B_MASS_CONSTRAINT_IP = math.sqrt(B_MASS_CONSTRAINT_IP_VECTOR[0]**2+B_MASS_CONSTRAINT_IP_VECTOR[1]**2+B_MASS_CONSTRAINT_IP_VECTOR[2]**2)",            
            ]
        _DetachedEEKPair = CombineParticles(
            DecayDescriptors = ["B+ -> J/psi(1S) e+","B- -> J/psi(1S) e-"],
            DaughtersCuts = dc,
            CombinationCut = cc,
            MotherCut = mc,
            Preambulo = preambulo
            )
        
        _DetachedEEKPair_Sel = Selection("SelEEK_for_"+_name,
                                         Algorithm = _DetachedEEKPair,
                                         RequiredSelections = [self._EK_TOSFilter,
                                                               self.Hlt2ProbeElectrons
                                                               ]
                                         )
        _DetachedEEK_TOSFilter= TOSFilter(_name,_DetachedEEKPair_Sel,
                                          self._config["DetachedEEK"]['TisTosSpec'])
        
        line = StrippingLine(_name+"Line",
                             prescale = 1.0,
                             FILTER = self.GECs,
                             HLT1 = "%(DetachedEEK)s"%self._config['Hlt1Req'],
                             RequiredRawEvents = ["Velo","Calo"],
                             selection = _DetachedEEK_TOSFilter,
                             
                             )

        return line

    ###helpers
    ##### velo tracking ####
    def MakeVeloTracks(self,prefilter):        
        if self._config["DoVeloDecoding"]:
            from DAQSys.Decoders import DecoderDB
            from DAQSys.DecoderClass import decodersForBank
            decs=[]
            vdec=DecoderDB["DecodeVeloRawBuffer/createBothVeloClusters"]
            vdec.Active=True
            DecoderDB["DecodeVeloRawBuffer/createVeloClusters"].Active=False
            DecoderDB["DecodeVeloRawBuffer/createVeloLiteClusters"].Active=False
            decs=decs+[vdec]
            VeloDecoding = GaudiSequencer("RecoDecodingSeq")
            VeloDecoding.Members += [d.setup() for d in decs ]
        
        MyFastVeloTracking = FastVeloTracking("For%sFastVelo"%self.name,
                                              OutputTracksName=self.VeloTrackOutputLocation)
        MyFastVeloTracking.OnlyForward = True
        MyFastVeloTracking.ResetUsedFlags = True
        ### prepare for fitting
        preve = TrackStateInitAlg("For%sInitSeedFit"%self.name,
                                  TrackLocation = self.VeloTrackOutputLocation)
        preve.StateInitTool.VeloFitterName = "FastVeloFitLHCbIDs"
        copyVelo = TrackContainerCopy( "For%sCopyVelo"%self.name )
        copyVelo.inputLocations = [self.VeloTrackOutputLocation]
        copyVelo.outputLocation = self.FittedVeloTrackOutputLocation
        
        ### fitting
        if self._config["VeloFitter"] == "ForwardStraightLine":
            MyVeloFit = ConfiguredForwardStraightLineEventFitter(Name="For%sVeloRefitterAlg"%self.name,
                                                                 TracksInContainer=self.FittedVeloTrackOutputLocation)
        elif self._config["VeloFitter"] == "SimplifiedGeometry":
            MyVeloFit = ConfiguredEventFitter(Name="For%sVeloRefitterAlg"%self.name,
                                              TracksInContainer=self.FittedVeloTrackOutputLocation,
                                              SimplifiedGeometry = True)
        else:
            MyVeloFit = ConfiguredEventFitter(Name="For%sVeloRefitterAlg"%self.name,
                                              TracksInContainer=self.FittedVeloTrackOutputLocation)
            
        #### making the proto particles
        MakeVeloProtos = ChargedProtoParticleMaker('For%sVeloProtoMaker'%self.name)
        MakeVeloProtos.Inputs=[self.FittedVeloTrackOutputLocation]
        MakeVeloProtos.Output = self.VeloProtoOutputLocation
        MakeVeloProtos.addTool( DelegatingTrackSelector, name="TrackSelector" )
        MakeVeloProtos.TrackSelector.TrackTypes = [ "Velo" ]
    
        #### the full sequence
        makeparts = GaudiSequencer('For%sMakeVeloTracksGS'%self.name)
        if self._config["DoVeloDecoding"]:
            makeparts.Members += [ VeloDecoding ] 
        makeparts.Members += [ MyFastVeloTracking ] 
        makeparts.Members += [ preve ] 
        makeparts.Members += [ copyVelo ] 
        makeparts.Members += [ MyVeloFit ] 
        makeparts.Members += [ MakeVeloProtos ] 
    
        #### some python magic to make this appear like a "Selection"
        return GSWrapper(name="For%sWrappedVeloTrackingFor"%self.name,
                         sequencer=makeparts,
                         output=self.VeloProtoOutputLocation,
                         requiredSelections =  prefilter)

    def MakeVeloParticles(self,name,
                          particle, 
                          protoParticlesMaker):        
        particleMaker =  NoPIDsParticleMaker("For%sParticleMaker%s"%(self.name,name) , Particle = particle, AddBremPhotonTo = [])
        particleMaker.Input = self.VeloProtoOutputLocation

        DataOnDemandSvc().AlgMap.update( {
                "/Event/Phys/" + particleMaker.name() + '/Particles' : particleMaker.getFullName(),
                "/Event/Phys/" + particleMaker.name() + '/Vertices'  : particleMaker.getFullName()
                } )

        AllVeloParticles = Selection("For%sSelAllVeloParts%s"%(self.name,name), 
                                     Algorithm = particleMaker, 
                                     RequiredSelections = [protoParticlesMaker], InputDataSetter=None)
        
        ### filter on the IP of the velo tracks
        return Selection("For%sSelVeloParts%s"%(self.name,name), 
                     Algorithm = FilterDesktop(Code="(MIPDV(PRIMARY) > %(VeloMINIP)s) & (TRCHI2DOF<%(VeloTrackChi2)s) & in_range(%(EtaMinVelo)s, ETA, %(EtaMaxVelo)s)" %self._config),
                     RequiredSelections = [AllVeloParticles])
        
#####Tos filter from B2DMuNuXUtils
def TOSFilter( name = None, sel = None, trigger = None ):
    if len(trigger) == 0:
        return sel
    from Configurables import TisTosParticleTagger
    _filter = TisTosParticleTagger(name+"_TriggerTos")
    _filter.TisTosSpecs = trigger
    _sel = Selection("Sel" + name + "_TriggerTos", RequiredSelections = [ sel ], Algorithm = _filter )
    return _sel
###### OTHER FUNCTIONS ###############
class GSWrapper(UniquelyNamedObject,
                ClonableObject,
                SelectionBase) :
    
    def __init__(self, name, sequencer, output, requiredSelections) :
        UniquelyNamedObject.__init__(self, name)
        ClonableObject.__init__(self, locals())
        SelectionBase.__init__(self,
                               algorithm = sequencer,
                               outputLocation = output,
                               requiredSelections = requiredSelections )        
            


#patch HltXLine, etc for the stripping
#from http://lhcb-doxygen.web.cern.ch/lhcb-doxygen/davinci/v42r6p1/py/dd/dd1/_wrappers_8py.html
from   SelPy.selection import EventSelection       as EvtSel
# =============================================================================
## Construct a selection of type selType with construction arguments.
#  Checks if name already belongs to an existing Configurable.
#  Checks whether input is Configurable and if so, extracts its non-default
#  parameters and passes them as a configurableGenerator.
def selectionWrapper(selType, name, *args, **kwargs) :
    """Construct a selection of type selType with construction arguments.
    Checks if name already belongs to an existing Configurable.
    Checks whether input is Configurable and if so, extracts its non-default
    parameters and passes them as a configurableGenerator.
    """
    checkName(name)
    
    algorithm = kwargs.pop('Algorithm')
    if isConfigurable( algorithm )  :
        checkConfigurable( algorithm)
        algGen=CloneCallable(algorithm)
        kwargs['ConfGenerator'] = algGen
    else :
        kwargs['ConfGenerator'] = algorithm
    return selType(name, *args, **kwargs)
# =============================================================================
## Wrapper around SelPy.EventSelection. Since SelPy.EventSelection takes a
#  ConfigurableGenerator as input and constructs a Configurable
#  with it it's own name, check first whether a configurable
#  with that name already exists and raise a NameError if that is the case.
#  If not, construct and return a SelPy.selection.EventSelection.
def EventSelection(name, *args, **kwargs) :
    """Wrapper around SelPy.EventSelection. Since SelPy.EventSelection takes a
    ConfigurableGenerator as input and constructs a Configurable
    with it it's own name, check first whether a configurable
    with that name already exists and raise a NameError if that is the case.
    If not, construct and return a SelPy.selection.EventSelection.
    """
    return selectionWrapper(EvtSel, name, *args, **kwargs)

# =============================================================================
## Simple wrapper for EventSelection, that allows to select/filter events according
#  to (global) trigger decision
#  @code
#  hlt1  = TriggerSelection( 'MyTrigger', 'Hlt1' , "HLT_PASS_RE('Hlt1.*DiMuon.*')" )
#  pions = ...
#  kaons = ...
#  mysel = SimpleSelection ( 'D0' , [ hlt1 , pions , kaons ] , .... )
#  @endcode
#  @see EventSelection
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2016-11-09
def TriggerSelection ( name          ,
                       trigger       ,   ## MUST be L0, L0DU, Hlt1, Hlt2 or Strip, Stripping
                       Code          ,   ## the code....
                       Location = '' ,   ## location of DecReports
                       *args         ,   ## other properties
                       **kwargs      ) : ## other properties
    """Simple wrapper for EventSelection, that allows to select/filter events according
    to (global) trigger decision:
    >>> hlt1  = TriggerSelection( 'MyTrigger', 'Hlt1' , \"HLT_PASS_RE('Hlt1.*DiMuon.*')\" )
    >>> pions = ...
    >>> kaons = ...
    >>> mysel = SimpleSelection ( 'D0' , [ hlt1 , pions , kaons ] , .... )
    """
    
    if kwargs.has_key('Algorithm') :
        raise KeyError ( 'Algorithm is not allowed keyword for TriggerSelection!')
    
    if   trigger.upper() in ( 'L0' , 'L0DU' ) :
        from GaudiConfUtils.ConfigurableGenerators import LoKi__L0Filter  as _ALGORITHM_
    elif trigger.upper() in ( 'HLT1' , 'HLT2' , 'STRIP' , 'STRIPPING' ) :
        from GaudiConfUtils.ConfigurableGenerators import LoKi__HDRFilter as _ALGORITHM_
    else :
        raise KeyError ( 'Trigger type "%s" is illegal for TriggerSelection' % trigger )
    
    if   Location : kwargs[ 'Location' ] = Location
    elif trigger.upper() in ( 'HLT1'  ,             ) : kwargs[ 'Location' ] = 'Hlt1/DecReports'
    elif trigger.upper() in ( 'HLT2'  ,             ) : kwargs[ 'Location' ] = 'Hlt2/DecReports'
    elif trigger.upper() in ( 'STRIP' , 'STRIPPING' ) : kwargs[ 'Location' ] = 'Strip/DecReports'
    
    kwargs[ 'Code'      ] =  Code
                                           
    return EventSelection ( name , Algorithm = _ALGORITHM_ ( *args , **kwargs ) )

# =============================================================================
## Simple wrapper for TriggerSelection, that allows to select/filter events according
#  to (global) trigger decision
#  @code
#  hlt1  = Hlt1Selection( 'MyTrigger', "HLT_PASS_RE('Hlt1.*DiMuon.*')" )
#  pions = ...
#  kaons = ...
#  mysel = SimpleSelection ( 'D0' , [ hlt1 , pions , kaons ] , .... )
#  @endcode
#  @see Hlt2Selection
#  @see StrippingSelection
#  @see TriggerSelection
#  @see EventSelection
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2016-11-09
def Hlt1Selection ( name , Code , *args , **kwargs ) :
    """Simple wrapper for TriggerSelection, that allows to select/filter events according
    to (global) trigger decision:
    >>> hlt1  = Hlt1Selection( 'MyTrigger' , \"HLT_PASS_RE('Hlt1.*DiMuon.*')\" )
    >>> pions = ...
    >>> kaons = ...
    >>> mysel = SimpleSelection ( 'D0' , [ hlt1 , pions , kaons ] , .... )
    """
    return TriggerSelection ( name , 'Hlt1' , Code , '' , *args , **kwargs )


# =============================================================================
## Simple wrapper for TriggerSelection, that allows to select/filter events according
#  to (global) trigger decision
#  @code
#  hlt2  = Hlt2Selection( 'MyTrigger', "HLT_PASS_RE('Hlt2.*DiMuon.*')" )
#  pions = ...
#  kaons = ...
#  mysel = SimpleSelection ( 'D0' , [ hlt2 , pions , kaons ] , .... )
#  @endcode
#  @see Hlt2Selection
#  @see StrippingSelection
#  @see TriggerSelection
#  @see EventSelection
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2016-11-09
def Hlt2Selection ( name , Code , *args , **kwargs ) :
    """Simple wrapper for TriggerSelection, that allows to select/filter events according
    to (global) trigger decision:
    >>> hlt2  = Hlt2Selection( 'MyTrigger' , \"HLT_PASS_RE('Hlt2.*DiMuon.*')\" )
    >>> pions = ...
    >>> kaons = ...
    >>> mysel = SimpleSelection ( 'D0' , [ hlt2 , pions , kaons ] , .... )
    """
    return TriggerSelection ( name , 'Hlt2' , Code , '' , *args , **kwargs )

 
###the end
