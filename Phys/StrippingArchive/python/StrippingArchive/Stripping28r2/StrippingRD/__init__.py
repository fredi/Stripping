###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

_selections = ('StrippingB02Lcmu_pKpi', 'StrippingB23MuLines', 'StrippingB2EtaMuMu', 'StrippingB2KLLXInclusive', 'StrippingB2KstTauTau', 'StrippingB2LLXBDT', 'StrippingB2Lambda0ELines', 'StrippingB2Lambda0MuLines', 'StrippingB2Lambda0MuOSLines', 'StrippingB2Lambda0SSMuMu3PiLines', 'StrippingB2MuMuMuMuLines', 'StrippingB2XLL', 'StrippingB2XMuMu', 'StrippingB2XTau', 'StrippingB2XTauMu', 'StrippingB2XTauTau', 'StrippingBLVLines', 'StrippingBd2KSLLX', 'StrippingBd2eeKstarBDT', 'StrippingBeauty2XGamma', 'StrippingBeauty2XGammaExclTDCPV', 'StrippingBeauty2XGammaExclusive', 'StrippingBeauty2XGammaNoBias', 'StrippingBs2MuMuGamma', 'StrippingBs2MuMuLines', 'StrippingBs2gammagamma', 'StrippingBs2st2KKMuX', 'StrippingBu2LLK', 'StrippingBu2MajoLep', 'StrippingBu2MuNu', 'StrippingD23MuLines', 'StrippingDarkBoson', 'StrippingDoubleSLForRX', 'StrippingHypb2L0HGamma', 'StrippingK0s2XXMuMu', 'StrippingKshort2Leptons', 'StrippingKshort2MuMuMuMu', 'StrippingKshort2PiPiMuMu', 'StrippingKshort2eePiPi', 'StrippingLFVLines', 'StrippingLb2L0Gamma', 'StrippingLc23MuLines', 'StrippingPhiToKSKS', 'StrippingRareNStrange', 'StrippingRareStrange', 'StrippingStrangeHadrons', 'StrippingStrangeSL', 'StrippingTau23MuLines', 'StrippingTau2LambdaMuLines', 'StrippingZVTOP')

for _sel in _selections :
    try :
        __import__( '%s.%s'  % ( __name__, _sel ) )
    except Exception, x:
        print '[WARNING] Submodule %s.%s raises the exception "%s" and will be skipped !' % ( __name__,_sel,x )

from sys import modules as _modules
_this = _modules[__name__]

_strippingKeys = filter ( lambda x : x[:9]=='Stripping',
                          locals().keys())

_strippingModules = [getattr(_this, _k) for _k in _strippingKeys]
