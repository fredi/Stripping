###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Set of lines for calo PID
Ds  -> Etap[rho gamma] pi
Ds* -> Ds[KKpi] gamma
eta -> mu mu gamma
Selections taken from TURCAL 2017-2018
v1.1: use StdVeryTightDsplus2KKPi instead of StdLooseDplus2KKPi to improve timing of Ds* line"
'''

__author__ = ['Max Chefdeville']
__date__ = '27/06/2018'
__version__ = '$Revision: 1.1 $'

__all__ = ('CaloPIDConf', 'default_config')

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Combine3BodySelection
from CommonParticles.Utils import updateDoD
from StandardParticles import StdLoosePions
from StandardParticles import StdLooseKaons
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import MeV, GeV, mm

default_config = {
    'NAME': 'CaloPID',
    'WGs': ['Calib'],
    'BUILDERTYPE': 'CaloPIDConf',
    'CONFIG': {

        #Ds2EtapPi
        'Etap_Pi_PT': 500.,  #MeV
        'Etap_Pi_MIPCHI2DV': 16.,
        'Etap_Pi_Track_Chi2ndof': 5.,
        'Etap_Pi_TRGHOSTPROB': 0.5,
        'Etap_Pi_P': 1000.,  #MeV
        'Etap_Pi_PID': 0,
        'Etap_gamma_PT': 1000.,  #MeV
        'Etap_Mass_Min': 900.,  #MeV
        'Etap_Mass_Max': 1020.,  #MeV
        'D_Pi_BACH_PT': 600.,  #MeV
        'D_APT': 2000.,  #MeV
        'D_Mass_Min': 1800.,  #MeV
        'D_Mass_Max': 2100.,  #MeV
        'D_Vtx_Chi2ndof': 4.,
        'D_BPVLTIME': 0.25,  #ps
        'D_DIRA': 0.999975,
        'D_IP': 0.05,  # mm,
        'D_IP_Chi2': 10.,

        #Eta2MuMuGamma
        'Eta_MuPT': 500,  #MeV
        'Eta_MuP': 10000,  #MeV
        'Eta_GhostProb': 0.3,
        'Eta_MuProbNNmu': 0.8,
        'Eta_DOCA': 0.2,  #mm
        'Eta_VChi2': 10,
        'Eta_MuIPChi2': 6.0,
        'Eta_MuPTPROD': 1,  #GeV^2
        'Eta_DiMu_PT': 1000,  #MeV
        'Eta_DiMu_FDChi2': 45,
        'Eta_Mass_Min': 400.,  #MeV
        'Eta_Mass_Max': 700.,  #MeV
        'Eta_gamma_PT': 200,  #MeV

        #Dsst2DsGamma
        'Dsst_Mass_Min': 2050.,  #MeV
        'Dsst_Mass_Max': 2250.,  #MeV
        'Dsst_DIRA': 0.999975,
        'Dsst_IP': 0.05,  #mm
        'Dsst_IP_Chi2': 10.,
        'Dsst_gamma_PT': 500.,  #MeV
        'PrescaleDs2EtapPi': 1,
        'PrescaleDsst2DsGamma': 1,
        'PrescaleEta2MuMuGamma': 1
    },
    'STREAMS': ['CharmCompleteEvent']
}

### Lines stored in this file:


class CaloPIDConf(LineBuilder):

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config

        self.GammaList = DataOnDemand(
            Location="Phys/StdLooseAllPhotons/Particles")
        self.MuonList = DataOnDemand(
            Location="Phys/StdAllLooseMuons/Particles")
        self.PionList = DataOnDemand(Location="Phys/StdLoosePions/Particles")
        self.KaonList = DataOnDemand(Location="Phys/StdLooseKaons/Particles")
        self.DsList = DataOnDemand(
            Location="Phys/StdVeryTightDsplus2KKPi/Particles")

        #-------------

        self.Rho2PiPi = self.createCombinationSel(
            OutputList=self.name + "Rho2PiPi",
            DecayDescriptor="rho(770)0 -> pi+ pi-",
            DaughterLists=[self.PionList],
            DaughterCuts={
                "pi+":
                "(PT > %(Etap_Pi_PT)s*MeV) & (MIPCHI2DV(PRIMARY) > %(Etap_Pi_MIPCHI2DV)s) & (TRCHI2DOF < %(Etap_Pi_Track_Chi2ndof)s) & (TRGHOSTPROB < %(Etap_Pi_TRGHOSTPROB)s) & (P > %(Etap_Pi_P)s*MeV) & (PIDK-PIDpi < %(Etap_Pi_PID)s)"
                % self.config
            },
            PreVertexCuts="(AM>600*MeV) & (AM<900*MeV)",
            PostVertexCuts="ALL")

        self.Etap2RhoGamma = self.createCombinationSel(
            OutputList=self.name + "Etap2RhoGamma",
            DecayDescriptor="eta_prime -> rho(770)0 gamma",
            DaughterLists=[self.Rho2PiPi, self.GammaList],
            DaughterCuts={
                "gamma": "(PT > %(Etap_gamma_PT)s*MeV)" % self.config
            },
            PreVertexCuts=
            "(in_range( %(Etap_Mass_Min)s*MeV,AM,%(Etap_Mass_Max)s*MeV))" %
            self.config,
            PostVertexCuts="ALL")

        self.Ds2EtapPi = self.createCombinationSel(
            OutputList=self.name + "Ds2EtapPi",
            DecayDescriptor="[D_s+ -> eta_prime pi+]cc",
            DaughterLists=[self.Etap2RhoGamma, self.PionList],
            DaughterCuts={
                "pi+":
                "(PT > %(D_Pi_BACH_PT)s*MeV) & (MIPCHI2DV(PRIMARY) > %(Etap_Pi_MIPCHI2DV)s) & (TRCHI2DOF < %(Etap_Pi_Track_Chi2ndof)s) & (TRGHOSTPROB < %(Etap_Pi_TRGHOSTPROB)s) & (P > %(Etap_Pi_P)s*MeV)"
                % self.config
            },
            PreVertexCuts=
            "(APT > %(D_APT)s*MeV) & (in_range( %(D_Mass_Min)s*MeV,AM,%(D_Mass_Max)s*MeV))"
            % self.config,
            PostVertexCuts=
            "(VFASPF(VCHI2PDOF) < %(D_Vtx_Chi2ndof)s) & (BPVLTIME() > %(D_BPVLTIME)s*ps) & (BPVDIRA>%(D_DIRA)s) & (BPVIP()<%(D_IP)s*mm) & (BPVIPCHI2()<%(D_IP_Chi2)s)"
            % self.config)

        Ds2EtapPiLine = StrippingLine(
            self.name + "Ds2EtapPiLine",
            prescale=config['PrescaleDs2EtapPi'],
            algos=[self.Ds2EtapPi],
            RequiredRawEvents=["Calo"])

        self.registerLine(Ds2EtapPiLine)

        #-------------

        FTCalibFilter = "( (MINTREE('pi+'==ABSID, PT) > 250.0*MeV ) & (MINTREE('pi+'==ABSID, P ) > 2000.0*MeV) & (MINTREE('pi+'==ABSID, MIPCHI2DV(PRIMARY)) > 4.0 ) & (MAXTREE('pi+'==ABSID, PIDK-PIDpi) < 3.0) & (MINTREE('K-'==ABSID, PIDK-PIDpi) > 7.0 ) & (MINTREE('K-'==ABSID, PT) > 250.0*MeV ) & (MINTREE('K-'==ABSID, P ) > 2000.0*MeV) & (MINTREE('K-'==ABSID, MIPCHI2DV(PRIMARY)) > 4.0 ) & ((SUMTREE( ISBASIC , PT ) > 3200.0*MeV) & (2 <= NINGENERATION((MIPCHI2DV(PRIMARY) > 10.0 ) , 1))) & (PT > 2000.0) & (VFASPF(VCHI2/VDOF) < 6.0) & (BPVDIRA > 0.99995) & (BPVIPCHI2() < 1500.0) & (VFASPF(VMINVDCHI2DV(PRIMARY)) > 150.0) & (BPVLTIME() > 0.2*ps) & (DOCACHI2MAX < 50.0) & (in_range ( 1920.0 , M , 2040.0 )) )"

        self.FTCalibDs = Selection(
            self.name + "FTCalibDs",
            Algorithm=FilterDesktop(Code=FTCalibFilter),
            RequiredSelections=[self.DsList])

        self.Dsst2DsGamma = self.createCombinationSel(
            OutputList=self.name + "Dsst2DsGamma",
            DecayDescriptor="[D*_s+ -> D_s+ gamma]cc",
            DaughterLists=[self.FTCalibDs, self.GammaList],
            DaughterCuts={"gamma": "(PT > %(Dsst_gamma_PT)s)" % self.config},
            PreVertexCuts="(AM>2000*MeV) & (AM<2300*MeV)",
            PostVertexCuts=
            "(BPVDIRA > %(Dsst_DIRA)s) & (BPVIP() < %(Dsst_IP)s) & (BPVIPCHI2() < %(Dsst_IP_Chi2)s) & (MM > %(Dsst_Mass_Min)s*MeV) & (MM < %(Dsst_Mass_Max)s*MeV)"
            % self.config)

        Dsst2DsGammaLine = StrippingLine(
            self.name + "Dsst2DsGammaLine",
            prescale=config['PrescaleDsst2DsGamma'],
            algos=[self.Dsst2DsGamma],
            RequiredRawEvents=["Calo"])

        self.registerLine(Dsst2DsGammaLine)

        #-------------

        self.DiMu = self.createCombinationSel(
            OutputList=self.name + "DiMu",
            DecayDescriptor="KS0 -> mu+ mu-",
            DaughterLists=[self.MuonList],
            DaughterCuts={
                "mu+":
                "(PT > %(Eta_MuPT)s*MeV) & (P > %(Eta_MuP)s*MeV) & (TRGHOSTPROB < %(Eta_GhostProb)s) & (PROBNNmu > %(Eta_MuProbNNmu)s)"
                % self.config
            },
            PreVertexCuts="(AMAXDOCA('') < %(Eta_DOCA)s*mm)" % self.config,
            PostVertexCuts="(VFASPF(VCHI2PDOF) < %(Eta_VChi2)s)" % self.config)

        PromptFilter = "(MINTREE('mu+'==ABSID,BPVIPCHI2()) < %(Eta_MuIPChi2)s) & (CHILD(PT,1)*CHILD(PT,2) > %(Eta_MuPTPROD)s*GeV*GeV) & (PT > %(Eta_DiMu_PT)s*MeV) & (HASVERTEX) & (BPVVDCHI2 < %(Eta_DiMu_FDChi2)s)" % self.config

        self.PromptDiMu = Selection(
            self.name + "PromptDiMu",
            Algorithm=FilterDesktop(Code=PromptFilter),
            RequiredSelections=[self.DiMu])

        self.Eta2MuMuGamma = self.createCombinationSel(
            OutputList=self.name + "Eta2MuMuGamma",
            DecayDescriptor="eta -> KS0 gamma",
            DaughterLists=[self.PromptDiMu, self.GammaList],
            DaughterCuts={
                "gamma": "(PT > %(Eta_gamma_PT)s*MeV)" % self.config
            },
            PreVertexCuts=
            "(in_range( %(Eta_Mass_Min)s*MeV , AM , %(Eta_Mass_Max)s*MeV))" %
            self.config,
            PostVertexCuts="ALL")

        Eta2MuMuGammaLine = StrippingLine(
            self.name + "Eta2MuMuGammaLine",
            prescale=config['PrescaleEta2MuMuGamma'],
            algos=[self.Eta2MuMuGamma],
            RequiredRawEvents=["Calo"])

        self.registerLine(Eta2MuMuGammaLine)

        #-------------

    def createSubSel(self, OutputList, InputList, Cuts):
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code=Cuts)
        return Selection(
            OutputList, Algorithm=filter, RequiredSelections=[InputList])

    def createCombinationSel(self,
                             OutputList,
                             DecayDescriptor,
                             DaughterLists,
                             DaughterCuts={},
                             PreVertexCuts="ALL",
                             PostVertexCuts="ALL",
                             ReFitPVs=True):
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles(
            DecayDescriptor=DecayDescriptor,
            DaughtersCuts=DaughterCuts,
            MotherCut=PostVertexCuts,
            CombinationCut=PreVertexCuts,
            ReFitPVs=ReFitPVs)
        return Selection(
            OutputList, Algorithm=combiner, RequiredSelections=DaughterLists)
