###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Stripping selection for D0 -> K pi gamma
'''

__author__ = ['Max Chefdeville']
__date__ = '10/04/2019'
__version__ = '$Revision: 1.0 $'

__all__ = ('StrippingD02KPiGammaConf', 'makeD02KPiGamma', 'default_config')

####################################################################
# Stripping selection for D0 -> K pi gamma
# line intended to measure PID performance of isPhoton
# by measuring pi0's reco'ed as photons
# Selections taken from Regis Lefevre D02Kpipi0 lines
####################################################################

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdTightPions, StdTightKaons, StdLooseAllPhotons

default_config = {
    'NAME': 'D02KPiGamma',
    'WGs': ['Calib'],
    'BUILDERTYPE': 'StrippingD02KPiGammaConf',
    'CONFIG': {
        'TrackMinPT': 300  # MeV
        ,
        'TrackMinTrackProb': 0.000001  # unitless
        ,
        'TrackMaxGhostProb': 0.3  # unitless
        ,
        'TrackMinIPChi2': 16  # unitless
        ,
        'GammaMinPT': 2000  # MeV
        ,
        'D0MinM': 1600  # MeV
        ,
        'D0MaxM': 2100  # MeV
        ,
        'D0MinVtxProb': 0.001  # unitless
        ,
        'D0MaxIPChi2': 9  # unitless
        ,
        'D0MinDIRA': 0.9999  # unitless
        ,
        'D0MinVVDChi2': 64  # unitless
        ,
        'GammaLinePrescale': 0.05  # unitless
        ,
        'GammaLinePostscale': 1  # unitless
    },
    'STREAMS': ['CharmCompleteEvent']
}


class StrippingD02KPiGammaConf(LineBuilder):

    __configuration_keys__ = ('TrackMinPT', 'TrackMinTrackProb',
                              'TrackMaxGhostProb', 'TrackMinIPChi2',
                              'GammaMinPT', 'D0MinM', 'D0MaxM', 'D0MinVtxProb',
                              'D0MaxIPChi2', 'D0MinDIRA', 'D0MinVVDChi2',
                              'GammaLinePrescale', 'GammaLinePostscale')

    ##############################################################
    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)

        myPions = StdTightPions
        myKaons = StdTightKaons
        myGammas = StdLooseAllPhotons

        #---------------------------------------
        # D -> HHGamma selections
        self.seld0 = makeD02KPiGamma(
            name,
            config,
            DecayDescriptor='[D0 -> K- pi+ gamma]cc',
            inputSel=[myKaons, myPions, myGammas])

        #---------------------------------------
        # Stripping lines
        self.D02KPiGamma_line = StrippingLine(
            name % locals()['config'],
            prescale=config['GammaLinePrescale'],
            postscale=config['GammaLinePostscale'],
            RequiredRawEvents=["Calo"],
            selection=self.seld0)
        # register lines
        self.registerLine(self.D02KPiGamma_line)


##############################################################
def makeD02KPiGamma(name, config, DecayDescriptor, inputSel):

    _TrackCuts = "(PT>%(TrackMinPT)s *MeV) & (TRPCHI2>%(TrackMinTrackProb)s) & (TRGHOSTPROB<%(TrackMaxGhostProb)s) & (MIPCHI2DV(PRIMARY)>%(TrackMinIPChi2)s)" % locals(
    )['config']
    _gammaCuts = "(PT>%(GammaMinPT)s *MeV)" % locals()['config']
    _daughterCuts = {'K-': _TrackCuts, 'pi+': _TrackCuts, 'gamma': _gammaCuts}
    _combCuts = "(AM>%(D0MinM)s *MeV) & (AM<%(D0MaxM)s *MeV)" % locals(
    )['config']
    _motherCuts = "(VFASPF(VPCHI2)>%(D0MinVtxProb)s) & (BPVVDCHI2>%(D0MinVVDChi2)s) & (BPVIPCHI2()<%(D0MaxIPChi2)s) & (BPVDIRA>%(D0MinDIRA)s)" % locals(
    )['config']

    _D = CombineParticles(
        DecayDescriptor=DecayDescriptor,
        MotherCut=_motherCuts,
        CombinationCut=_combCuts,
        DaughtersCuts=_daughterCuts)

    return Selection(name + 'Sel', Algorithm=_D, RequiredSelections=inputSel)


##############################################################
