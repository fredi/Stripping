###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Stripping lines for selection of general decay topologies
    [Xi_c0 -> (V0 -> h+ h-) h+ h-]CC
In this file the stripping line for this decay is build
    [Xi_c0 -> (Lambda0 -> p+ pi-) K- pi+]CC  : CF
    [Xi_c0 -> (Lambda0 -> p+ pi-) K- K+]CC   : CS
    [Xi_c0 -> (Lambda0 -> p+ pi-) pi- pi+]CC : CS
    [Xi_c0 -> (Lambda0 -> p+ pi-) pi- K+]CC  : DCS
Throughout this file, 'Bachelor' refers to the children of the Xi_c0 which is
not part of the V0 decay.
"""

__author__ = ['Jinlin Fu, Louis Henry']
__date__ = '18/07/2019'

__all__ = ('default_config', 'StrippingHc2V02HConf')

from GaudiKernel.SystemOfUnits import MeV, GeV, mm, mrad
from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop, DaVinci__N3BodyDecays
#from StandardParticles import StdNoPIDsPions as InputPionsPiPi
#from StandardParticles import StdNoPIDsKaons as InputKaons

#from StandardParticles import StdAllLoosePions as InputPions
#from StandardParticles import StdAllLooseKaons as InputKaons

from StandardParticles import StdAllNoPIDsPions as InputPions
from StandardParticles import StdAllNoPIDsKaons as InputKaons

#from StandardParticles import StdLooseLambdaLL as InputLambdasLL
#from StandardParticles import StdVeryLooseLambdaLL as InputLambdasLL
#from StandardParticles import StdLooseLambdaDD as InputLambdasDD

from PhysSelPython.Wrappers import Selection, MergedSelection, DataOnDemand
from StrippingUtils.Utils import LineBuilder
from StrippingConf.StrippingLine import StrippingLine

default_config = {
    'NAME': 'Hc2V02H',
    'WGs': ['Charm'],
    'BUILDERTYPE': 'StrippingHc2V02HConf',
    'STREAMS': ['Charm'],
    'CONFIG': {
        # Minimum Xic0 bachelor momentum
        'Bach_P_MIN': 2.0 * GeV,
        'Bach_PT_MIN': 250.0 * MeV,
        # MIPCHI2DV_PRIMARY_Min only for PiPi mode
        'MIPCHI2DV_PRIMARY_Min': 2,
        #
        'p_L0_P_LL_min': 2.0 * GeV,
        'p_L0_PT_LL_min': 100.0 * MeV,
        'pi_L0_P_LL_min': 2.0 * GeV,
        'pi_L0_PT_LL_min': 100.0 * MeV,
        #TrackGhost Prob
        'TrGhostProbMax': 0.3,
        # PID of the Lambda proton, pi
        "ProbNNpMin_L0_LL": 0.10,
        "ProbNNpMin_L0_DD": 0.10,
        "ProbNNpiMin_L0_LL": 0.10,
        "ProbNNpiMin_L0_DD": 0.10,
        # PID of the bachelor
        "ProbNNkMin": 0.10,
        "ProbNNpiMin": 0.10,
        # Minimum L0 momentum
        #'Lambda0_P_MIN': 2000*MeV,
        'Lambda0_P_MIN': 6000 * MeV,
        # Minimum L0 transverse momentum
        'Lambda0_PT_MIN': 250 * MeV,
        # Minimum flight distance chi^2 of L0 from the primary vertex
        #'Lambda0_FDCHI2_MIN_LL': 256,
        #'Lambda0_FDCHI2_MIN_DD': 256,
        'Lambda0_FDCHI2_MIN_LL': 4,
        'Lambda0_FDCHI2_MIN_DD': 4,
        # Minimum flight distance of L0 from the primary vertex: EndVertexZ(Lambda)-EndVertexZ(Xic)
        #"LambdaMinFD_LL"  : 25.*mm,
        #"LambdaMinFD_DD"  : 0. *mm,
        "LambdaMinFD_LL": 5. * mm,
        "LambdaMinFD_DD": 500. * mm,
        # Maximum L0 vertex chi^2 per vertex fit DoF
        #'Lambda0_VCHI2VDOF_MAX': 12.0,
        'Lambda0_VCHI2VDOF_MAX_LL': 12.0,
        'Lambda0_VCHI2VDOF_MAX_DD': 25.0,
        # Xic0 mass window around the nominal Xic0 mass before the vertex fit
        'Comb_ADAMASS_WIN': 120.0 * MeV,
        # Xic0 mass window around the nominal Xic0 mass after the vertex fit
        'Xic_ADMASS_WIN': 90.0 * MeV,
        # Maximum distance of closest approach of Xic0 children
        'Comb_ADOCAMAX_MAX': 0.4 * mm,
        # Maximum Xic0 vertex chi^2 per vertex fit DoF
        'Xic_VCHI2VDOF_MAX_LL': 3.0,
        'Xic_VCHI2VDOF_MAX_DD': 3.0,
        # Xic P, PT
        'Xic_P_min': 10000.0 * MeV,
        'Xic_PT_min': 250.0 * MeV,
        # Maximum angle between Xic0 momentum and Xic0 direction of flight
        # 'Xic_acosBPVDIRA_MAX_LL':  31.6*mrad, #0.9995
        #'Xic_acosBPVDIRA_MAX_DD':  31.6*mrad,
        #'Xic_acosBPVDIRA_MAX_LL':  28.28*mrad, #0.9996
        #'Xic_acosBPVDIRA_MAX_DD':  31.6*mrad,
        #'Xic_acosBPVDIRA_MAX_LL':  24.49*mrad, #0.9997
        'Xic_acosBPVDIRA_MAX_LL': 20. * mrad,  #0.9998
        'Xic_acosBPVDIRA_MAX_DD': 31.6 * mrad,
        # Primary vertex displacement requirement, either that the Xic0 is some
        # sigma away from the PV, or it has a minimum flight time
        #'Xic_PVDispCut_LL': '(BPVVDCHI2 > 49.0)',
        #'Xic_PVDispCut_DD': '(BPVVDCHI2 > 49.0)',
        #'Xic_PVDispCut_LL': '(BPVVDCHI2 > 1.0)',
        #'Xic_PVDispCut_DD': '(BPVVDCHI2 > 1.0)',
        # HLT filters, only process events firing triggers matching the RegEx
        'Hlt1Filter': None,
        'Hlt2Filter': None,
        # Fraction of candidates to randomly throw away before stripping
        'PrescaleXic2LambdaPiPiLL': 1.0,
        'PrescaleXic2LambdaPiPiDD': 1.0,
        'PrescaleXic2LambdaKPiLL': 1.0,
        'PrescaleXic2LambdaKPiDD': 1.0,
        'PrescaleXic2LambdaKKLL': 1.0,
        'PrescaleXic2LambdaKKDD': 1.0,
        'PrescaleXic2LambdaPiKLL': 1.0,
        'PrescaleXic2LambdaPiKDD': 1.0,
        # Fraction of candidates to randomly throw away after stripping
        'PostscaleXic2LambdaPiPiLL': 1.0,
        'PostscaleXic2LambdaPiPiDD': 1.0,
        'PostscaleXic2LambdaKPiLL': 1.0,
        'PostscaleXic2LambdaKPiDD': 1.0,
        'PostscaleXic2LambdaKKLL': 1.0,
        'PostscaleXic2LambdaKKDD': 1.0,
        'PostscaleXic2LambdaPiKLL': 1.0,
        'PostscaleXic2LambdaPiKDD': 1.0,
    }
}


class StrippingHc2V02HConf(LineBuilder):
    """Creates LineBuilder object containing the stripping lines."""
    # Allowed configuration keys
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):
        """Initialise this LineBuilder instance."""
        self.name = name
        self.config = config
        LineBuilder.__init__(self, name, config)

        # Decay descriptors
        self.Xic2LambdaPiK = ['[Xi_c0 -> Lambda0 pi- K+]cc']
        self.Xic2LambdaPiPi = ['[Xi_c0 -> Lambda0 pi- pi+]cc']
        self.Xic2LambdaKPi = ['[Xi_c0 -> Lambda0 K-  pi+]cc']
        self.Xic2LambdaKK = ['[Xi_c0 -> Lambda0 K-  K+]cc']

        # Line names
        # 'LL' and 'DD' will be appended to these names for the LL and DD
        # Selection and StrippingLine instances
        self.Xic2LambdaPiK_name = '{0}_Xic2LambdaPiK'.format(name)
        self.Xic2LambdaPiPi_name = '{0}_Xic2LambdaPiPi'.format(name)
        self.Xic2LambdaKPi_name = '{0}_Xic2LambdaKPi'.format(name)
        self.Xic2LambdaKK_name = '{0}_Xic2LambdaKK'.format(name)

        # Build bachelor pion and kaon cut strings
        # Cuts MIPCHI2DV(PRIMARY)>4 & PT>250*MeV already present in the InputsParticles
        childCutsKaons = ('(P > {0[Bach_P_MIN]})'
                          '& (PT > {0[Bach_PT_MIN]})'
                          '& (PROBNNk > {0[ProbNNkMin]})'
                          '& (TRGHOSTPROB < {0[TrGhostProbMax]})').format(
                              self.config)

        childCutsPions = ('(P > {0[Bach_P_MIN]})'
                          '& (PT > {0[Bach_PT_MIN]})'
                          '& (PROBNNpi > {0[ProbNNpiMin]})'
                          '& (TRGHOSTPROB < {0[TrGhostProbMax]})').format(
                              self.config)

        childCutsPionsTight = (
            '(P > {0[Bach_P_MIN]})'
            '& (PT > {0[Bach_PT_MIN]})'
            '& (PROBNNpi > {0[ProbNNpiMin]})'
            '& (TRGHOSTPROB < {0[TrGhostProbMax]})'
            '& (MIPCHI2DV(PRIMARY) > {0[MIPCHI2DV_PRIMARY_Min]})').format(
                self.config)

        kineticCutsKaons = '{0}'.format(childCutsKaons)
        kineticCutsPions = '{0}'.format(childCutsPions)
        kineticCutsPionsTight = '{0}'.format(childCutsPionsTight)

        # Build Lambda0 cut strings
        lambda0LLCuts = (
            '(P > {0[Lambda0_P_MIN]})'
            '& (PT > {0[Lambda0_PT_MIN]})'
            '& (VFASPF(VCHI2/VDOF) < {0[Lambda0_VCHI2VDOF_MAX_LL]})').format(
                self.config)
        lambda0DDCuts = (
            '(P > {0[Lambda0_P_MIN]})'
            '& (PT > {0[Lambda0_PT_MIN]})'
            '& (VFASPF(VCHI2/VDOF) < {0[Lambda0_VCHI2VDOF_MAX_DD]})').format(
                self.config)

        # Filter Input particles
        self.Pions = Selection(
            'PionsFor{0}'.format(name),
            Algorithm=FilterDesktop(Code=kineticCutsPions),
            RequiredSelections=[InputPions])

        self.PionsTight = Selection(
            'TightPionsFor{0}'.format(name),
            Algorithm=FilterDesktop(Code=kineticCutsPionsTight),
            RequiredSelections=[InputPions])

        self.Kaons = Selection(
            'KaonsFor{0}'.format(name),
            Algorithm=FilterDesktop(Code=kineticCutsKaons),
            RequiredSelections=[InputKaons])

        # Filter Input Lambdas
        self.LambdaListLooseDD = MergedSelection(
            "StdLooseDDLambdaFor" + self.name,
            RequiredSelections=[
                DataOnDemand(Location="Phys/StdLooseLambdaDD/Particles")
            ])

        self.LambdaListLooseLL = MergedSelection(
            "StdVeryLooseLLLambdaFor" + self.name,
            RequiredSelections=[
                DataOnDemand(Location="Phys/StdVeryLooseLambdaLL/Particles")
            ])

        self.LambdaListLL = self.createSubSel(
            OutputList="LambdaLLFor" + self.name,
            InputList=self.LambdaListLooseLL,
            Cuts="(MAXTREE('p+'==ABSID,PROBNNp) > %(ProbNNpMin_L0_LL)s ) "
            "& (MAXTREE('pi-'==ABSID,PROBNNpi) > %(ProbNNpiMin_L0_LL)s )"
            "& (MAXTREE('p+'==ABSID, P) > %(p_L0_P_LL_min)s )"
            "& (MAXTREE('p+'==ABSID, PT) > %(p_L0_PT_LL_min)s )"
            "& (MAXTREE('pi-'==ABSID, P) > %(pi_L0_P_LL_min)s )"
            "& (MAXTREE('pi-'==ABSID, PT) > %(pi_L0_PT_LL_min)s )"
            '& (BPVVDCHI2 > %(Lambda0_FDCHI2_MIN_LL)s )' % self.config)

        self.LambdaListDD = self.createSubSel(
            OutputList="LambdaDDFor" + self.name,
            InputList=self.LambdaListLooseDD,
            Cuts="(MAXTREE('p+'==ABSID,PROBNNp) > %(ProbNNpMin_L0_DD)s ) "
            "& (MAXTREE('pi-'==ABSID,PROBNNpi) > %(ProbNNpiMin_L0_DD)s )"
            '& (BPVVDCHI2 > %(Lambda0_FDCHI2_MIN_DD)s )' % self.config)

        self.Lambda0LL = Selection(
            'Lambda0LLFor{0}'.format(name),
            Algorithm=FilterDesktop(Code=lambda0LLCuts),
            RequiredSelections=[self.LambdaListLL])
        self.Lambda0DD = Selection(
            'Lambda0DDFor{0}'.format(name),
            Algorithm=FilterDesktop(Code=lambda0DDCuts),
            RequiredSelections=[self.LambdaListDD])

        # Build selection for Xic -> L pi pi
        self.selXic2LambdaPiPi = self.makeHc2V02H(
            name=self.Xic2LambdaPiPi_name,
            inputSelLL=[self.Lambda0LL, self.PionsTight],
            inputSelDD=[self.Lambda0DD, self.PionsTight],
            decDescriptors=self.Xic2LambdaPiPi)

        # Build selection for Xic -> L K K
        self.selXic2LambdaKK = self.makeHc2V02H(
            name=self.Xic2LambdaKK_name,
            inputSelLL=[self.Lambda0LL, self.Kaons],
            inputSelDD=[self.Lambda0DD, self.Kaons],
            decDescriptors=self.Xic2LambdaKK)

        # Build selection for Xic -> L K pi
        self.selXic2LambdaKPi = self.makeHc2V02H(
            name=self.Xic2LambdaKPi_name,
            inputSelLL=[self.Lambda0LL, self.Kaons, self.Pions],
            inputSelDD=[self.Lambda0DD, self.Kaons, self.Pions],
            decDescriptors=self.Xic2LambdaKPi)

        # Build selection for Xic -> L pi K
        self.selXic2LambdaPiK = self.makeHc2V02H(
            name=self.Xic2LambdaPiK_name,
            inputSelLL=[self.Lambda0LL, self.Kaons, self.Pions],
            inputSelDD=[self.Lambda0DD, self.Kaons, self.Pions],
            decDescriptors=self.Xic2LambdaPiK)

        # Make line for Xic -> L pi pi
        self.line_Xic2LambdaPiPiLL = self.make_line(
            name='{0}LLLine'.format(self.Xic2LambdaPiPi_name),
            selection=self.selXic2LambdaPiPi[0],
            prescale=config['PrescaleXic2LambdaPiPiLL'],
            postscale=config['PostscaleXic2LambdaPiPiLL'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter'])
        self.line_Xic2LambdaPiPiDD = self.make_line(
            name='{0}DDLine'.format(self.Xic2LambdaPiPi_name),
            selection=self.selXic2LambdaPiPi[1],
            prescale=config['PrescaleXic2LambdaPiPiDD'],
            postscale=config['PostscaleXic2LambdaPiPiDD'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter'])

        # Make line for Xic -> L K K
        self.line_Xic2LambdaKKLL = self.make_line(
            name='{0}LLLine'.format(self.Xic2LambdaKK_name),
            selection=self.selXic2LambdaKK[0],
            prescale=config['PrescaleXic2LambdaKKLL'],
            postscale=config['PostscaleXic2LambdaKKLL'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter'])
        self.line_Xic2LambdaKKDD = self.make_line(
            name='{0}DDLine'.format(self.Xic2LambdaKK_name),
            selection=self.selXic2LambdaKK[1],
            prescale=config['PrescaleXic2LambdaKKDD'],
            postscale=config['PostscaleXic2LambdaKKDD'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter'])

        # Make line for Xic -> L K pi
        self.line_Xic2LambdaKPiLL = self.make_line(
            name='{0}LLLine'.format(self.Xic2LambdaKPi_name),
            selection=self.selXic2LambdaKPi[0],
            prescale=config['PrescaleXic2LambdaKPiLL'],
            postscale=config['PostscaleXic2LambdaKPiLL'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter'])
        self.line_Xic2LambdaKPiDD = self.make_line(
            name='{0}DDLine'.format(self.Xic2LambdaKPi_name),
            selection=self.selXic2LambdaKPi[1],
            prescale=config['PrescaleXic2LambdaKPiDD'],
            postscale=config['PostscaleXic2LambdaKPiDD'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter'])

        # Make line for Xic -> L pi K
        self.line_Xic2LambdaPiKLL = self.make_line(
            name='{0}LLLine'.format(self.Xic2LambdaPiK_name),
            selection=self.selXic2LambdaPiK[0],
            prescale=config['PrescaleXic2LambdaPiKLL'],
            postscale=config['PostscaleXic2LambdaPiKLL'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter'])
        self.line_Xic2LambdaPiKDD = self.make_line(
            name='{0}DDLine'.format(self.Xic2LambdaPiK_name),
            selection=self.selXic2LambdaPiK[1],
            prescale=config['PrescaleXic2LambdaPiKDD'],
            postscale=config['PostscaleXic2LambdaPiKDD'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter'])

    def make_line(self, name, selection, prescale, postscale, **kwargs):
        """Create the stripping line defined by the selection.

        Keyword arguments:
        name -- Base name for the Line
        selection -- Selection instance
        prescale -- Fraction of candidates to randomly drop before stripping
        postscale -- Fraction of candidates to randomly drop after stripping
        **kwargs -- Keyword arguments passed to StrippingLine constructor
        """
        # Only create the line with positive pre- and postscales
        # You can disable each line by setting either to a negative value
        if prescale > 0 and postscale > 0:
            line = StrippingLine(
                name,
                selection=selection,
                prescale=prescale,
                postscale=postscale,
                **kwargs)
            self.registerLine(line)
            return line
        else:
            return False

    def makeHc2V02H(self, name, inputSelLL, inputSelDD, decDescriptors):
        """Return two Selection instances for a Xi_c0 -> V0 h+ h+ h- decay.

        The return value is a two-tuple of Selection instances as
            (LL Selection, DD Selection)
        where LL and DD are the method of reconstruction for the V0.
        Keyword arguments:
        name -- Name to give the Selection instance
        inputSelLL -- List of inputs passed to Selection.RequiredSelections
                      for the LL Selection
        inputSelDD -- List of inputs passed to Selection.RequiredSelections
                      for the DD Selection
        decDescriptors -- List of decay descriptors for CombineParticles
        """
        lclPreambulo = ['from math import cos']

        combCuts = ("(ADAMASS('Xi_c0') < {0[Comb_ADAMASS_WIN]})"
                    "& (ADOCA(1,2) < {0[Comb_ADOCAMAX_MAX]})"
                    "& (ADOCA(2,3) < {0[Comb_ADOCAMAX_MAX]})"
                    "& (ADOCA(1,3) < {0[Comb_ADOCAMAX_MAX]})").format(
                        self.config)

        xicCuts_LL = (
            "(VFASPF(VCHI2/VDOF) < {0[Xic_VCHI2VDOF_MAX_LL]})"
            #"& ({0[Xic_PVDispCut_LL]})"
            "& (BPVDIRA > cos({0[Xic_acosBPVDIRA_MAX_LL]}))"
            "& (ADMASS('Xi_c0') < {0[Xic_ADMASS_WIN]})"
            "& ((CHILD(VFASPF(VZ),1) - VFASPF(VZ)) > {0[LambdaMinFD_LL]})"
            "& (P > {0[Xic_P_min]})"
            "& (PT > {0[Xic_PT_min]})").format(self.config)

        xicCuts_DD = (
            "(VFASPF(VCHI2/VDOF) < {0[Xic_VCHI2VDOF_MAX_DD]})"
            #"& ({0[Xic_PVDispCut_DD]})"
            "& (BPVDIRA > cos({0[Xic_acosBPVDIRA_MAX_DD]}))"
            "& (ADMASS('Xi_c0') < {0[Xic_ADMASS_WIN]})"
            "& ((CHILD(VFASPF(VZ),1) - VFASPF(VZ)) > {0[LambdaMinFD_DD]})"
            "& (P > {0[Xic_P_min]})"
            "& (PT > {0[Xic_PT_min]})").format(self.config)

        comb12Cuts = ("(DAMASS('Xi_c0') < {0[Comb_ADAMASS_WIN]})"
                      "& (ADOCA(1,2) < {0[Comb_ADOCAMAX_MAX]})").format(
                          self.config)

        _Xic_LL = DaVinci__N3BodyDecays(
            DecayDescriptors=decDescriptors,
            Preambulo=lclPreambulo,
            CombinationCut=combCuts,
            MotherCut=xicCuts_LL,
            Combination12Cut=comb12Cuts,
        )

        _Xic_DD = DaVinci__N3BodyDecays(
            DecayDescriptors=decDescriptors,
            Preambulo=lclPreambulo,
            CombinationCut=combCuts,
            MotherCut=xicCuts_DD,
            Combination12Cut=comb12Cuts,
        )

        selLL = Selection(
            '{0}LL'.format(name),
            Algorithm=_Xic_LL,
            RequiredSelections=inputSelLL)
        selDD = Selection(
            '{0}DD'.format(name),
            Algorithm=_Xic_DD,
            RequiredSelections=inputSelDD)
        return selLL, selDD

    def createSubSel(self, OutputList, InputList, Cuts):
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code=Cuts)
        return Selection(
            OutputList, Algorithm=filter, RequiredSelections=[InputList])
