/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef TOPOVERTEXANALYSERTUPLETOOL_H
#define TOPOVERTEXANALYSERTUPLETOOL_H 1

// Include files
// from Gaudi
#include "IncTopoVert/ITopoVertexAnalyserTupleTool.h"

#include "GaudiAlg/GaudiTool.h"
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"



/** @class TopoVertexAnalyserTupleTool TopoVertexAnalyserTupleTool.h
 *  This class analyses the vertex found by the TopoVertexTool.
 *
 *  @author Julien Cogan and Mathieu Perrin-Terrin
 *  @date   2013-01-31
 */
class TopoVertexAnalyserTupleTool : public GaudiTool, virtual public ITopoVertexAnalyserTupleTool
{

public:

  /// Standard constructor
  TopoVertexAnalyserTupleTool( const std::string& type,
                               const std::string& name,
                               const IInterface* parent);

  ~TopoVertexAnalyserTupleTool( ); ///< Destructor

public:

  StatusCode analyseVertices(Tuples::Tuple* tuple, const std::string& tesLocation ) override;

};

#endif // TOPOVERTEXANALYSERTUPLETOOL_H
