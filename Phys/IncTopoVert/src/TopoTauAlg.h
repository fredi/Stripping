/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef TOPOTAUALG_H
#define TOPOTAUALG_H 1

// Include files
#include "Kernel/IEventTupleTool.h"            // Interface
#include "GaudiKernel/ToolFactory.h"
#include "IncTopoVert/ITopoVertexTool.h"
#include "Kernel/IParticleCombiner.h"

// from Gaudi
#include "Kernel/DaVinciAlgorithm.h"

/** @class TopoTauAlg TopoTauAlg.h
 *
 *
 *  @author Julien Cogan, Giampiero Mancinelli
 *  @date   2013-12-17
 */
class TopoTauAlg : public DaVinciAlgorithm {
public:
  /// Standard constructor
  typedef std::vector<LHCb::RecVertex*> RecVertexVector;

  TopoTauAlg( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~TopoTauAlg( ); ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:

  ITopoVertexTool  * m_topoVertexTool ;
  IParticleCombiner* m_combiner;


private:

  std::string m_VFparticlesInputLocation;
  std::string m_particlesInputLocation;
  std::string m_outputLocation;

  IDistanceCalculator*  m_Geom;
//   double m_cut_ghost;
//   double m_cut_ips_VF;
//   double m_cut_ips;
  double m_cut_ntrk;
  double m_cut_mass;
  int             m_nEvents;            ///< Number of events
  int             m_nAccepted;          ///< Number of events accepted
  int             m_nCandidates;        ///< Number of candidates
};
#endif // TOPOTAUTALG_H
